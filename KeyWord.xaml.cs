﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using MobileShop_Application.Class;

namespace MobileShop_Application
{
    /// <summary>
    /// Interaction logic for KeyWord.xaml
    /// </summary>
    public partial class KeyWord : Window
    {
        public KeyWord()
        {
            InitializeComponent();
            A0.Focus();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (A0.Text == String.Empty || A1.Text == String.Empty || A2.Text == String.Empty || A3.Text == String.Empty || A4.Text == String.Empty || A5.Text == String.Empty || A6.Text == String.Empty || A7.Text == String.Empty || A8.Text == String.Empty || A9.Text == String.Empty)
            {
                MessageBox.Show("Fill all Key");
                return;
            }
            saveKey(1, A0.Text.Trim());
            saveKey(2, A1.Text.Trim());
            saveKey(3, A2.Text.Trim());
            saveKey(4, A3.Text.Trim());
            saveKey(5, A4.Text.Trim());
            saveKey(6, A5.Text.Trim());
            saveKey(7, A6.Text.Trim());
            saveKey(8, A7.Text.Trim());
            saveKey(9, A8.Text.Trim());
            saveKey(10, A9.Text.Trim());
            MessageBox.Show("Successfully Completed..!!","Thank You");
        }

        private void saveKey(int id,string key)
        {
            DBCon con = new DBCon();

            SqlParameter[] param2 = {
                                               new SqlParameter("@Id",SqlDbType.Int){Value=id},
                                               new SqlParameter("@key",SqlDbType.NChar){Value=key}
                                           };
            con.insertInvoice(AppConstraints.UPDATE_KEY, param2);
        }
    }
}
