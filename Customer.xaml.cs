﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using MobileShop_Application.Class;
using Microsoft.Win32;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Interop;


namespace MobileShop_Application
{
    /// <summary>
    /// Interaction logic for Customer.xaml
    /// </summary>
    public partial class Customer : Window
    {
        [DllImport("user32.dll")]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        private const int GWL_STYLE = -16;

        private const int WS_MAXIMIZEBOX = 0x10000; //maximize button
        private const int WS_MINIMIZEBOX = 0x20000; //minimize button

        public string editId = "0",load="";
        private Byte[] photoOld = new Byte[0];
        private Byte[] proofOld = new Byte[0];
        //Storing user image
        private Image img = new Image();
        //it used to store uploaded photo as array of byte code
        public byte[] photo;
        public byte[] proof;
        public Customer()
        {
            InitializeComponent();
            this.SourceInitialized += MainWindow_SourceInitialized;
            txtName.Focus();
            addHotKeys();
        }
        private void addHotKeys()
        {
            RoutedCommand newCmd = new RoutedCommand();
            newCmd.InputGestures.Add(new KeyGesture(Key.N, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(newCmd, btnNew_Click));

            RoutedCommand saveCmd = new RoutedCommand();
            saveCmd.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(saveCmd, btnSave_Click));

            RoutedCommand delCmd = new RoutedCommand();
            delCmd.InputGestures.Add(new KeyGesture(Key.D, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(delCmd, btnDelete_Click));

        }
        private IntPtr _windowHandle;
        private void MainWindow_SourceInitialized(object sender, EventArgs e)
        {
            _windowHandle = new WindowInteropHelper(this).Handle;

            //disable maximize button
            DisableMaximizeButton();
        }

        protected void DisableMaximizeButton()
        {
            if (_windowHandle == IntPtr.Zero)
                throw new InvalidOperationException("The window has not yet been completely initialized");

            SetWindowLong(_windowHandle, GWL_STYLE, GetWindowLong(_windowHandle, GWL_STYLE) & ~WS_MAXIMIZEBOX);
        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (txtName.Text == String.Empty)
            {
                MessageBox.Show("Name Field Required...!!");
                return;
            }
           
            if (btnSave.Content.ToString() == "_Save")
            {
                // Saving customer data into Database

                if (MessageBox.Show("Are you want to Save", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    try
                    {
                        using (SqlConnection con = new SqlConnection(DBCon.conStr))
                        {
                            con.Open();
                            using (SqlCommand cmd = new SqlCommand(AppConstraints.INSERT_CUSTOMER))
                            {
                                cmd.Connection = con;
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@name", txtName.Text);
                                cmd.Parameters.AddWithValue("@address", txtAddress.Text);
                                cmd.Parameters.AddWithValue("@place", txtPlace.Text);
                                cmd.Parameters.AddWithValue("@phone1", txtPhone1.Text);
                                cmd.Parameters.AddWithValue("@phone2", txtPhone2.Text);
                                cmd.Parameters.AddWithValue("@customer_type", cmbCustomerType.Text);
                                cmd.Parameters.AddWithValue("@date", dtpDate.Text);
                                if (photo == null)
                                {
                                    FileStream fs = new FileStream("profile.png", FileMode.Open, FileAccess.Read);
                                    photo = new byte[fs.Length];
                                    fs.Read(photo, 0, System.Convert.ToInt32(fs.Length));
                                    fs.Close();
                                }
                                cmd.Parameters.AddWithValue("@photo", photo);

                                if (proof == null)
                                {
                                    FileStream fs = new FileStream("profile.png", FileMode.Open, FileAccess.Read);
                                    proof = new byte[fs.Length];
                                    fs.Read(proof, 0, System.Convert.ToInt32(fs.Length));
                                    fs.Close();
                                }
                                cmd.Parameters.AddWithValue("@proof", proof);
                                if(cmd.ExecuteNonQuery()>0)
                                {
                                    MessageBox.Show("Saved..!!");
                                }

                            }
                        }
                    }
                    catch (Exception es)
                    {
                        MessageBox.Show(es.Message);
                    }
                }
            }
            else
            {
                if (MessageBox.Show("Are you want to Update", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    try
                    {
                        using (SqlConnection con = new SqlConnection(DBCon.conStr))
                        {
                            con.Open();
                            using (SqlCommand cmd = new SqlCommand(AppConstraints.UPDATE_CUSTOMER))
                            {
                                cmd.Connection = con;
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@Id", this.editId);
                                cmd.Parameters.AddWithValue("@name", txtName.Text);
                                cmd.Parameters.AddWithValue("@address", txtAddress.Text);
                                cmd.Parameters.AddWithValue("@place", txtPlace.Text);
                                cmd.Parameters.AddWithValue("@phone1", txtPhone1.Text);
                                cmd.Parameters.AddWithValue("@phone2", txtPhone2.Text);
                                cmd.Parameters.AddWithValue("@customer_type", cmbCustomerType.Text);
                                cmd.Parameters.AddWithValue("@date", dtpDate.Text);
                                if (photo == null)
                                {
                                    photo = photoOld;
                                }
                                cmd.Parameters.AddWithValue("@photo", photo);

                                if (proof == null)
                                {
                                    proof = proofOld;
                                }
                                cmd.Parameters.AddWithValue("@proof", proof);
                                if (cmd.ExecuteNonQuery() > 0)
                                {
                                    MessageBox.Show("Saved..!!");
                                }

                            }
                        }
                    }
                    catch (Exception es)
                    {
                        MessageBox.Show(es.Message);
                    }
                }
            }
        }

        private void loadEdit()
        {
            if(this.load=="EDIT")
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(DBCon.conStr))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand("SELECT * FROM Customer WHERE Id=" + this.editId))
                        {
                            cmd.Connection = con;
                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                if (dr.Read())
                                {
                                    txtName.Text = dr["name"].ToString();
                                    txtAddress.Text = dr["address"].ToString();
                                    txtPlace.Text = dr["place"].ToString();
                                    txtPhone1.Text = dr["phone1"].ToString();
                                    txtPhone2.Text = dr["phone2"].ToString();
                                    cmbCustomerType.Text = dr["customer_type"].ToString();
                                    photoOld = (Byte[])dr["photo"];
                                    proofOld = (Byte[])dr["proof"];
                                    dtpDate.Text = dr["date"].ToString();
                                    ImageSource bi = ByteToBitmapSource(photoOld);
                                    ImageSource pro = ByteToBitmapSource(proofOld);
                                    imgPhoto.Source = bi;
                                    imgProof.Source = pro;
                                    btnSave.Content = "_Update";
                                    btnDelete.IsEnabled = true;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        public ImageSource ByteToBitmapSource(byte[] image)
        {

            MemoryStream strm = new MemoryStream();
            strm.Write(image, 0, image.Length);
            strm.Position = 0;
            System.Drawing.Image img = System.Drawing.Image.FromStream(strm);
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            MemoryStream ms = new MemoryStream();
            img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
            ms.Seek(0, SeekOrigin.Begin);
            bi.StreamSource = ms;
            bi.EndInit();
            ImageSource imgSrc = bi as ImageSource;
            return imgSrc;
        }

        private void btnPhotoUpload_Click(object sender, RoutedEventArgs e)
        {
            //Upload Customer Data into Database
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            openFileDialog.Filter = "Image files (*.png;*.jpeg;*.gif;*.jpg)|*.png;*.jpeg;*.gif;*.jpg";
            if (openFileDialog.ShowDialog() == true)
            {
                BitmapImage src = new BitmapImage();
                src.BeginInit();
                src.UriSource = new Uri(openFileDialog.FileName, UriKind.RelativeOrAbsolute);
                src.EndInit();
                img.Source = src;
                img.Stretch = Stretch.Fill;
                imgPhoto.Source = src;
                FileStream fs = new FileStream(openFileDialog.FileName, FileMode.Open, FileAccess.Read);
                photo = new byte[fs.Length];
                fs.Read(photo, 0, System.Convert.ToInt32(fs.Length));
                fs.Close();
            }
        }

        private void btnProofUpload_Click(object sender, RoutedEventArgs e)
        {
            //Upload Customer Data into Database
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            openFileDialog.Filter = "Image files (*.png;*.jpeg;*.gif;*.jpg)|*.png;*.jpeg;*.gif;*.jpg";
            if (openFileDialog.ShowDialog() == true)
            {
                BitmapImage src = new BitmapImage();
                src.BeginInit();
                src.UriSource = new Uri(openFileDialog.FileName, UriKind.RelativeOrAbsolute);
                src.EndInit();
                img.Source = src;
                img.Stretch = Stretch.Fill;
                imgProof.Source = src;
                FileStream fs = new FileStream(openFileDialog.FileName, FileMode.Open, FileAccess.Read);
                proof = new byte[fs.Length];
                fs.Read(proof, 0, System.Convert.ToInt32(fs.Length));
                fs.Close();
            }
        
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();

        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            
                if (MessageBox.Show("Are you want to Delete..?? Data will loss..!!", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    SqlParameter[] param = {
                                               new SqlParameter("@Id",SqlDbType.Decimal){Value=this.editId}
                                           };
                    DBCon con = new DBCon();
                    con.deleteData(AppConstraints.DELETE_CUSTOMER, param);


                }
            }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            txtName.Text = String.Empty;
            txtPlace.Text = String.Empty;
            txtAddress.Text = String.Empty;
            txtPhone1.Text = String.Empty;
            txtPhone2.Text = String.Empty;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            dtpDate.Text = DateTime.Now.ToString();
            loadEdit();
            
        }

        private void btnPhotoCamera_Click(object sender, RoutedEventArgs e)
        {
            Camera camera = new Camera();
            camera.obj = this;
            camera.type = "Photo";
            camera.ShowDialog();
        }

        private void btnProofCamera_Click(object sender, RoutedEventArgs e)
        {
            Camera camera = new Camera();
            camera.obj = this;
            camera.type = "Proof";
            camera.ShowDialog();
        }

        private void imgPhoto_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (photo==null)
                {
                    ImageViewer view = new ImageViewer();
                    view.img = ByteToBitmapSource(photoOld);
                    view.ShowDialog();
                }
                else
                {
                    ImageViewer view = new ImageViewer();
                    view.img = ByteToBitmapSource(photo);
                    view.ShowDialog();
                }
               
            }
            catch { }
        }

        private void imgProof_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if(proof==null)
                {
                    ImageViewer view = new ImageViewer();
                    view.img = ByteToBitmapSource(proofOld);
                    view.ShowDialog();
                }
                else
                {
                    ImageViewer view = new ImageViewer();
                    view.img = ByteToBitmapSource(proof);
                    view.ShowDialog();
                }
                
            }
            catch { }
        }

       

            
   }
 }

