﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;
using MobileShop_Application.Class;

namespace MobileShop_Application
{
    /// <summary>
    /// Interaction logic for View_Item.xaml
    /// </summary>
    public partial class View_Item : Window
    {
        
        public View_Item()
        {
            InitializeComponent();
            dtpFrom.Text = DateTime.Now.ToShortDateString();
            dtpTo.Text = DateTime.Now.ToShortDateString();
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Excel.Application app = new Excel.Application();
                app.Visible = true;
                Excel.Workbook wb = app.Workbooks.Add(1);
                Excel.Worksheet ws = (Excel.Worksheet)wb.Worksheets[1];
                Excel.Range range;
                range = ws.get_Range("A1", "M1");
                range.Font.Bold = true;
                range.Font.Underline = true;
                range.Font.Size = 14;
                range.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);

                ws.Cells[1, 1] = "SlNo";
                ws.Cells[1, 2] = "Name";
                ws.Cells[1, 3] = "Model";
                ws.Cells[1, 4] = "Brand";
                ws.Cells[1, 5] = "Imei";
                ws.Cells[1, 6] = "Imei2";
                ws.Cells[1, 7] = "Purchase_rate";
                ws.Cells[1, 8] = "Sale Rate";
                ws.Cells[1, 9] = "Colour";
                ws.Cells[1, 10] = "Feature";
                ws.Cells[1, 11] = "Os";
                ws.Cells[1, 12] = "Date";
                ws.Cells[1, 13] = "Seller";
                ws.Cells[1, 14] = "Buyer";
                int i = 2;
                foreach (var lvi in lstvItem.Items)
                {
                    var item = lvi as Item_List;
                    if (item != null)
                    {
                        ws.Cells[i, 1] = item.slno;
                        ws.Cells[i, 2] = item.name;
                        ws.Cells[i, 3] = item.model;
                        ws.Cells[i, 4] = item.brand;
                        ws.Cells[i, 5] = item.imei;
                        ws.Cells[i, 6] = item.imei2;
                        ws.Cells[i, 7] = item.purchase_rate;
                        ws.Cells[i, 8] = item.sale_rate;
                        ws.Cells[i, 9] = item.colour;
                        ws.Cells[i, 10] = item.feature;
                        ws.Cells[i, 11] = item.os;
                        ws.Cells[i, 12] = item.date;
                        ws.Cells[i, 13] = item.seller;
                        ws.Cells[i, 14] = item.buyer;
                    }
                    i++;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            loadData(1);
            this.Title = "Items in Stock";
        }

        private void loadData(int active)
        {
            decimal stock = 0;
            lstvItem.Items.Clear();
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.LIST_VIEW_ITEM))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@active",active);
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            int slno = 1;
                            while (dr.Read())
                            {
                                lstvItem.Items.Add(new Item_List
                                {
                                    Id = dr["Id"].ToString(),
                                    slno = slno++.ToString(),
                                    barcode = dr["barcode"].ToString(),
                                    name = dr["name"].ToString(),
                                    model = dr["model"].ToString(),
                                    brand = dr["brand"].ToString(),
                                    imei = dr["imei"].ToString(),
                                    imei2 = dr["imei2"].ToString(),
                                    purchase_rate =ConvertToEncryprt.ConvertNumberToEncrypt(dr["purchase_rate"].ToString()),
                                    sale_rate = ConvertToEncryprt.ConvertNumberToEncrypt(dr["sale_rate"].ToString()),
                                    colour = dr["colour"].ToString(),
                                    feature = dr["feature"].ToString(),
                                    os = dr["os"].ToString(),
                                    date = Convert.ToDateTime(dr["date"].ToString()).ToShortDateString(),
                                    seller = dr["seller"].ToString(),
                                    seller_id = dr["customer_id"].ToString(),
                                    buyer = dr["buyer"].ToString(),
                                    buyer_id = dr["buyer_id"].ToString()
                                });
                                stock += Convert.ToDecimal(dr["purchase_rate"].ToString());
                            }
                            lblStock.Content = "Stock Value: " + ConvertToEncryprt.ConvertNumberToEncrypt(stock.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            loadData(1);
            this.Title = "Items in Stock";
        }

        private void rdoSold_Click(object sender, RoutedEventArgs e)
        {
            if (rdoSold.IsChecked == true)
            {
                loadData(0);
                this.Title = "Sold Items List";
            }

        }

        private void rdoStock_Click(object sender, RoutedEventArgs e)
        {
            if (rdoStock.IsChecked == true)
            {
                loadData(1);
                this.Title = "Items in Stock";
            }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void txtModel_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (rdoStock.IsChecked == true)
            {
                loadDataByModel(1);
                this.Title = "Items in Stock";
            }
            else
            {
                loadDataByModel(0);
                this.Title = "Sold Items List";
            }
        }
        private void loadDataByModel(int active)
        {
            decimal stock = 0;
            lstvItem.Items.Clear();
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.LIST_VIEW_ITEM_WITH_MODEL))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@active", active);
                        cmd.Parameters.AddWithValue("@model", txtModel.Text);
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            int slno = 1;
                            while (dr.Read())
                            {
                                lstvItem.Items.Add(new Item_List
                                {
                                    Id = dr["Id"].ToString(),
                                    slno = slno++.ToString(),
                                    barcode = dr["barcode"].ToString(),
                                    name = dr["name"].ToString(),
                                    model = dr["model"].ToString(),
                                    brand = dr["brand"].ToString(),
                                    imei = dr["imei"].ToString(),
                                    imei2 = dr["imei2"].ToString(),
                                    purchase_rate = ConvertToEncryprt.ConvertNumberToEncrypt(dr["purchase_rate"].ToString()),
                                    sale_rate = ConvertToEncryprt.ConvertNumberToEncrypt(dr["sale_rate"].ToString()),
                                    colour = dr["colour"].ToString(),
                                    feature = dr["feature"].ToString(),
                                    os = dr["os"].ToString(),
                                    date = Convert.ToDateTime(dr["date"].ToString()).ToShortDateString(),
                                    seller = dr["seller"].ToString(),
                                    seller_id = dr["customer_id"].ToString(),
                                    buyer = dr["buyer"].ToString(),
                                    buyer_id = dr["buyer_id"].ToString()
                                });
                                stock += Convert.ToDecimal(dr["purchase_rate"].ToString());
                            }
                            lblStock.Content = "Stock Value: " + ConvertToEncryprt.ConvertNumberToEncrypt(stock.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtBrand_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (rdoStock.IsChecked == true)
            {
                loadDataByBrand(1);
                this.Title = "Items in Stock";
            }
            else
            {
                loadDataByBrand(0);
                this.Title = "Sold Items List";
            }
        }
        private void loadDataByBrand(int active)
        {
            decimal stock = 0;
            lstvItem.Items.Clear();
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.LIST_VIEW_ITEM_WITH_BRAND))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@active", active);
                        cmd.Parameters.AddWithValue("@brand", txtBrand.Text);
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            int slno = 1;
                            while (dr.Read())
                            {
                                lstvItem.Items.Add(new Item_List
                                {
                                    Id = dr["Id"].ToString(),
                                    slno = slno++.ToString(),
                                    barcode = dr["barcode"].ToString(),
                                    name = dr["name"].ToString(),
                                    model = dr["model"].ToString(),
                                    brand = dr["brand"].ToString(),
                                    imei = dr["imei"].ToString(),
                                    imei2 = dr["imei2"].ToString(),
                                    purchase_rate = ConvertToEncryprt.ConvertNumberToEncrypt(dr["purchase_rate"].ToString()),
                                    sale_rate = ConvertToEncryprt.ConvertNumberToEncrypt(dr["sale_rate"].ToString()),
                                    colour = dr["colour"].ToString(),
                                    feature = dr["feature"].ToString(),
                                    os = dr["os"].ToString(),
                                    date = Convert.ToDateTime(dr["date"].ToString()).ToShortDateString(),
                                    seller = dr["seller"].ToString(),
                                    seller_id = dr["customer_id"].ToString(),
                                    buyer = dr["buyer"].ToString(),
                                    buyer_id = dr["buyer_id"].ToString()
                                });
                                stock += Convert.ToDecimal(dr["purchase_rate"].ToString());
                            }
                            lblStock.Content = "Stock Value: " + ConvertToEncryprt.ConvertNumberToEncrypt(stock.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtImei_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (rdoStock.IsChecked == true)
            {
                loadDataByIMEI(1);
                this.Title = "Items in Stock";
            }
            else
            {
                loadDataByIMEI(0);
                this.Title = "Sold Items List";
            }
        }

        private void loadDataByIMEI(int active)
        {
            decimal stock = 0;
            lstvItem.Items.Clear();
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.LIST_VIEW_ITEM_WITH_IMEI))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@active", active);
                        cmd.Parameters.AddWithValue("@imei", txtImei.Text);
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            int slno = 1;
                            while (dr.Read())
                            {
                                lstvItem.Items.Add(new Item_List
                                {
                                    Id = dr["Id"].ToString(),
                                    slno = slno++.ToString(),
                                    barcode = dr["barcode"].ToString(),
                                    name = dr["name"].ToString(),
                                    model = dr["model"].ToString(),
                                    brand = dr["brand"].ToString(),
                                    imei = dr["imei"].ToString(),
                                    imei2 = dr["imei2"].ToString(),
                                    purchase_rate = ConvertToEncryprt.ConvertNumberToEncrypt(dr["purchase_rate"].ToString()),
                                    sale_rate = ConvertToEncryprt.ConvertNumberToEncrypt(dr["sale_rate"].ToString()),
                                    colour = dr["colour"].ToString(),
                                    feature = dr["feature"].ToString(),
                                    os = dr["os"].ToString(),
                                    date = Convert.ToDateTime(dr["date"].ToString()).ToShortDateString(),
                                    seller = dr["seller"].ToString(),
                                    seller_id = dr["customer_id"].ToString(),
                                    buyer = dr["buyer"].ToString(),
                                    buyer_id = dr["buyer_id"].ToString()
                                });
                                stock += Convert.ToDecimal(dr["purchase_rate"].ToString());
                            }
                            lblStock.Content = "Stock Value: " + ConvertToEncryprt.ConvertNumberToEncrypt(stock.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void loadDataByDate(int active)
        {
            decimal stock = 0;
            lstvItem.Items.Clear();
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.LIST_VIEW_ITEM_WITH_DATE))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@active", active);
                        cmd.Parameters.AddWithValue("@from",Convert.ToDateTime(dtpFrom.Text));
                        cmd.Parameters.AddWithValue("@to", Convert.ToDateTime(dtpTo.Text));
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            int slno = 1;
                            while (dr.Read())
                            {
                                lstvItem.Items.Add(new Item_List
                                {
                                    Id = dr["Id"].ToString(),
                                    slno = slno++.ToString(),
                                    barcode = dr["barcode"].ToString(),
                                    name = dr["name"].ToString(),
                                    model = dr["model"].ToString(),
                                    brand = dr["brand"].ToString(),
                                    imei = dr["imei"].ToString(),
                                    imei2 = dr["imei2"].ToString(),
                                    purchase_rate = ConvertToEncryprt.ConvertNumberToEncrypt(dr["purchase_rate"].ToString()),
                                    sale_rate = ConvertToEncryprt.ConvertNumberToEncrypt(dr["sale_rate"].ToString()),
                                    colour = dr["colour"].ToString(),
                                    feature = dr["feature"].ToString(),
                                    os = dr["os"].ToString(),
                                    date = Convert.ToDateTime(dr["date"].ToString()).ToShortDateString(),
                                    seller = dr["seller"].ToString(),
                                    seller_id = dr["customer_id"].ToString(),
                                    buyer = dr["buyer"].ToString(),
                                    buyer_id = dr["buyer_id"].ToString()
                                });
                                stock += Convert.ToDecimal(dr["purchase_rate"].ToString());
                            }
                            lblStock.Content = "Stock Value: " + ConvertToEncryprt.ConvertNumberToEncrypt(stock.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dtpTo_CalendarClosed(object sender, RoutedEventArgs e)
        {
            if (rdoStock.IsChecked == true)
            {
                loadDataByDate(1);
                this.Title = "Items in Stock(from " + dtpFrom.Text + " to " + dtpTo.Text + ")";
            }
            else
            {
                loadDataByDate(0);
                this.Title = "Sold Items List(from " + dtpFrom.Text + " to " + dtpTo.Text + ")";
            }
        }

        private void lstvItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var item = lstvItem.Items.GetItemAt(lstvItem.SelectedIndex) as Item_List;
                Item itm1 = new Item();
                itm1.editId = item.Id;
                itm1.load = "EDIT";
                itm1.ShowDialog();             
            }
            catch
            {
            }
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            if (rdoStock.IsChecked == true)
            {
                loadData(1);
                this.Title = "Items in Stock";
            }
            else
            {
                loadData(0);
                this.Title = "Sold Items List";
            }
        }

        private void txtBarcode_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (rdoStock.IsChecked == true)
            {
                loadList_By_Barcode(1);
                this.Title = "Items in Stock";
            }
            else
            {
                loadList_By_Barcode(0);
                this.Title = "Sold Items List";
            }
        }
        private void loadList_By_Barcode(int active)
        {
            decimal stock = 0;
            lstvItem.Items.Clear();
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.LIST_VIEW_ITEM_WITH_BARCODE))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@active", active);
                        cmd.Parameters.AddWithValue("@barcode", txtBarcode.Text);
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            int slno = 1;
                            while (dr.Read())
                            {
                                lstvItem.Items.Add(new Item_List
                                {
                                    Id = dr["Id"].ToString(),
                                    slno = slno++.ToString(),
                                    barcode = dr["barcode"].ToString(),
                                    name = dr["name"].ToString(),
                                    model = dr["model"].ToString(),
                                    brand = dr["brand"].ToString(),
                                    imei = dr["imei"].ToString(),
                                    imei2 = dr["imei2"].ToString(),
                                    purchase_rate = ConvertToEncryprt.ConvertNumberToEncrypt(dr["purchase_rate"].ToString()),
                                    sale_rate = ConvertToEncryprt.ConvertNumberToEncrypt(dr["sale_rate"].ToString()),
                                    colour = dr["colour"].ToString(),
                                    feature = dr["feature"].ToString(),
                                    os = dr["os"].ToString(),
                                    date = Convert.ToDateTime(dr["date"].ToString()).ToShortDateString(),
                                    seller = dr["seller"].ToString(),
                                    seller_id = dr["customer_id"].ToString(),
                                    buyer = dr["buyer"].ToString(),
                                    buyer_id = dr["buyer_id"].ToString()
                                });
                                stock += Convert.ToDecimal(dr["purchase_rate"].ToString());
                            }
                            lblStock.Content = "Stock Value: " + ConvertToEncryprt.ConvertNumberToEncrypt(stock.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtItem_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (rdoStock.IsChecked == true)
            {
                loadList_By_Item(1);
                this.Title = "Items in Stock";
            }
            else
            {
                loadList_By_Item(0);
                this.Title = "Sold Items List";
            }
        }

        private void loadList_By_Item(int active)
        {
            decimal stock = 0;
            lstvItem.Items.Clear();
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.LIST_VIEW_ITEM_WITH_ITEM))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@active", active);
                        cmd.Parameters.AddWithValue("@item", txtItem.Text);
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            int slno = 1;
                            while (dr.Read())
                            {
                                lstvItem.Items.Add(new Item_List
                                {
                                    Id = dr["Id"].ToString(),
                                    slno = slno++.ToString(),
                                    barcode = dr["barcode"].ToString(),
                                    name = dr["name"].ToString(),
                                    model = dr["model"].ToString(),
                                    brand = dr["brand"].ToString(),
                                    imei = dr["imei"].ToString(),
                                    imei2 = dr["imei2"].ToString(),
                                    purchase_rate = ConvertToEncryprt.ConvertNumberToEncrypt(dr["purchase_rate"].ToString()),
                                    sale_rate = ConvertToEncryprt.ConvertNumberToEncrypt(dr["sale_rate"].ToString()),
                                    colour = dr["colour"].ToString(),
                                    feature = dr["feature"].ToString(),
                                    os = dr["os"].ToString(),
                                    date = Convert.ToDateTime(dr["date"].ToString()).ToShortDateString(),
                                    seller = dr["seller"].ToString(),
                                    seller_id = dr["customer_id"].ToString(),
                                    buyer = dr["buyer"].ToString(),
                                    buyer_id = dr["buyer_id"].ToString()
                                });
                                stock += Convert.ToDecimal(dr["purchase_rate"].ToString());
                            }
                            lblStock.Content = "Stock Value: " + ConvertToEncryprt.ConvertNumberToEncrypt(stock.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnViewBuyer_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.Tag.ToString()!= "6")
            {
            Customer cust = new Customer();
            cust.load = "EDIT";
            cust.editId = btn.Tag.ToString();
            cust.Owner = this;
            cust.Show();
            }
        }

        private void btnViewSeller_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            
                Customer cust = new Customer();
                cust.load = "EDIT";
                cust.editId = btn.Tag.ToString();
                cust.Owner = this;
                cust.Show();
           
        }
       
    }

    class Item_List
    {
        public string slno { get; set; }
        public string Id { get; set; }
        public string barcode { get; set; }
        public string name { get; set; }
        public string model { get; set; }
        public string brand { get; set; }
        public string imei { get; set; }
        public string imei2 { get; set; }
        public string purchase_rate { get; set; }
        public string sale_rate { get; set; }
        public string colour { get; set; }
        public string feature { get; set; }
        public string os { get; set; }
        public string date { get; set; }
        public string seller { get; set; }
        public string seller_id { get; set; }
        public string buyer { get; set; }
        public string buyer_id { get; set; }
    }
}
