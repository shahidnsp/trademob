﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using AForge.Video.DirectShow;
using System.Drawing.Imaging;
using System.Net;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows.Interop;
using System.Windows.Media;

namespace MobileShop_Application
{
    public partial class Camera : Form
    {
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);
        FilterInfoCollection videoDevices;
        public Customer obj = null;
        public string type = "";
        public Camera()
        {
            InitializeComponent();
            // show device list
            try
            {
                // enumerate video devices
                videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);

                if (videoDevices.Count == 0)
                    throw new ApplicationException();

                // add all devices to combo
                foreach (FilterInfo device in videoDevices)
                {
                    devicesCombo.Items.Add(device.Name);
                }
            }
            catch (ApplicationException)
            {
                devicesCombo.Items.Add("No local capture devices");
                devicesCombo.Enabled = false;
                takePictureBtn.Enabled = false;
            }

            devicesCombo.SelectedIndex = 0;

            VideoCaptureDevice videoCaptureSource = new VideoCaptureDevice(videoDevices[devicesCombo.SelectedIndex].MonikerString);
            videoSourcePlayer.VideoSource = videoCaptureSource;
            videoSourcePlayer.Start();
        }

        private void Camera_FormClosing(object sender, FormClosingEventArgs e)
        {
            videoSourcePlayer.SignalToStop();
            videoSourcePlayer.WaitForStop();
            videoDevices = null;
            videoDevices = null;
        }

        private void devicesCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            videoSourcePlayer.SignalToStop();
            videoSourcePlayer.WaitForStop();
            VideoCaptureDevice videoCaptureSource = new VideoCaptureDevice(videoDevices[devicesCombo.SelectedIndex].MonikerString);
            videoSourcePlayer.VideoSource = videoCaptureSource;
            videoSourcePlayer.Start();
        }

        private void takePictureBtn_Click(object sender, EventArgs e)
        {
            //try
            //{
                videoSourcePlayer.Refresh();
                DateTime time = DateTime.Now;              // Use current time
                //string format = "MMM ddd d HH mm yyyy";    // Use this format
                String strFilename = "Capture-" + time.ToLongDateString() + ".jpg";
                if (videoSourcePlayer.IsRunning)
                {
                     
                    //MessageBox.Show(strFilename);
                    //labelSaved.Text = "Capture Saved : " + strFilename;
                    if (type == "Photo")
                    {
                        Bitmap picture = videoSourcePlayer.GetCurrentVideoFrame();
                     
                        picture.Save(strFilename, ImageFormat.Jpeg);
                        BitmapImage src = new BitmapImage();
                        src.BeginInit();
                        src.UriSource = new Uri("F:\\MobileShop_Application\\MobileShop_Application\\bin\\Debug\\" + strFilename, UriKind.RelativeOrAbsolute);
                        src.EndInit();
                        obj.imgPhoto.Source = src;
                        obj.imgPhoto.Stretch = Stretch.Fill;
                        obj.imgPhoto.Source = src;
                        FileStream fs = new FileStream(strFilename, FileMode.Open, FileAccess.Read);
                        obj.photo = new byte[fs.Length];
                        fs.Read(obj.photo, 0, System.Convert.ToInt32(fs.Length));
                        fs.Close();
                        MessageBox.Show("Photo Captured...Close window or Try again", "Thank You");
                        //obj.imgPhoto = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(videoSourcePlayer.GetCurrentVideoFrame(),IntPtr.Zero,System.Windows.Int32Rect.Empty,BitmapSizeOptions.FromWidthAndHeight(300,300));
                    }
                    else
                    {                       
                        Bitmap picture1 = videoSourcePlayer.GetCurrentVideoFrame();
                        picture1.Save(strFilename + "_proof", ImageFormat.Jpeg);
                        BitmapImage src = new BitmapImage();
                        src.BeginInit();
                        src.UriSource = new Uri("F:\\MobileShop_Application\\MobileShop_Application\\bin\\Debug\\" + strFilename, UriKind.RelativeOrAbsolute);
                        src.EndInit();
                        obj.imgProof.Source = src;
                        obj.imgProof.Stretch = Stretch.Fill;
                        obj.imgProof.Source = src;
                        FileStream fs = new FileStream(strFilename, FileMode.Open, FileAccess.Read);
                        obj.proof = new byte[fs.Length];
                        fs.Read(obj.proof, 0, System.Convert.ToInt32(fs.Length));
                        fs.Close();
                        MessageBox.Show("Proof Captured...Close window or Try again", "Thank You");
                    }
                }
            //}
            //catch { }

                videoSourcePlayer.SignalToStop();
                videoSourcePlayer.WaitForStop();
                VideoCaptureDevice videoCaptureSource = new VideoCaptureDevice(videoDevices[devicesCombo.SelectedIndex].MonikerString);
                videoSourcePlayer.VideoSource = videoCaptureSource;
                videoSourcePlayer.Start();
        }

        private BitmapImage Bitmap2BitmapImage(Bitmap bitmap)
        {
            BitmapSource i = Imaging.CreateBitmapSourceFromHBitmap(
                           bitmap.GetHbitmap(),
                           IntPtr.Zero,
                           System.Windows.Int32Rect.Empty,
                           BitmapSizeOptions.FromEmptyOptions());
            return (BitmapImage)i;
        }
    }
}
