﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using MobileShop_Application.Class;

namespace MobileShop_Application
{
    /// <summary>
    /// Interaction logic for MDIForm.xaml
    /// </summary>
    public partial class MDIForm : Window
    {
        public MDIForm()
        {
            InitializeComponent();
        }

        private void mnBrand_Click(object sender, RoutedEventArgs e)
        {
            Brand brand = new Brand();
            brand.Owner = this;
            brand.Show();
        }

        private void mnBrandList_Click(object sender, RoutedEventArgs e)
        {
            ViewBrand viewBrand = new ViewBrand();
            viewBrand.Owner = this;
            viewBrand.Show();
        }

        private void mnExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void mnProduct_Click(object sender, RoutedEventArgs e)
        {
            Item item = new Item();
            item.Owner = this;
            item.Show();
        }

        private void mnCustomer_Click(object sender, RoutedEventArgs e)
        {
            Customer cust = new Customer();
            cust.Owner = this;
            cust.Show();
        }

        private void mnProductList_Click(object sender, RoutedEventArgs e)
        {
            View_Item viewitem = new View_Item();
            viewitem.Owner = this;
            viewitem.Show();
        }

        private void mnCSList_Click(object sender, RoutedEventArgs e)
        {
            View_Customer cust = new View_Customer();
            cust.Owner = this;
            cust.Show();
        }

        private void mnSales_Click(object sender, RoutedEventArgs e)
        {
            Sales_Invoice sales = new Sales_Invoice();
            sales.Owner = this;
            sales.Show();
        }

        private void mnSalesReport_Click(object sender, RoutedEventArgs e)
        {
            View_Sales sales = new View_Sales();
            sales.Owner = this;
            sales.Show();
        }

        private void mnCSSList_Click(object sender, RoutedEventArgs e)
        {
            View_Customer cust = new View_Customer();
            cust.Owner = this;
            cust.Show();
        }

        private void btnSales_Click(object sender, RoutedEventArgs e)
        {
            Sales_Invoice sales = new Sales_Invoice();
            sales.Owner = this;
            sales.Show();
        }

        private void btnSalesList_Click(object sender, RoutedEventArgs e)
        {
            View_Sales sales = new View_Sales();
            sales.Owner = this;
            sales.Show();
        }

        private void btnProduct_Click(object sender, RoutedEventArgs e)
        {
            Item item = new Item();
            item.Owner = this;
            item.Show();
        }

        private void btnCustomerList_Click(object sender, RoutedEventArgs e)
        {
            View_Customer cust = new View_Customer();
            cust.Owner = this;
            cust.Show();
        }

        private void btnCustomer_Click(object sender, RoutedEventArgs e)
        {
            Customer cust = new Customer();
            cust.Owner = this;
            cust.Show();
        }

        private void btnProductList_Click(object sender, RoutedEventArgs e)
        {
            View_Item viewitem = new View_Item();
            viewitem.Owner = this;
            viewitem.Show();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void mnVisit_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.psybotechnologies.com");
        }

        private void mnAbout_Click(object sender, RoutedEventArgs e)
        {
            About about = new About();
            about.Owner = this;
            about.Show();
        }

        private void mnKey_Click(object sender, RoutedEventArgs e)
        {
            KeyWord key = new KeyWord();
            key.Owner = this;
            key.Show();
        }

        private void mnUser_Click(object sender, RoutedEventArgs e)
        {
            UserRegistration usr = new UserRegistration();
            usr.Owner = this;
            usr.Show();
        }

        private void mnBcakground_Click(object sender, RoutedEventArgs e)
        {
            Background back = new Background();
            back.Owner = this;
            back.Show();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            getKey();
        }

        private void getKey()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("SELECT * FROM KeyValue"))
                    {
                        cmd.Connection = con;
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                ConvertToEncryprt.keys[Convert.ToInt32(dr["Id"].ToString()) - 1] = dr["keyval"].ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnPrefix_Click(object sender, RoutedEventArgs e)
        {
            Prefix prefix = new Prefix();
            prefix.Owner = this;
            prefix.Show();
        }

        private void mnCompany_Click(object sender, RoutedEventArgs e)
        {
            Company company = new Company();
            company.Owner = this;
            company.Show();
        }

        private void mnBarcode_Click(object sender, RoutedEventArgs e)
        {
            BarcodeGenerator bar = new BarcodeGenerator();
            bar.Owner = this;
            bar.Show();
        }

        private void btIncome_Click(object sender, RoutedEventArgs e)
        {
            Income inc = new Income();
            inc.Owner = this;
            inc.Show();
        }

        private void btnExpense_Click(object sender, RoutedEventArgs e)
        {
            Expense exp = new Expense();
            exp.Owner = this;
            exp.Show();
        }

        private void mnIncome_Click(object sender, RoutedEventArgs e)
        {
            Income inc = new Income();
            inc.Owner = this;
            inc.Show();
        }

        private void mnExpense_Click(object sender, RoutedEventArgs e)
        {
            Expense exp = new Expense();
            exp.Owner = this;
            exp.Show();
        }

        private void mnBalance_Click(object sender, RoutedEventArgs e)
        {
            ViewBalance bal = new ViewBalance();
            bal.Owner = this;
            bal.Show();
        }

        private void btnProfit_Click(object sender, RoutedEventArgs e)
        {
            ViewBalance bal = new ViewBalance();
            bal.Owner = this;
            bal.Show();
        }

       

        private void btnTaxRate_Click(object sender, RoutedEventArgs e)
        {
            TaxRate tax = new TaxRate();
            tax.Owner = this;
            tax.Show();
        }

        private void btnstorageLoccation_Click(object sender, RoutedEventArgs e)
        {
            Storage_Location loccation = new Storage_Location();
            loccation.Owner = this;
            loccation.Show();
        }

        private void mnUnit_Click(object sender, RoutedEventArgs e)
        {
            Unit un = new Unit();
            un.Owner = this;
            un.Show();
        }

      

       

      

       
    }
}
