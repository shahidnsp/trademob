﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using MobileShop_Application.Class;
using System.Runtime.InteropServices;
using System.Windows.Interop;

namespace MobileShop_Application
{
    /// <summary>
    /// Interaction logic for Brand.xaml
    /// </summary>
    public partial class Brand : Window
    {
        [DllImport("user32.dll")]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        private const int GWL_STYLE = -16;

        private const int WS_MAXIMIZEBOX = 0x10000; //maximize button
        private const int WS_MINIMIZEBOX = 0x20000; //minimize button

        public string editId = "0";
        public string load = "0";
        public Brand()
        {
            InitializeComponent();
            this.SourceInitialized += MainWindow_SourceInitialized;
            txtName.Focus();
            addHotKeys();
        }
        private void addHotKeys()
        {
            RoutedCommand newCmd = new RoutedCommand();
            newCmd.InputGestures.Add(new KeyGesture(Key.N, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(newCmd, btnNew_Click));

            RoutedCommand saveCmd = new RoutedCommand();
            saveCmd.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(saveCmd, btnSave_Click));

            RoutedCommand delCmd = new RoutedCommand();
            delCmd.InputGestures.Add(new KeyGesture(Key.D, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(delCmd, btnDelete_Click));
        }
        private IntPtr _windowHandle;
        private void MainWindow_SourceInitialized(object sender, EventArgs e)
        {
            _windowHandle = new WindowInteropHelper(this).Handle;
            //disable maximize button
            DisableMaximizeButton();
        }

        protected void DisableMaximizeButton()
        {
            if (_windowHandle == IntPtr.Zero)
                throw new InvalidOperationException("The window has not yet been completely initialized");
            SetWindowLong(_windowHandle, GWL_STYLE, GetWindowLong(_windowHandle, GWL_STYLE) & ~WS_MAXIMIZEBOX);
        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if(txtName.Text==String.Empty)
            {
                MessageBox.Show("Name Field Required...!!");
                return;
            }
            if (btnSave.Content.ToString() == "_Save")
            {
                if (MessageBox.Show("Are you want to save..??", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    SqlParameter[] param = {
                                           new SqlParameter("@name",SqlDbType.NVarChar){Value=txtName.Text}
                                       };
                    DBCon con = new DBCon();
                    con.insertData(AppConstraints.INSERT_BRAND, param);
                }
            }
            else
            {
                if (MessageBox.Show("Are you want to Update..??", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    SqlParameter[] param = {
                                           new SqlParameter("@Id",SqlDbType.Decimal){Value=this.editId},
                                           new SqlParameter("@name",SqlDbType.NVarChar){Value=txtName.Text}
                                       };
                    DBCon con = new DBCon();
                    con.modifyData(AppConstraints.UPDATE_BRAND, param);
                }
            }
            loadDate();
        }
        class Items
        {
            public string Id { get; set; }
            public string slno { get; set; }
            public string name { get; set; }
        }
        private void loadDate()
        {
            lstvBrand.Items.Clear();
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.LIST_VIEW_BRAND))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            int slno = 1;
                            while (dr.Read())
                            {
                               
                                lstvBrand.Items.Add(new Items
                                {
                                    Id = dr["Id"].ToString(),
                                    slno = slno++.ToString(),
                                    name = dr["name"].ToString()                                   
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            txtName.Text = String.Empty;
            btnSave.Content = "_Save";
            btnDelete.IsEnabled = false;
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Are you want to Delete..?? Data will be loss..!!", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                SqlParameter[] param = {
                                           new SqlParameter("@Id",SqlDbType.Decimal){Value=this.editId}
                                       };
                DBCon con = new DBCon();
                con.deleteData(AppConstraints.DELETE_BRAND, param);
                loadDate();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            loadDate();
            btnDelete.IsEnabled = false;
            loadEdit();
        }

        private void lstvBrand_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var item = lstvBrand.Items.GetItemAt(lstvBrand.SelectedIndex) as Items;
                this.editId = item.Id;
                btnSave.Content = "_Update";
                btnDelete.IsEnabled = true;
            }
            catch { }
        }

        private void loadEdit()
        {
            if(this.load=="EDIT")
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(DBCon.conStr))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand("SELECT name FROM Brand WHERE Id="+this.editId))
                        {
                            cmd.Connection = con;
                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                if (dr.Read())
                                {
                                    txtName.Text = dr["name"].ToString();
                                    btnSave.Content = "_Update";
                                    btnDelete.IsEnabled = true;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}
