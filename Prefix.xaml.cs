﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using MobileShop_Application.Class;
using System.Runtime.InteropServices;
using System.Windows.Interop;

namespace MobileShop_Application
{
    /// <summary>
    /// Interaction logic for Prefix.xaml
    /// </summary>
    public partial class Prefix : Window
    {
        [DllImport("user32.dll")]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        private const int GWL_STYLE = -16;

        private const int WS_MAXIMIZEBOX = 0x10000; //maximize button
        private const int WS_MINIMIZEBOX = 0x20000; //minimize button
        public Prefix()
        {
            InitializeComponent();
            this.SourceInitialized += MainWindow_SourceInitialized;
            loadData();
        }
        private IntPtr _windowHandle;
        private void MainWindow_SourceInitialized(object sender, EventArgs e)
        {
            _windowHandle = new WindowInteropHelper(this).Handle;
            //disable maximize button
            DisableMaximizeButton();
        }

        protected void DisableMaximizeButton()
        {
            if (_windowHandle == IntPtr.Zero)
                throw new InvalidOperationException("The window has not yet been completely initialized");
            SetWindowLong(_windowHandle, GWL_STYLE, GetWindowLong(_windowHandle, GWL_STYLE) & ~WS_MAXIMIZEBOX);
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }   
        private void loadData()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("SELECT * FROM InvoiceID"))
                    {
                        cmd.Connection = con;
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                switch(dr["Id"].ToString())
                                {
                                    case "2":
                                        txtSPrefix.Text = dr["prefix"].ToString();
                                        txtSStart.Text = dr["start"].ToString();
                                        if (dr["yes"].ToString() == "1")
                                        {
                                            txtSPrefix.IsEnabled = true;
                                            rdoSPrefix.IsChecked = true;
                                            if (dr["year"].ToString() =="1")
                                                chkSYear.IsChecked = true;
                                            else
                                                chkSYear.IsChecked = false;
                                        }
                                        else
                                        {
                                            txtSPrefix.IsEnabled = false;
                                            rdoSNumeric.IsChecked = true;
                                        }
                                        break;
                                    case "1":
                                        txtPOPrefix.Text = dr["prefix"].ToString();
                                        txtPOStart.Text = dr["start"].ToString();
                                        if (dr["yes"].ToString() == "1")
                                        {
                                            txtPOPrefix.IsEnabled = true;
                                            rdoPOPrefix.IsChecked = true;
                                            if (dr["year"].ToString() == "1")
                                                chkPOYear.IsChecked = true;
                                            else
                                                chkPOYear.IsChecked = false;
                                        }
                                        else
                                        {
                                            txtPOPrefix.IsEnabled = false;
                                            rdoPONumeric.IsChecked = true;
                                        }
                                        break;                                   
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void rdoSNumeric_Click(object sender, RoutedEventArgs e)
        {
            if (rdoSNumeric.IsChecked == true)
                txtSPrefix.IsEnabled = false;
            else
                txtSPrefix.IsEnabled = true;
        }

        private void rdoSPrefix_Click(object sender, RoutedEventArgs e)
        {
            if (rdoSPrefix.IsChecked == true)
                txtSPrefix.IsEnabled = true;
            else
                txtSPrefix.IsEnabled = false;
        }

        private void rdoPONumeric_Click(object sender, RoutedEventArgs e)
        {
            if (rdoPONumeric.IsChecked == true)
                txtPOPrefix.IsEnabled = false;
            else
                txtPOPrefix.IsEnabled = true;
        }

        private void rdoPOPrefix_Click(object sender, RoutedEventArgs e)
        {
            if (rdoPOPrefix.IsChecked == true)
                txtPOPrefix.IsEnabled = true;
            else
                txtPOPrefix.IsEnabled = false;
        }
        private void updateSales()
        {
            int yes = 0, year = 0;
            if (rdoSPrefix.IsChecked == true)
                yes = 1;
            if (chkSYear.IsChecked == true)
                year = 1;


            SqlParameter[] param ={
                                          new SqlParameter("@Id",SqlDbType.Int){Value=2},
                                          new SqlParameter("@prefix",SqlDbType.VarChar){Value=txtSPrefix.Text},
                                          new SqlParameter("@yes",SqlDbType.Int){Value=yes},
                                          new SqlParameter("@year",SqlDbType.Int){Value=year},
                                          new SqlParameter("@start",SqlDbType.Decimal){Value=txtSStart.Text}
                                     };
            DBCon con = new DBCon();
            con.insertInvoice(AppConstraints.UPDATE_INVOICE_PREPIX, param);
        }
        private void updateProduct()
        {
            int yes = 0, year = 0;
            if (rdoPOPrefix.IsChecked == true)
                yes = 1;
            if (chkPOYear.IsChecked == true)
                year = 1;

            SqlParameter[] param ={
                                          new SqlParameter("@Id",SqlDbType.Int){Value=1},
                                          new SqlParameter("@prefix",SqlDbType.VarChar){Value=txtPOPrefix.Text},
                                          new SqlParameter("@yes",SqlDbType.Int){Value=yes},
                                          new SqlParameter("@year",SqlDbType.Int){Value=year},
                                          new SqlParameter("@start",SqlDbType.Decimal){Value=txtPOStart.Text}
                                     };
            DBCon con = new DBCon();
            con.insertInvoice(AppConstraints.UPDATE_INVOICE_PREPIX, param);
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            updateProduct();
            updateSales();
            Message msg = new Message();
            msg.setMessageBox("Successfully updated", 1);
        }

      }
  }



