﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using MobileShop_Application.Class;
using System.Windows.Media.Animation;

namespace MobileShop_Application
{
    /// <summary>
    /// Interaction logic for TaxRate.xaml
    /// </summary>
    public partial class TaxRate : Window
    {

        private Message msg = new Message();
        public TaxRate()
        {
            ResourceDictionary newRes = new ResourceDictionary();
            newRes.Source = new Uri(UserController.theme, UriKind.Relative);
            this.Resources.MergedDictionaries.Clear();
            this.Resources.MergedDictionaries.Add(newRes);
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void getMaxId()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("Select_Max_TaxID"))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.Read())
                            {
                                if (dr[0] == DBNull.Value)
                                {
                                    txtTaxID.Text = "1";
                                }
                                else
                                {
                                    txtTaxID.Text = (Convert.ToDecimal(dr[0]) + 1).ToString();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (txtName.Text == String.Empty)
            {
                MessageBox.Show("Tax name required");
                return;
            }

            if (txtRate.Text == String.Empty)
            {
                MessageBox.Show("Tax Percentage required");
                return;
            }

            if (btnSave.Content.ToString() == "_Save")
            {
                /*
                 * 
                 * Insert Tax Category and Tax Percentage to Database Tax table
                 * 
                 * */
                if (MessageBox.Show("Are you want to Save?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    SqlParameter[] param ={
                                          new SqlParameter("@name",SqlDbType.VarChar){Value=txtName.Text},
                                          new SqlParameter("@percentage",SqlDbType.Decimal){Value=txtRate.Text},
                                          new SqlParameter("@reg_date",SqlDbType.DateTime){Value=DateTime.Now},
                                          new SqlParameter("@last_update",SqlDbType.DateTime){Value=DateTime.Now},
                                          new SqlParameter("@active",SqlDbType.Decimal){Value=1}
                                     };
                    DBCon con = new DBCon();
                    con.insertData(AppConstraints.INSERT_TAX_RATE, param);
                }
            }
            else if (btnSave.Content.ToString() == "_Update")
            {
                if (MessageBox.Show("Are you want to Update?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    SqlParameter[] param ={
                                          new SqlParameter("@Id",SqlDbType.VarChar){Value=txtTaxID.Text},
                                          new SqlParameter("@name",SqlDbType.VarChar){Value=txtName.Text},
                                          new SqlParameter("@percentage",SqlDbType.Decimal){Value=txtRate.Text},
                                          new SqlParameter("@last_update",SqlDbType.DateTime){Value=DateTime.Now}

                                     };
                    DBCon con = new DBCon();
                    con.modifyData(AppConstraints.UPDATE_TAX, param);
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            addHotKeys();
            getMaxId();
            loadList();

            if (UserController.write == 1)
                btnSave.IsEnabled = true;
            else
                btnSave.IsEnabled = false;
        }

        private void addHotKeys()
        {
            RoutedCommand newCmd = new RoutedCommand();
            newCmd.InputGestures.Add(new KeyGesture(Key.N, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(newCmd, btnNew_Click));

            RoutedCommand saveCmd = new RoutedCommand();
            saveCmd.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(saveCmd, btnSave_Click));
        }

        private void loadList()
        {
            DBCon con = new DBCon();
            con.list_DataView(AppConstraints.LIST_VIEW_TAX, this.lstvTax);
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            //To Add new Item into Tax
            getMaxId();
            txtName.Text = "";
            txtRate.Text = "";

            btnSave.Content = "_Save";
            btnSave.ToolTip = "Alt+S";

            if (UserController.write == 1)
                btnSave.IsEnabled = true;
            else
                btnSave.IsEnabled = false;
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            if (lstvTax.SelectedIndex >= 0)
            {
                DataRowView item = lstvTax.Items.GetItemAt(lstvTax.SelectedIndex) as DataRowView;
                txtTaxID.Text = item[0].ToString();
                txtName.Text = item[1].ToString();
                txtRate.Text = item[2].ToString();

                if (UserController.delete == 2)
                {
                    if (MessageBox.Show("Are you want to Delete?", "confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        SqlParameter[] param ={
                                          new SqlParameter("@Id",SqlDbType.Decimal){Value=txtTaxID.Text}
                                      };
                        DBCon con = new DBCon();
                        con.deleteData(AppConstraints.DELETE_TAX, param);
                        loadList();
                    }
                }
                else if (UserController.delete == 1)
                {
                    if (MessageBox.Show("Are you want to Soft Delete?", "confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        DBCon con = new DBCon();
                        con.softDelete(AppConstraints.SOFTDELETE_TAX, txtTaxID.Text);
                        loadList();
                    }
                }
                lstvTax.SelectedIndex = -1;
            }
            else
            {
                msg.setMessageBox("Select Item First!!", 2);
            }
        }

        private void txtRate_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            char a = Convert.ToChar(e.Text);
            if (!char.IsControl(a) && !char.IsDigit(a) && (a != '.'))
            {
                e.Handled = true;
            }


            //Allow only one . 
            if (a == '.' && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            loadList();
        }



    }
}
