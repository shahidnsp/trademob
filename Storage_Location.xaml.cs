﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Media.Animation;
using MobileShop_Application.Class;

namespace MobileShop_Application
{
    /// <summary>
    /// Interaction logic for Storage_Location.xaml
    /// </summary>
    public partial class Storage_Location : Window
    {

        Message msg = new Message();
        public Storage_Location()
        {
            ResourceDictionary newRes = new ResourceDictionary();
            newRes.Source = new Uri(UserController.theme, UriKind.Relative);
            this.Resources.MergedDictionaries.Clear();
            this.Resources.MergedDictionaries.Add(newRes);
            InitializeComponent();
        }

        private void addHotKeys()
        {
            RoutedCommand newCmd = new RoutedCommand();
            newCmd.InputGestures.Add(new KeyGesture(Key.N, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(newCmd, btnNew_Click));

            RoutedCommand saveCmd = new RoutedCommand();
            saveCmd.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(saveCmd, btnSave_Click));
        }

        private void getMaxId()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.SELECT_MAX_STORAGE_LOCATION))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.Read())
                            {
                                if (dr[0] == DBNull.Value)
                                {
                                    txtLocation_Id.Text = "1";
                                }
                                else
                                {
                                    txtLocation_Id.Text = (Convert.ToDecimal(dr[0]) + 1).ToString();
                                }
                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void loadList()
        {
            DBCon con = new DBCon();
            con.list_DataView(AppConstraints.LIST_VIEW_STORAGE_LOCATION, this.lstvStorage);
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            loadList();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            getMaxId();
            loadList();
            if (UserController.write == 0)
                btnSave.IsEnabled = false;
            addHotKeys();
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            if (UserController.delete != 0)
            {
                if (lstvStorage.SelectedIndex >= 0)
                {
                    DataRowView item = lstvStorage.Items.GetItemAt(lstvStorage.SelectedIndex) as DataRowView;
                    txtLocation_Id.Text = item[0].ToString();
                    txtLocation_Name.Text = item[1].ToString();

                    if (UserController.delete == 2)
                    {
                        if (MessageBox.Show("Are you want to Delete? Data will lose", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                        {
                            SqlParameter[] param ={
                                          new SqlParameter("@Id",SqlDbType.Decimal){Value=Convert.ToDecimal(txtLocation_Id.Text)}
                                      };
                            DBCon con = new DBCon();
                            con.deleteData(AppConstraints.DELETE_STORAGE_LOCATION, param);
                            loadList();
                        }
                    }
                    else if (UserController.delete == 1)
                    {
                        if (MessageBox.Show("Are you want to Delete?", "confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                        {
                            DBCon con = new DBCon();
                            con.softDelete(AppConstraints.SOFTDELETE_STORAGE_LOCATION, txtLocation_Id.Text);
                            loadList();
                        }
                    }
                    lstvStorage.SelectedIndex = -1;
                }
                else
                {
                    msg.setMessageBox("Select Item First!!", 2);
                }
            }
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            getMaxId();
            txtLocation_Name.Text = string.Empty;
            if (UserController.write == 1)
            {
                btnSave.Content = "_Save";
                btnSave.ToolTip = "Alt+S";
                btnSave.IsEnabled = true;
            }
            else
            {
                btnSave.Content = "_Save";
                btnSave.ToolTip = "Alt+S";
                btnSave.IsEnabled = false;
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            saveOrUpdate();
        }

        private void saveOrUpdate()
        {
            if (txtLocation_Name.Text == String.Empty)
            {
                txtLocation_Name.BorderThickness = new Thickness(2);
                txtLocation_Name.BorderBrush = new SolidColorBrush(Colors.Red);
                return;
            }

            if (btnSave.Content.ToString() == "_Save")
            {
                if (MessageBox.Show("Are you want to Save?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    SqlParameter[] param ={
                                        new SqlParameter("@Id",SqlDbType.VarChar){Value=txtLocation_Id.Text},
                                        new SqlParameter("@name",SqlDbType.VarChar){Value=txtLocation_Name.Text},
                                        new SqlParameter("@reg_date",SqlDbType.DateTime){Value=DateTime.Now},
                                        new SqlParameter("@last_update",SqlDbType.DateTime){Value=DateTime.Now},
                                        new SqlParameter("@active",SqlDbType.Decimal){Value=1}

                                      };

                    DBCon con = new DBCon();
                    con.insertData(AppConstraints.INSERT_STORAGE_LOCATION, param);
                    loadList();

                }
            }
            else if (btnSave.Content.ToString() == "_Update")
            {
                if (MessageBox.Show("Are you want to Update?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    SqlParameter[] param ={
                                        new SqlParameter("@Id",SqlDbType.VarChar){Value=txtLocation_Id.Text},
                                        new SqlParameter("@name",SqlDbType.VarChar){Value=txtLocation_Name.Text},
                                        new SqlParameter("@last_update",SqlDbType.DateTime){Value=DateTime.Now}
                                      };
                    DBCon con = new DBCon();
                    con.modifyData(AppConstraints.UPDATE__STORAGE_LOCATION, param);
                    loadList();
                }
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnNew_MouseEnter(object sender, MouseEventArgs e)
        {
            btnNew.Foreground = new SolidColorBrush(Colors.Red);
        }

        private void btnNew_MouseLeave(object sender, MouseEventArgs e)
        {
            btnNew.Foreground = new SolidColorBrush(Colors.White);
        }

        private void btnSave_MouseEnter(object sender, MouseEventArgs e)
        {
            btnSave.Foreground = new SolidColorBrush(Colors.Red);
        }

        private void btnSave_MouseLeave(object sender, MouseEventArgs e)
        {
            btnSave.Foreground = new SolidColorBrush(Colors.White);
        }

        private void btnCancel_MouseEnter(object sender, MouseEventArgs e)
        {
            btnCancel.Foreground = new SolidColorBrush(Colors.Red);
        }

        private void btnCancel_MouseLeave(object sender, MouseEventArgs e)
        {
            btnCancel.Foreground = new SolidColorBrush(Colors.White);
        }

        private void txtLocation_Name_KeyDown(object sender, KeyEventArgs e)
        {
            txtLocation_Name.BorderThickness = new Thickness(1);
            txtLocation_Name.BorderBrush = new SolidColorBrush(Colors.White);

            if (e.Key == Key.Enter)
                saveOrUpdate();
        }

      


    }
}
