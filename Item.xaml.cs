﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using MobileShop_Application.Class;
using System.Runtime.InteropServices;
using System.Windows.Interop;


namespace MobileShop_Application
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Item : Window
    {
        private string prefix = "";
        private int isPrefix = 0, year = 0;
        private decimal start = 0;
        public string editId = "0",load="";
        [DllImport("user32.dll")]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        private const int GWL_STYLE = -16;
        List<int> list = new List<int>();

        private const int WS_MAXIMIZEBOX = 0x10000; //maximize button
        private const int WS_MINIMIZEBOX = 0x20000; //minimize button
        public Item()
        {
            InitializeComponent();
            this.SourceInitialized += MainWindow_SourceInitialized;
            txtName.Focus();
            getPrefix();
            addHotKeys();
        }
        private void addHotKeys()
        {
            RoutedCommand newCmd = new RoutedCommand();
            newCmd.InputGestures.Add(new KeyGesture(Key.N, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(newCmd, btnName_Click));

            RoutedCommand saveCmd = new RoutedCommand();
            saveCmd.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(saveCmd, btnSave_Click));

            RoutedCommand delCmd = new RoutedCommand();
            delCmd.InputGestures.Add(new KeyGesture(Key.D, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(delCmd, btnDelete_Click));

        }

        private IntPtr _windowHandle;
        private void MainWindow_SourceInitialized(object sender, EventArgs e)
        {
            _windowHandle = new WindowInteropHelper(this).Handle;

            //disable maximize button
            DisableMaximizeButton();
        }

        protected void DisableMaximizeButton()
        {
            if (_windowHandle == IntPtr.Zero)
                throw new InvalidOperationException("The window has not yet been completely initialized");

            SetWindowLong(_windowHandle, GWL_STYLE, GetWindowLong(_windowHandle, GWL_STYLE) & ~WS_MAXIMIZEBOX);
        }
        private bool getPrefix()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.GET_INVOICE_PREPIX))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Id", "PROD");
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.Read())
                            {
                                if (dr["yes"] != DBNull.Value)
                                    isPrefix = Convert.ToInt32(dr["yes"]);
                                start = Convert.ToDecimal(dr["start"]);
                                prefix = dr["prefix"].ToString();
                                if (dr["year"] != DBNull.Value)
                                    year = Convert.ToInt32(dr["year"]);
                                return true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return false;
        }

        private void setProductID()
        {
            decimal inv = getMaxID() + start;
            if (inv != 0)
            {
                if (isPrefix == 0)
                {

                    txtBarcode.Text = inv.ToString();
                }
                else
                {
                    if (year == 0)
                    {
                        txtBarcode.Text = prefix + inv.ToString();
                    }
                    else
                    {
                        DateTime nextYear = DateTime.Now.AddYears(1);
                        txtBarcode.Text = prefix + inv.ToString() + "/" + DateTime.Now.Year.ToString() + "-" + nextYear.Year.ToString();
                    }
                }

            }
        }

        private decimal getMaxID()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("SELECT COUNT(Id) FROM Item"))
                    {
                        cmd.Connection = con;
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.Read())
                            {
                                if (dr[0] == DBNull.Value)
                                    return 1;
                                else
                                    return (Convert.ToDecimal(dr[0])) + 1;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return 0;
        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (txtName.Text == String.Empty)
            {
                MessageBox.Show("Name Field Required...!!");
                return;
            }

            if (cmbBrand.SelectedIndex==0)
            {
                MessageBox.Show("Brand Field Required...!!");
                return;
            }

            if (cmbCustomer.SelectedIndex==0)
            {
                cmbCustomer.SelectedValue = 7;
            }

            decimal sales = 0, purchase = 0,qty=0;
            int type = 0, isstock = 0;
            try
            {
                qty = Convert.ToDecimal(txtQty.Text);
            }
            catch { }

            if (rdoService.IsChecked == true)
                type = 1;
            if (rdoMobile.IsChecked == true)
                isstock = 1;

            try
            {
                sales = Convert.ToDecimal(txtSalePrice.Text);
            }
            catch { }
            try
            {
                purchase = Convert.ToDecimal(txtPurchasePrice.Text);
            }
            catch { }
            
            if (btnSave.Content.ToString() == "_Save")
            {
                if (MessageBox.Show("Are you want to Save..??", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    SqlParameter[] param = {
                                           new SqlParameter("@barcode",SqlDbType.NVarChar){Value=txtBarcode.Text},
                                           new SqlParameter("@name",SqlDbType.NVarChar){Value=txtName.Text},
                                           new SqlParameter("@model",SqlDbType.NVarChar){Value=txtModel.Text},
                                           new SqlParameter("@brand_id",SqlDbType.Decimal){Value=cmbBrand.SelectedValue},
                                           new SqlParameter("@imei",SqlDbType.NVarChar){Value=txtImei.Text},
                                           new SqlParameter("@imei2",SqlDbType.NVarChar){Value=txtImei2.Text},
                                           new SqlParameter("@purchase_rate",SqlDbType.VarChar){Value=purchase},
                                           new SqlParameter("@sale_rate",SqlDbType.VarChar){Value=sales},
                                           new SqlParameter("@colour",SqlDbType.NVarChar){Value=txtColour.Text},
                                           new SqlParameter("@active",SqlDbType.Decimal){Value=1},
                                           new SqlParameter("@feature",SqlDbType.NVarChar){Value=txtFeatures.Text},
                                           new SqlParameter("@os",SqlDbType.NVarChar){Value=txtOs.Text},
                                           new SqlParameter("@date",SqlDbType.DateTime){Value=dtpDate.Text},
                                           new SqlParameter("@customer_id",SqlDbType.Decimal){Value=cmbCustomer.SelectedValue},
                                           new SqlParameter("@location_id",SqlDbType.Decimal){Value=cmbLocation.SelectedValue},
                                           new SqlParameter("@tax_id",SqlDbType.Decimal){Value=cmbTax.SelectedValue},
                                           new SqlParameter("@quantity",SqlDbType.NVarChar){Value=qty},
                                           new SqlParameter("@type",SqlDbType.Int){Value=type},
                                           new SqlParameter("@isstock",SqlDbType.Int){Value=isstock},
                                           new SqlParameter("@unit_id",SqlDbType.Decimal){Value=cmbUnit.SelectedValue}
                                       };
                    DBCon con = new DBCon();
                    con.insertData(AppConstraints.INSERT_ITEM, param);
                    btnQue.IsEnabled = true;

                    //if (rdoMobile.IsChecked == true)
                    //{
                    //    SqlParameter[] param2 = {
                    //                       new SqlParameter("@name",SqlDbType.NVarChar){Value=txtName.Text+" Purchase"},
                    //                       new SqlParameter("@amount",SqlDbType.VarChar){Value=purchase},
                    //                       new SqlParameter("@description",SqlDbType.NVarChar){Value="Purchase from "+cmbCustomer.Text},
                    //                       new SqlParameter("@date",SqlDbType.Date){Value=dtpDate.Text},
                    //                       new SqlParameter("@type",SqlDbType.VarChar){Value="Purchase"},
                    //                       new SqlParameter("@purchase_id",SqlDbType.Decimal){Value=getLastInsertedId()}
                    //                   };
                    //    con.insertInvoice(AppConstraints.INSERT_EXPENSE, param2);
                    //}

                    txtName.Text = String.Empty;
                    txtColour.Text = String.Empty;
                    txtFeatures.Text = String.Empty;
                    txtImei.Text = String.Empty;
                    txtModel.Text = String.Empty;
                    txtOs.Text = String.Empty;
                    txtPurchasePrice.Text = String.Empty;
                    txtSalePrice.Text = String.Empty;
                    setProductID();
                }        
            }
            else
            {
                if (MessageBox.Show("Are you want to Update..??", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    SqlParameter[] param = {
                                           new SqlParameter("@Id",SqlDbType.Decimal){Value=this.editId},
                                           new SqlParameter("@barcode",SqlDbType.NVarChar){Value=txtBarcode.Text},
                                           new SqlParameter("@name",SqlDbType.NVarChar){Value=txtName.Text},
                                           new SqlParameter("@model",SqlDbType.NVarChar){Value=txtModel.Text},
                                           new SqlParameter("@brand_id",SqlDbType.Decimal){Value=cmbBrand.SelectedValue},
                                           new SqlParameter("@imei",SqlDbType.NVarChar){Value=txtImei.Text},
                                           new SqlParameter("@imei2",SqlDbType.NVarChar){Value=txtImei2.Text},
                                           new SqlParameter("@purchase_rate",SqlDbType.VarChar){Value=purchase},
                                           new SqlParameter("@sale_rate",SqlDbType.VarChar){Value=sales},
                                           new SqlParameter("@colour",SqlDbType.NVarChar){Value=txtColour.Text},
                                           new SqlParameter("@feature",SqlDbType.NVarChar){Value=txtFeatures.Text},
                                           new SqlParameter("@os",SqlDbType.NVarChar){Value=txtOs.Text},
                                           new SqlParameter("@date",SqlDbType.DateTime){Value=dtpDate.Text},
                                           new SqlParameter("@customer_id",SqlDbType.Decimal){Value=cmbCustomer.SelectedValue},
                                           new SqlParameter("@location_id",SqlDbType.Decimal){Value=cmbLocation.SelectedValue},
                                           new SqlParameter("@tax_id",SqlDbType.Decimal){Value=cmbTax.SelectedValue},
                                           new SqlParameter("@quantity",SqlDbType.NVarChar){Value=qty},
                                           new SqlParameter("@type",SqlDbType.Int){Value=type},
                                           new SqlParameter("@isstock",SqlDbType.Int){Value=isstock},
                                           new SqlParameter("@unit_id",SqlDbType.Decimal){Value=cmbUnit.SelectedValue}
                                       };
                    DBCon con = new DBCon();
                    con.modifyData(AppConstraints.UPDATE_ITEM, param);

                  
                    //SqlParameter[] param5 = {
                    //                           new SqlParameter("@Id",SqlDbType.VarChar){Value=this.editId},
                    //                       };
                    //con.insertInvoice(AppConstraints.DELETE_EXPENSE_PURCHASE, param5);

                    //SqlParameter[] param2 = {
                    //                       new SqlParameter("@name",SqlDbType.NVarChar){Value=txtName.Text+" Purchase"},
                    //                       new SqlParameter("@amount",SqlDbType.VarChar){Value=purchase},
                    //                       new SqlParameter("@description",SqlDbType.NVarChar){Value="Purchase from "+cmbCustomer.Text},
                    //                       new SqlParameter("@date",SqlDbType.Date){Value=dtpDate.Text},
                    //                       new SqlParameter("@type",SqlDbType.VarChar){Value="Purchase"},
                    //                       new SqlParameter("@purchase_id",SqlDbType.Decimal){Value=editId}
                    //                   };
                    //con.insertInvoice(AppConstraints.INSERT_EXPENSE, param2);

                    txtName.Text = String.Empty;
                    txtColour.Text = String.Empty;
                    txtFeatures.Text = String.Empty;
                    txtImei.Text = String.Empty;
                    txtModel.Text = String.Empty;
                    txtOs.Text = String.Empty;
                    txtPurchasePrice.Text = String.Empty;
                    txtSalePrice.Text = String.Empty;
                    setProductID();
                }
            }
        }

        private int getLastInsertedId()
        {
            using (SqlConnection con = new SqlConnection(DBCon.conStr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT MAX(Id) FROM Item"))
                {
                    cmd.Connection = con;
                    SqlDataReader rd = cmd.ExecuteReader();
                    if (rd.Read())
                    {
                        return Convert.ToInt32(rd[0].ToString());
                    }
                }
            }
            return 0;
        }

        private void loadEdit()
        {
            if (this.load == "EDIT")
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(DBCon.conStr))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand("SELECT * FROM Item WHERE Id=" + this.editId))
                        {
                            cmd.Connection = con;
                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                if (dr.Read())
                                {
                                    txtName.Text = dr["name"].ToString();
                                    txtModel.Text = dr["model"].ToString();
                                    txtBarcode.Text = dr["barcode"].ToString();
                                    cmbBrand.SelectedValue = dr["brand_id"].ToString();
                                    txtImei.Text = dr["imei"].ToString();
                                    txtImei2.Text = dr["imei2"].ToString();
                                    txtPurchasePrice.Text = dr["purchase_rate"].ToString();
                                    txtSalePrice.Text = dr["sale_rate"].ToString();
                                    txtColour.Text = dr["colour"].ToString();
                                    txtFeatures.Text = dr["feature"].ToString();
                                    txtOs.Text = dr["os"].ToString();
                                    dtpDate.Text = dr["date"].ToString();
                                    cmbCustomer.SelectedValue = dr["customer_id"].ToString();
                                    cmbLocation.SelectedValue = dr["location_id"].ToString();
                                    cmbTax.SelectedValue = dr["tax_id"].ToString();
                                    txtQty.Text = dr["quantity"].ToString();
                                    if (dr["type"].ToString() == "0")
                                        rdoInventory.IsChecked = true;
                                    else
                                        rdoService.IsChecked = true;

                                    if (dr["isstock"].ToString() == "0")
                                        rdoStock.IsChecked = true;
                                    else
                                        rdoMobile.IsChecked = true;

                                    btnSave.Content = "_Update";
                                    btnDelete.IsEnabled = true;
                                    btnPrint.IsEnabled = true;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            dtpDate.Text = DateTime.Now.ToString();
            DBCon con = new DBCon();
            con.fillComboBox(this.cmbBrand, AppConstraints.COMBO_BRAND, "Brand", "name", "Id", "Select");
            con.fillComboBox(this.cmbCustomer, AppConstraints.COMBO_CUSTOMER, "Customer", "name", "Id", "Select");
            con.fillComboBox(this.cmbTax, AppConstraints.COMBO_TAX, "Tax", "name", "Id", "Select");
            con.fillComboBox(this.cmbLocation, AppConstraints.COMBO_LOCATION, "Location", "name", "Id", "Select");
            con.fillComboBox(this.cmbUnit, AppConstraints.COMBO_UNIT, "Unit", "name", "Id", "Select");
            setProductID();
            cmbCustomer.SelectedValue = 7;
            loadEdit();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Are you want to Delete..?? Data will be loss..!!", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                SqlParameter[] param = {
                                           new SqlParameter("@Id",SqlDbType.Decimal){Value=this.editId}
                                       };
                DBCon con = new DBCon();
                con.deleteData(AppConstraints.DELETE_ITEM, param);
            }
        }

        private void btnName_Click(object sender, RoutedEventArgs e)
        {
            txtName.Text = String.Empty;
            txtColour.Text = String.Empty;
            txtFeatures.Text = String.Empty;
            txtImei.Text = String.Empty;
            txtModel.Text = String.Empty;
            txtOs.Text = String.Empty;
            txtPurchasePrice.Text = String.Empty;
            txtSalePrice.Text = String.Empty;
            setProductID();
        }

        private void btnFindBrand_Click(object sender, RoutedEventArgs e)
        {
            Brand brand = new Brand();
            brand.ShowDialog();
            DBCon con = new DBCon();
            con.fillComboBox(this.cmbBrand, AppConstraints.COMBO_BRAND, "Brand", "name", "Id", "Select");
        }

        private void btnFindCustomer_Click(object sender, RoutedEventArgs e)
        {
            Customer cust = new Customer();
            cust.cmbCustomerType.SelectedIndex = 0;
            cust.cmbCustomerType.IsEnabled = false;
            cust.ShowDialog();
            DBCon con = new DBCon();
            con.fillComboBox(this.cmbCustomer, AppConstraints.COMBO_CUSTOMER, "Customer", "name", "Id", "Select");
        }

        private void btnQue_Click(object sender, RoutedEventArgs e)
        {
            list.Add(getLastInsertedId());
            btnPrint.IsEnabled = true;
            MessageBox.Show("Product added to Queue!","Thank You");
        }

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            BarcodeGenerator barcode = new BarcodeGenerator();
            barcode.list = this.list;
            barcode.loadItems();
            barcode.Owner = this;
            barcode.Show();
        }

        private void txtPurchasePrice_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            char a = Convert.ToChar(e.Text);
            if (!char.IsControl(a) && !char.IsDigit(a) && (a != '.'))
            {
                e.Handled = true;
            }


            //Allow only one . 
            if (a == '.' && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtSalePrice_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            char a = Convert.ToChar(e.Text);
            if (!char.IsControl(a) && !char.IsDigit(a) && (a != '.'))
            {
                e.Handled = true;
            }


            //Allow only one . 
            if (a == '.' && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void rdoStock_Click(object sender, RoutedEventArgs e)
        {
            if (rdoStock.IsChecked == true)
            {
                txtImei.IsEnabled = false;
                txtImei2.IsEnabled = false;
                txtOs.IsEnabled = false;
            }       
        }

        private void rdoMobile_Click(object sender, RoutedEventArgs e)
        {
            if (rdoMobile.IsChecked == true)
            {
                txtImei.IsEnabled = true;
                txtImei2.IsEnabled = true;
                txtOs.IsEnabled = true;
            }    
        }
    }
}
