﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using MobileShop_Application.Class;
using Excel = Microsoft.Office.Interop.Excel;

namespace MobileShop_Application
{
    /// <summary>
    /// Interaction logic for ViewBrand.xaml
    /// </summary>
    public partial class ViewBrand : Window
    {
        public ViewBrand()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Excel.Application app = new Excel.Application();
                app.Visible = true;
                Excel.Workbook wb = app.Workbooks.Add(1);
                Excel.Worksheet ws = (Excel.Worksheet)wb.Worksheets[1];
                Excel.Range range;
                range = ws.get_Range("A1", "C1");
                range.Font.Bold = true;
                range.Font.Underline = true;
                range.Font.Size = 14;
                range.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);

                ws.Cells[1, 1] = "SlNo";
                ws.Cells[1, 2] = "Id";
                ws.Cells[1, 3] = "Name";
              
                int i = 2;
                foreach (var lvi in lstvBrand.Items)
                {
                    var item = lvi as Brand_List;
                    if (item != null)
                    {
                        ws.Cells[i, 1] = item.slno;
                        ws.Cells[i, 2] = item.Id;
                        ws.Cells[i, 3] = item.name;
                      
                    }
                    i++;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        class Brand_List
        {
            public string slno { get; set; }
            public string Id { get; set; }
            public string name { get; set; }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Brand newbrand = new Brand();
            newbrand.ShowDialog();
        }

        private void loadDate()
        {
            lstvBrand.Items.Clear();
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.LIST_VIEW_BRAND))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            int slno = 1;
                            while (dr.Read())
                            {

                                lstvBrand.Items.Add(new Brand_List
                                {
                                    Id = dr["Id"].ToString(),
                                    slno = slno++.ToString(),
                                    name = dr["name"].ToString()
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            loadDate();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            loadDate();
        }

        private void txtName_TextChanged(object sender, TextChangedEventArgs e)
        {
            lstvBrand.Items.Clear();
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.LIST_VIEW_BRAND_WITH_NAME))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@name", txtName.Text);
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            int slno = 1;
                            while (dr.Read())
                            {

                                lstvBrand.Items.Add(new Brand_List
                                {
                                    Id = dr["Id"].ToString(),
                                    slno = slno++.ToString(),
                                    name = dr["name"].ToString()
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lstvBrand_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var item = lstvBrand.Items.GetItemAt(lstvBrand.SelectedIndex) as Brand_List;
                txtName.Text = item.name;
                Brand brand = new Brand();
                brand.editId = item.Id;
                brand.load = "EDIT";
                brand.Show();
            }
            catch { }
        }
    }
}
