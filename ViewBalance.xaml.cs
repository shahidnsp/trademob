﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using MobileShop_Application.Class;
using Excel = Microsoft.Office.Interop.Excel;
using MobileShop_Application.Class;

namespace MobileShop_Application
{
    /// <summary>
    /// Interaction logic for ViewBalance.xaml
    /// </summary>
    public partial class ViewBalance : Window
    {
        public ViewBalance()
        {
            InitializeComponent();
            dtpFrom.Text = DateTime.Now.ToShortDateString();
            dtpTo.Text = DateTime.Now.ToShortDateString();
            loadList();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void loadList()
        {
            lstvExpense.Items.Clear();
            lstvIncome.Items.Clear();
            decimal income = 0, expense = 0;
            //Load Income..............................
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.LIST_VIEW_INCOME))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                lstvIncome.Items.Add(new IncomeList
                                {
                                    Id = dr["Id"].ToString(),
                                    name = dr["name"].ToString(),
                                    date = dr["date"].ToString(),
                                    amount =ConvertToEncryprt.ConvertNumberToEncrypt(dr["amount"].ToString()), 
                                    decription = dr["description"].ToString(),
                                    type = dr["type"].ToString()                    
                                });
                                income += Convert.ToDecimal(dr["amount"].ToString());                             
                            }                         
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            //Load Expense..............................
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.LIST_VIEW_EXPENSE))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                lstvExpense.Items.Add(new ExpenseList
                                {
                                    Id = dr["Id"].ToString(),
                                    name = dr["name"].ToString(),
                                    date = dr["date"].ToString(),
                                    amount = ConvertToEncryprt.ConvertNumberToEncrypt(dr["amount"].ToString()),
                                    decription = dr["description"].ToString(),
                                    type = dr["type"].ToString()
                                });
                                expense += Convert.ToDecimal(dr["amount"].ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


            lblIncome.Content = "Total Income: " + ConvertToEncryprt.ConvertNumberToEncrypt(income.ToString());
            lblExpense.Content = "Total Expense: " + ConvertToEncryprt.ConvertNumberToEncrypt(expense.ToString());
            lblBalance.Content = "Balance: " + ConvertToEncryprt.ConvertNumberToEncrypt((income-expense).ToString());


        }

        class IncomeList
        {
            public string Id { get; set; }
            public string name { get; set; }
            public string amount { get; set; }
            public string decription { get; set; }
            public string date { get; set; }
            public string type { get; set; }
        }
        class ExpenseList
        {
            public string Id { get; set; }
            public string name { get; set; }
            public string amount { get; set; }
            public string decription { get; set; }
            public string date { get; set; }
            public string type { get; set; }
        }

        private void btnIncome_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Are you want to Delete..?? Data will be loss..!!", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                foreach (var item in lstvIncome.Items)
                {
                    var itm = item as IncomeList;
                    if (itm != null)
                    {
                        SqlParameter[] param = {
                                           new SqlParameter("@Id",SqlDbType.Decimal){Value=itm.Id}
                                       };
                        DBCon con = new DBCon();
                        con.insertInvoice(AppConstraints.DELETE_INCOME, param);
                    }
                }
                MessageBox.Show("Incomes List cleared Successfully..!!", "Thank You");
                loadList();
            }
        }

        private void btnExpense_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Are you want to Delete..?? Data will be loss..!!", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                foreach (var item in lstvExpense.Items)
                {
                    var itm = item as ExpenseList;
                    if (itm != null)
                    {
                        SqlParameter[] param = {
                                           new SqlParameter("@Id",SqlDbType.Decimal){Value=itm.Id}
                                       };
                        DBCon con = new DBCon();
                        con.insertInvoice(AppConstraints.DELETE_EXPENSE, param);
                    }
                }
                MessageBox.Show("Expense List cleared Successfully..!!", "Thank You");
                loadList();
            }
        }

        private void dtpTo_CalendarClosed(object sender, RoutedEventArgs e)
        {
            lstvExpense.Items.Clear();
            lstvIncome.Items.Clear();
            decimal income = 0, expense = 0;
            //Load Income..............................
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.LIST_VIEW_INCOME_WITH_DATE))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@from",Convert.ToDateTime(dtpFrom.Text));
                        cmd.Parameters.AddWithValue("@to", Convert.ToDateTime(dtpTo.Text));
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                lstvIncome.Items.Add(new IncomeList
                                {
                                    Id = dr["Id"].ToString(),
                                    name = dr["name"].ToString(),
                                    date = dr["date"].ToString(),
                                    amount = ConvertToEncryprt.ConvertNumberToEncrypt(dr["amount"].ToString()),
                                    decription = dr["description"].ToString(),
                                    type = dr["type"].ToString()
                                });
                                income += Convert.ToDecimal(dr["amount"].ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            //Load Expense..............................
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.LIST_VIEW_EXPENSE_WITH_DATE))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@from", Convert.ToDateTime(dtpFrom.Text));
                        cmd.Parameters.AddWithValue("@to", Convert.ToDateTime(dtpTo.Text));
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                lstvExpense.Items.Add(new ExpenseList
                                {
                                    Id = dr["Id"].ToString(),
                                    name = dr["name"].ToString(),
                                    date = dr["date"].ToString(),
                                    amount = ConvertToEncryprt.ConvertNumberToEncrypt(dr["amount"].ToString()),
                                    decription = dr["description"].ToString(),
                                    type = dr["type"].ToString()
                                });
                                expense += Convert.ToDecimal(dr["amount"].ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


            lblIncome.Content = "Total Income: " + ConvertToEncryprt.ConvertNumberToEncrypt(income.ToString());
            lblExpense.Content = "Total Expense: " + ConvertToEncryprt.ConvertNumberToEncrypt(expense.ToString());
            lblBalance.Content = "Balance: " + ConvertToEncryprt.ConvertNumberToEncrypt((income - expense).ToString());
        }

        private void txtIncome_TextChanged(object sender, TextChangedEventArgs e)
        {
            lstvIncome.Items.Clear();
            decimal income = 0;
            //Load Income..............................
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.LIST_VIEW_INCOME_BY_INCOME))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@income", txtIncome.Text);
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                lstvIncome.Items.Add(new IncomeList
                                {
                                    Id = dr["Id"].ToString(),
                                    name = dr["name"].ToString(),
                                    date = dr["date"].ToString(),
                                    amount = ConvertToEncryprt.ConvertNumberToEncrypt(dr["amount"].ToString()),
                                    decription = dr["description"].ToString(),
                                    type = dr["type"].ToString()
                                });
                                income += Convert.ToDecimal(dr["amount"].ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            lblIncome.Content = "Total Income: " + ConvertToEncryprt.ConvertNumberToEncrypt(income.ToString());
        }

        private void txtExpense_TextChanged(object sender, TextChangedEventArgs e)
        {
            lstvExpense.Items.Clear();
            decimal expense = 0;
            //Load Expense..............................
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.LIST_VIEW_EXPENSE_BY_EXPENSE))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@expense", txtExpense.Text);
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                lstvExpense.Items.Add(new ExpenseList
                                {
                                    Id = dr["Id"].ToString(),
                                    name = dr["name"].ToString(),
                                    date = dr["date"].ToString(),
                                    amount = ConvertToEncryprt.ConvertNumberToEncrypt(dr["amount"].ToString()),
                                    decription = dr["description"].ToString(),
                                    type = dr["type"].ToString()
                                });
                                expense += Convert.ToDecimal(dr["amount"].ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            lblExpense.Content = "Total Expense: " + ConvertToEncryprt.ConvertNumberToEncrypt(expense.ToString());
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Excel.Application app = new Excel.Application();
                app.Visible = true;
                Excel.Workbook wb = app.Workbooks.Add(1);
                Excel.Worksheet ws = (Excel.Worksheet)wb.Worksheets[1];
                Excel.Range range;
                range = ws.get_Range("A1", "M1");
                range.Font.Bold = true;
                range.Font.Underline = true;
                range.Font.Size = 14;
                range.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);

                ws.Cells[1, 1] = "SlNo";
                ws.Cells[1, 2] = "Name";
                ws.Cells[1, 3] = "Amount";
                ws.Cells[1, 4] = "Description";
                ws.Cells[1, 5] = "Date";
                ws.Cells[1, 6] = "Type";
                int i = 2;
                int slno=1;
                foreach (var lvi in lstvIncome.Items)
                {
                    var item = lvi as IncomeList;
                    if (item != null)
                    {
                        ws.Cells[i, 1] = slno++.ToString();
                        ws.Cells[i, 2] = item.name;
                        ws.Cells[i, 3] = item.amount;
                        ws.Cells[i, 4] = item.decription;
                        ws.Cells[i, 5] = item.date;
                        ws.Cells[i, 6] = item.type;
                    }
                    i++;
                }
                i = i + 3;
                slno = 1;
                foreach (var lvi in lstvExpense.Items)
                {
                    var item = lvi as ExpenseList;
                    if (item != null)
                    {
                        ws.Cells[i, 1] = slno++.ToString();
                        ws.Cells[i, 2] = item.name;
                        ws.Cells[i, 3] = item.amount;
                        ws.Cells[i, 4] = item.decription;
                        ws.Cells[i, 5] = item.date;
                        ws.Cells[i, 6] = item.type;
                    }
                    i++;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lstvIncome_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var item = lstvIncome.Items.GetItemAt(lstvIncome.SelectedIndex) as IncomeList;
                Income inc = new Income();
                inc.editId = item.Id;
                inc.load = "EDIT";
                inc.ShowDialog();
            }
            catch
            {
            }
        }

        private void lstvExpense_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var item = lstvExpense.Items.GetItemAt(lstvExpense.SelectedIndex) as ExpenseList;
                Expense inc = new Expense();
                inc.editId = item.Id;
                inc.load = "EDIT";
                inc.ShowDialog();
            }
            catch
            {
            }
        }
    }
}
