﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using MobileShop_Application.Class;
using System.Windows.Media.Animation;

namespace MobileShop_Application
{
    /// <summary>
    /// Interaction logic for Unit.xaml
    /// </summary>
    public partial class Unit : Window
    {
        Message msg = new Message();
        public Unit()
        {
            //ResourceDictionary newRes = new ResourceDictionary();
            //newRes.Source = new Uri(UserController.theme, UriKind.Relative);
            //this.Resources.MergedDictionaries.Clear();
            //this.Resources.MergedDictionaries.Add(newRes);
            InitializeComponent();
        }
        private void addHotKeys()
        {
            RoutedCommand newCmd = new RoutedCommand();
            newCmd.InputGestures.Add(new KeyGesture(Key.N, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(newCmd, btnNew1_Click));

            RoutedCommand saveCmd = new RoutedCommand();
            saveCmd.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(saveCmd, btnSave1_Click));
        }
        private void getMaxId()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.SELECT_MAX_UNIT_ID))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.Read())
                            {
                                if (dr[0] == DBNull.Value)
                                {
                                    txtUnit_Id.Text = "1";
                                }
                                else
                                {
                                    txtUnit_Id.Text = (Convert.ToDecimal(dr[0]) + 1).ToString();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnNew1_Click(object sender, RoutedEventArgs e)
        {
            getMaxId();
            txtUnit_Name.Text = String.Empty;

            btnSave1.Content = "_Save";
            btnSave1.ToolTip = "Alt+S";

            //if (UserController.write == 1)
            //{
            //    btnSave1.IsEnabled = true;
            //}
            //else
            //{
            //    btnSave1.IsEnabled = false;
            //}
        }

        private void btnSave1_Click(object sender, RoutedEventArgs e)
        {
            //if (txtUnit_Name.Text == String.Empty)
            //{
            //    txtUnit_Name.BorderThickness = new Thickness(2);
            //    txtUnit_Name.BorderBrush = new SolidColorBrush(Colors.Red);
            //    return;
            //}

            if (btnSave1.Content.ToString() == "_Save")
            {
                if (MessageBox.Show("Are you want to Save?", "confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    //insertion of unit data in to table Unit
                    string isunit = "NO";
                    if (chkUnit.IsChecked == true)
                    {
                        isunit = "YES";
                    }
                    SqlParameter[] param = {
                                           new SqlParameter("@Id",SqlDbType.Decimal){Value=txtUnit_Id.Text},
                                           new SqlParameter("@name",SqlDbType.VarChar){Value=txtUnit_Name.Text},
                                           new SqlParameter("@reg_date",SqlDbType.DateTime){Value=DateTime.Now},                                       
                                           new SqlParameter("@last_update",SqlDbType.DateTime){Value=DateTime.Now},                                      
                                           new SqlParameter("@active",SqlDbType.Decimal){Value=1},
                                           new SqlParameter("@isunit",SqlDbType.VarChar){Value=isunit},
                                           new SqlParameter("@kg",SqlDbType.VarChar){Value=txtKG.Text}
                                       };

                    DBCon con = new DBCon();
                    con.insertData(AppConstraints.INSERT_UNIT_DATA, param);
                    loadList();
                }
            }
            else if (btnSave1.Content.ToString() == "_Update")
            {
                if (MessageBox.Show("Are you want to Update?", "confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    //Updating unit information
                    string isunit = "NO";
                    if (chkUnit.IsChecked == true)
                    {
                        isunit = "YES";
                    }
                    SqlParameter[] param = {
                                           new SqlParameter("@Id",SqlDbType.Decimal){Value=txtUnit_Id.Text},
                                           new SqlParameter("@name",SqlDbType.VarChar){Value=txtUnit_Name.Text},                                     
                                           new SqlParameter("@last_update",SqlDbType.DateTime){Value=DateTime.Now},    
                                           new SqlParameter("@isunit",SqlDbType.VarChar){Value=isunit},
                                           new SqlParameter("@kg",SqlDbType.VarChar){Value=txtKG.Text}
                                       };

                    DBCon con = new DBCon();
                    con.insertData(AppConstraints.UPDATE__UNIT_DETAILS, param);
                    loadList();
                }
            }
        }

        private void btnClose_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void loadList()
        {
            DBCon con = new DBCon();
            con.list_DataView(AppConstraints.LIST_VIEW_UNIT, this.lstvUnit);
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            if (UserController.delete != 0)
            {
                if (lstvUnit.SelectedIndex >= 0)
                {
                    DataRowView item = lstvUnit.Items.GetItemAt(lstvUnit.SelectedIndex) as DataRowView;
                    txtUnit_Id.Text = item[0].ToString();
                    txtUnit_Name.Text = item[1].ToString();

                    if (UserController.delete == 2)
                    {
                        if (MessageBox.Show("Are you want to Delete? Data will lose!!!", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                        {
                            SqlParameter[] param ={
                                          new SqlParameter("@Id",SqlDbType.Decimal){Value=Convert.ToDecimal(txtUnit_Id.Text)}
                                      };
                            DBCon con = new DBCon();
                            con.deleteData(AppConstraints.DELETE_UNIT, param);
                            loadList();
                        }
                    }
                    else if (UserController.delete == 1)
                    {

                        if (MessageBox.Show("Are you want to Delete?", "confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                        {
                            DBCon con = new DBCon();
                            con.softDelete(AppConstraints.SOFTDELETE_UNIT, txtUnit_Id.Text);
                            loadList();
                        }
                    }
                    lstvUnit.SelectedIndex = -1;
                }
                else
                {
                    msg.setMessageBox("Select Item First!!", 2);
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            addHotKeys();
            getMaxId();
            loadList();
           
        }

        private void btnNew1_MouseEnter(object sender, MouseEventArgs e)
        {
            btnNew1.Foreground = new SolidColorBrush(Colors.Red);
        }

        private void btnNew1_MouseLeave(object sender, MouseEventArgs e)
        {
            btnNew1.Foreground = new SolidColorBrush(Colors.Black);
        }

        private void btnSave1_MouseEnter(object sender, MouseEventArgs e)
        {
            btnSave1.Foreground = new SolidColorBrush(Colors.Red);
        }

        private void btnSave1_MouseLeave(object sender, MouseEventArgs e)
        {
            btnSave1.Foreground = new SolidColorBrush(Colors.Black);
        }

        private void btnClose_MouseEnter(object sender, MouseEventArgs e)
        {
            btnClose.Foreground = new SolidColorBrush(Colors.Red);
        }

        private void btnClose_MouseLeave(object sender, MouseEventArgs e)
        {
            btnClose.Foreground = new SolidColorBrush(Colors.Black);
        }

        private void chkUnit_Click(object sender, RoutedEventArgs e)
        {
            if (chkUnit.IsChecked == true)
            {
                txtKG.IsEnabled = true;
            }
            else
            {
                txtKG.IsEnabled = false;
            }
        }

        private void txtUnit_Name_TextChanged(object sender, TextChangedEventArgs e)
        {
            lblLabel.Content = "1 " + txtUnit_Name.Text + " =";
        }

        private void txtUnit_Name_KeyDown(object sender, KeyEventArgs e)
        {
            txtUnit_Name.BorderThickness = new Thickness(1);
            txtUnit_Name.BorderBrush = new SolidColorBrush(Colors.White);
        }

        private void txtKG_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            char a = Convert.ToChar(e.Text);
            if (!char.IsControl(a) && !char.IsDigit(a) && (a != '.'))
            {
                e.Handled = true;
            }


            //Allow only one . 
            if (a == '.' && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

       
    }
}
