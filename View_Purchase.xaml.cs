﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using Excel = Microsoft.Office.Interop.Excel;
using MobileShop_Application.Class;



namespace MobileShop_Application
{
    /// <summary>
    /// Interaction logic for View_Purchase.xaml
    /// </summary>
    public partial class View_Purchase : Window
    {
        public View_Purchase()
        {
            InitializeComponent();
            dtpFrom.Text = DateTime.Now.ToShortDateString();
            dtpTo.Text = DateTime.Now.ToShortDateString();
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Excel.Application app = new Excel.Application();
                app.Visible = true;
                Excel.Workbook wb = app.Workbooks.Add(1);
                Excel.Worksheet ws = (Excel.Worksheet)wb.Worksheets[1];
                Excel.Range range;
                range = ws.get_Range("A1", "H1");
                range.Font.Bold = true;
                range.Font.Underline = true;
                range.Font.Size = 14;
                range.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);

                ws.Cells[1, 1] = "SlNo";
                ws.Cells[1, 2] = "Invoice No";
                ws.Cells[1, 3] = "Invoice Date";
                ws.Cells[1, 4] = "Customer";
                ws.Cells[1, 5] = "Gross Amount";
                ws.Cells[1, 6] = "Discount";
                ws.Cells[1, 7] = "Additional";
                ws.Cells[1, 8] = "Balance";

                int i = 2;
                foreach (var lvi in lstvCustomer.Items)
                {
                    var item = lvi as Purchase_List;
                    if (item != null)
                    {
                        ws.Cells[i, 1] = item.slno;
                        ws.Cells[i, 2] = item.invoice_no;
                        ws.Cells[i, 3] = item.invoice_date;
                        ws.Cells[i, 4] = item.customer;
                        ws.Cells[i, 5] = item.gross_amount;
                        ws.Cells[i, 6] = item.discount;
                        ws.Cells[i, 7] = item.additional;
                        ws.Cells[i, 8] = item.balance;

                    }
                    i++;
                }
                i++;
                ws.Cells[i, 1] = lblSales.Content.ToString();
                ws.Cells[i, 2] = lblDiscount.Content.ToString();
                ws.Cells[i, 3] = lblBalance.Content.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        class Purchase_List
        {
            public string slno { get; set; }
            public string Id { get; set; }
            public string invoice_no { get; set; }
            public string invoice_date { get; set; }
            public string customer { get; set; }
            public string gross_amount { get; set; }
            public string discount { get; set; }
            public string additional { get; set; }
            public string balance { get; set; }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Purchase_Invoice Purchase = new Purchase_Invoice();
            Purchase.Show();
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {

        }

        private void loadData()
        {
            decimal sales = 0, discount = 0, balance = 0;
            lstvCustomer.Items.Clear();
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.LIST_VIEW_SALES_INVOICE))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            int slno = 1;
                            while (dr.Read())
                            {
                                lstvCustomer.Items.Add(new Purchase_List
                                {
                                    Id = dr["Id"].ToString(),
                                    slno = slno++.ToString(),
                                    invoice_no = dr["invoice_no"].ToString(),
                                    invoice_date = dr["invoice_date"].ToString(),
                                    customer = dr["customer"].ToString(),
                                    gross_amount = ConvertToEncryprt.ConvertNumberToEncrypt(dr["gross_amount"].ToString()),
                                    discount = ConvertToEncryprt.ConvertNumberToEncrypt(dr["discount"].ToString()),
                                    additional = ConvertToEncryprt.ConvertNumberToEncrypt(dr["additional"].ToString()),
                                    balance = ConvertToEncryprt.ConvertNumberToEncrypt(dr["balance"].ToString())
                                });
                                sales += Convert.ToDecimal(dr["gross_amount"].ToString());
                                discount += Convert.ToDecimal(dr["discount"].ToString());
                                balance += Convert.ToDecimal(dr["balance"].ToString());
                            }
                            lblSales.Content = "Total Sales: " + ConvertToEncryprt.ConvertNumberToEncrypt(sales.ToString());
                            lblDiscount.Content = "TotalDiscount: " + ConvertToEncryprt.ConvertNumberToEncrypt(discount.ToString());
                            lblBalance.Content = "Total Balance: " + ConvertToEncryprt.ConvertNumberToEncrypt(balance.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            loadData();
        }

        private void txtInvoiceNo_TextChanged(object sender, TextChangedEventArgs e)
        {
            decimal sales = 0, discount = 0, balance = 0;
            lstvCustomer.Items.Clear();
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.LIST_VIEW_SALES_INVOICE_BY_INVOICENO))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@invoiceno", txtInvoiceNo.Text);
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            int slno = 1;
                            while (dr.Read())
                            {
                                lstvCustomer.Items.Add(new Purchase_List
                                {
                                    Id = dr["Id"].ToString(),
                                    slno = slno++.ToString(),
                                    invoice_no = dr["invoice_no"].ToString(),
                                    invoice_date = dr["invoice_date"].ToString(),
                                    customer = dr["customer"].ToString(),
                                    gross_amount = ConvertToEncryprt.ConvertNumberToEncrypt(dr["gross_amount"].ToString()),
                                    discount = ConvertToEncryprt.ConvertNumberToEncrypt(dr["discount"].ToString()),
                                    additional = ConvertToEncryprt.ConvertNumberToEncrypt(dr["additional"].ToString()),
                                    balance = ConvertToEncryprt.ConvertNumberToEncrypt(dr["balance"].ToString())
                                });
                                sales += Convert.ToDecimal(dr["gross_amount"].ToString());
                                discount += Convert.ToDecimal(dr["discount"].ToString());
                                balance += Convert.ToDecimal(dr["balance"].ToString());
                            }
                            lblSales.Content = "Total Sales: " + ConvertToEncryprt.ConvertNumberToEncrypt(sales.ToString());
                            lblDiscount.Content = "TotalDiscount: " + ConvertToEncryprt.ConvertNumberToEncrypt(discount.ToString());
                            lblBalance.Content = "Total Balance: " + ConvertToEncryprt.ConvertNumberToEncrypt(balance.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtCustomer_TextChanged(object sender, TextChangedEventArgs e)
        {
            decimal sales = 0, discount = 0, balance = 0;
            lstvCustomer.Items.Clear();
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.LIST_VIEW_SALES_INVOICE_BY_CUSTOMER))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@customer", txtCustomer.Text);
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            int slno = 1;
                            while (dr.Read())
                            {
                                lstvCustomer.Items.Add(new Purchase_List
                                {
                                    Id = dr["Id"].ToString(),
                                    slno = slno++.ToString(),
                                    invoice_no = dr["invoice_no"].ToString(),
                                    invoice_date = dr["invoice_date"].ToString(),
                                    customer = dr["customer"].ToString(),
                                    gross_amount = ConvertToEncryprt.ConvertNumberToEncrypt(dr["gross_amount"].ToString()),
                                    discount = ConvertToEncryprt.ConvertNumberToEncrypt(dr["discount"].ToString()),
                                    additional = ConvertToEncryprt.ConvertNumberToEncrypt(dr["additional"].ToString()),
                                    balance = ConvertToEncryprt.ConvertNumberToEncrypt(dr["balance"].ToString())
                                });
                                sales += Convert.ToDecimal(dr["gross_amount"].ToString());
                                discount += Convert.ToDecimal(dr["discount"].ToString());
                                balance += Convert.ToDecimal(dr["balance"].ToString());
                            }
                            lblSales.Content = "Total Sales: " + ConvertToEncryprt.ConvertNumberToEncrypt(sales.ToString());
                            lblDiscount.Content = "TotalDiscount: " + ConvertToEncryprt.ConvertNumberToEncrypt(discount.ToString());
                            lblBalance.Content = "Total Balance: " + ConvertToEncryprt.ConvertNumberToEncrypt(balance.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dtpTo_CalendarClosed(object sender, RoutedEventArgs e)
        {
            decimal sales = 0, discount = 0, balance = 0;
            lstvCustomer.Items.Clear();
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.LIST_VIEW_SALES_INVOICE_BY_DATE))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@from", dtpFrom.Text);
                        cmd.Parameters.AddWithValue("@to", dtpTo.Text);
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            int slno = 1;
                            while (dr.Read())
                            {
                                lstvCustomer.Items.Add(new Purchase_List
                                {
                                    Id = dr["Id"].ToString(),
                                    slno = slno++.ToString(),
                                    invoice_no = dr["invoice_no"].ToString(),
                                    invoice_date = dr["invoice_date"].ToString(),
                                    customer = dr["customer"].ToString(),
                                    gross_amount = ConvertToEncryprt.ConvertNumberToEncrypt(dr["gross_amount"].ToString()),
                                    discount = ConvertToEncryprt.ConvertNumberToEncrypt(dr["discount"].ToString()),
                                    additional = ConvertToEncryprt.ConvertNumberToEncrypt(dr["additional"].ToString()),
                                    balance = ConvertToEncryprt.ConvertNumberToEncrypt(dr["balance"].ToString())
                                });
                                sales += Convert.ToDecimal(dr["gross_amount"].ToString());
                                discount += Convert.ToDecimal(dr["discount"].ToString());
                                balance += Convert.ToDecimal(dr["balance"].ToString());
                            }
                            lblSales.Content = "Total Sales: " + ConvertToEncryprt.ConvertNumberToEncrypt(sales.ToString());
                            lblDiscount.Content = "TotalDiscount: " + ConvertToEncryprt.ConvertNumberToEncrypt(discount.ToString());
                            lblBalance.Content = "Total Balance: " + ConvertToEncryprt.ConvertNumberToEncrypt(balance.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lblCredit_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            decimal sales = 0, discount = 0, balance = 0;
            lstvCustomer.Items.Clear();
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.LIST_VIEW_SALES_INVOICE_BY_CREDIT))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            int slno = 1;
                            while (dr.Read())
                            {
                                lstvCustomer.Items.Add(new Purchase_List
                                {
                                    Id = dr["Id"].ToString(),
                                    slno = slno++.ToString(),
                                    invoice_no = dr["invoice_no"].ToString(),
                                    invoice_date = dr["invoice_date"].ToString(),
                                    customer = dr["customer"].ToString(),
                                    gross_amount = ConvertToEncryprt.ConvertNumberToEncrypt(dr["gross_amount"].ToString()),
                                    discount = ConvertToEncryprt.ConvertNumberToEncrypt(dr["discount"].ToString()),
                                    additional = ConvertToEncryprt.ConvertNumberToEncrypt(dr["additional"].ToString()),
                                    balance = ConvertToEncryprt.ConvertNumberToEncrypt(dr["balance"].ToString())
                                });
                                sales += Convert.ToDecimal(dr["gross_amount"].ToString());
                                discount += Convert.ToDecimal(dr["discount"].ToString());
                                balance += Convert.ToDecimal(dr["balance"].ToString());
                            }
                            lblSales.Content = "Total Sales: " + ConvertToEncryprt.ConvertNumberToEncrypt(sales.ToString());
                            lblDiscount.Content = "TotalDiscount: " + ConvertToEncryprt.ConvertNumberToEncrypt(discount.ToString());
                            lblBalance.Content = "Total Balance: " + ConvertToEncryprt.ConvertNumberToEncrypt(balance.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void lstvCustomer_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var item = lstvCustomer.Items.GetItemAt(lstvCustomer.SelectedIndex) as Purchase_List;
                Sales_Invoice itm1 = new Sales_Invoice();
                itm1.editId = item.Id;
                itm1.load = "EDIT";
                itm1.ShowDialog();
            }
            catch
            {
            }
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            loadData();
        }

       




    }
}
