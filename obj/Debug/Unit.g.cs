﻿#pragma checksum "..\..\Unit.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "98E4F5B2255E3D187F0FC075F44D127F"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using RootLibrary.WPF.Localization;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace MobileShop_Application {
    
    
    /// <summary>
    /// Unit
    /// </summary>
    public partial class Unit : System.Windows.Window, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 9 "..\..\Unit.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtUnit_Id;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\Unit.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtUnit_Name;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\Unit.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblRegDate;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\Unit.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chkUnit;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\Unit.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblLabel;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\Unit.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtKG;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\Unit.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView lstvUnit;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\Unit.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSave1;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\Unit.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnClose;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\Unit.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnNew1;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/MobileShop_Application;component/unit.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\Unit.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 4 "..\..\Unit.xaml"
            ((MobileShop_Application.Unit)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.txtUnit_Id = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.txtUnit_Name = ((System.Windows.Controls.TextBox)(target));
            
            #line 11 "..\..\Unit.xaml"
            this.txtUnit_Name.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txtUnit_Name_TextChanged);
            
            #line default
            #line hidden
            
            #line 11 "..\..\Unit.xaml"
            this.txtUnit_Name.KeyDown += new System.Windows.Input.KeyEventHandler(this.txtUnit_Name_KeyDown);
            
            #line default
            #line hidden
            return;
            case 4:
            this.lblRegDate = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.chkUnit = ((System.Windows.Controls.CheckBox)(target));
            
            #line 13 "..\..\Unit.xaml"
            this.chkUnit.Click += new System.Windows.RoutedEventHandler(this.chkUnit_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.lblLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.txtKG = ((System.Windows.Controls.TextBox)(target));
            
            #line 15 "..\..\Unit.xaml"
            this.txtKG.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.txtKG_PreviewTextInput);
            
            #line default
            #line hidden
            return;
            case 8:
            this.lstvUnit = ((System.Windows.Controls.ListView)(target));
            return;
            case 10:
            this.btnSave1 = ((System.Windows.Controls.Button)(target));
            
            #line 46 "..\..\Unit.xaml"
            this.btnSave1.Click += new System.Windows.RoutedEventHandler(this.btnSave1_Click);
            
            #line default
            #line hidden
            
            #line 46 "..\..\Unit.xaml"
            this.btnSave1.MouseEnter += new System.Windows.Input.MouseEventHandler(this.btnSave1_MouseEnter);
            
            #line default
            #line hidden
            
            #line 46 "..\..\Unit.xaml"
            this.btnSave1.MouseLeave += new System.Windows.Input.MouseEventHandler(this.btnSave1_MouseLeave);
            
            #line default
            #line hidden
            return;
            case 11:
            this.btnClose = ((System.Windows.Controls.Button)(target));
            
            #line 47 "..\..\Unit.xaml"
            this.btnClose.Click += new System.Windows.RoutedEventHandler(this.btnClose_Click_1);
            
            #line default
            #line hidden
            
            #line 47 "..\..\Unit.xaml"
            this.btnClose.MouseEnter += new System.Windows.Input.MouseEventHandler(this.btnClose_MouseEnter);
            
            #line default
            #line hidden
            
            #line 47 "..\..\Unit.xaml"
            this.btnClose.MouseLeave += new System.Windows.Input.MouseEventHandler(this.btnClose_MouseLeave);
            
            #line default
            #line hidden
            return;
            case 12:
            this.btnNew1 = ((System.Windows.Controls.Button)(target));
            
            #line 48 "..\..\Unit.xaml"
            this.btnNew1.Click += new System.Windows.RoutedEventHandler(this.btnNew1_Click);
            
            #line default
            #line hidden
            
            #line 48 "..\..\Unit.xaml"
            this.btnNew1.MouseEnter += new System.Windows.Input.MouseEventHandler(this.btnNew1_MouseEnter);
            
            #line default
            #line hidden
            
            #line 48 "..\..\Unit.xaml"
            this.btnNew1.MouseLeave += new System.Windows.Input.MouseEventHandler(this.btnNew1_MouseLeave);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 9:
            
            #line 30 "..\..\Unit.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnRemove_Click);
            
            #line default
            #line hidden
            break;
            }
        }
    }
}

