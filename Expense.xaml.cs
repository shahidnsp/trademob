﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using System.Data;
using System.Data.SqlClient;
using MobileShop_Application.Class;

namespace MobileShop_Application
{
    /// <summary>
    /// Interaction logic for Expense.xaml
    /// </summary>
    public partial class Expense : Window
    {
        public string editId = "0";
        public string load = "0";

        [DllImport("user32.dll")]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        private const int GWL_STYLE = -16;

        private const int WS_MAXIMIZEBOX = 0x10000; //maximize button
        private const int WS_MINIMIZEBOX = 0x20000; //minimize button
        public Expense()
        {
            InitializeComponent();
            this.SourceInitialized += MainWindow_SourceInitialized;
            txtName.Focus();
            dtpDate.Text = DateTime.Now.ToShortDateString();
        }
        private IntPtr _windowHandle;
        private void MainWindow_SourceInitialized(object sender, EventArgs e)
        {
            _windowHandle = new WindowInteropHelper(this).Handle;
            //disable maximize button
            DisableMaximizeButton();
        }

        protected void DisableMaximizeButton()
        {
            if (_windowHandle == IntPtr.Zero)
                throw new InvalidOperationException("The window has not yet been completely initialized");
            SetWindowLong(_windowHandle, GWL_STYLE, GetWindowLong(_windowHandle, GWL_STYLE) & ~WS_MAXIMIZEBOX);
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            dtpDate.Text = DateTime.Now.ToShortDateString();
            txtName.Text = String.Empty;
            txtAmount.Text = String.Empty;
            txtDesc.Text = String.Empty;
        }



        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (txtName.Text == String.Empty)
            {
                MessageBox.Show("Name Field Required...!!");
                return;
            }
            if (txtAmount.Text == String.Empty)
            {
                MessageBox.Show("Amount Field Required...!!");
                return;
            }

            if (btnSave.Content.ToString() == "_Save")
            {
                if (MessageBox.Show("Are you want to save..??", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    SqlParameter[] param = {
                                           new SqlParameter("@name",SqlDbType.NVarChar){Value=txtName.Text},
                                           new SqlParameter("@amount",SqlDbType.VarChar){Value=txtAmount.Text},
                                           new SqlParameter("@description",SqlDbType.NVarChar){Value=txtDesc.Text},
                                           new SqlParameter("@date",SqlDbType.Date){Value=dtpDate.Text},
                                           new SqlParameter("@type",SqlDbType.VarChar){Value="Expense"},
                                           new SqlParameter("@purchase_id",SqlDbType.Decimal){Value=0}
                                       };
                    DBCon con = new DBCon();
                    con.insertData(AppConstraints.INSERT_EXPENSE, param);
                }
            }
            else
            {
                if (MessageBox.Show("Are you want to Update..??", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    SqlParameter[] param = {
                                           new SqlParameter("@Id",SqlDbType.Decimal){Value=this.editId},
                                           new SqlParameter("@name",SqlDbType.NVarChar){Value=txtName.Text},
                                           new SqlParameter("@amount",SqlDbType.VarChar){Value=txtAmount.Text},
                                           new SqlParameter("@description",SqlDbType.NVarChar){Value=txtDesc.Text},
                                           new SqlParameter("@date",SqlDbType.Date){Value=dtpDate.Text}
                                       };
                    DBCon con = new DBCon();
                    con.modifyData(AppConstraints.UPDATE_EXPENSE, param);

                }
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Are you want to Delete..?? Data will be loss..!!", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                SqlParameter[] param = {
                                           new SqlParameter("@Id",SqlDbType.Decimal){Value=this.editId}
                                       };
                DBCon con = new DBCon();
                con.deleteData(AppConstraints.DELETE_EXPENSE, param);
            }
        }

        private void loadEdit()
        {
            if (this.load == "EDIT")
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(DBCon.conStr))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand("SELECT * FROM Expense WHERE Id=" + this.editId))
                        {
                            cmd.Connection = con;
                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                if (dr.Read())
                                {
                                    txtName.Text = dr["name"].ToString();
                                    txtAmount.Text = dr["amount"].ToString();
                                    txtDesc.Text = dr["description"].ToString();
                                    dtpDate.Text = dr["date"].ToString();
                                    btnSave.Content = "_Update";
                                    btnDelete.IsEnabled = true;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            loadEdit();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
