﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using MobileShop_Application.Class;

namespace MobileShop_Application
{
    /// <summary>
    /// Interaction logic for Company.xaml
    /// </summary>
    public partial class Company : Window
    {
        public Company()
        {
            InitializeComponent();
            loadData();
        }
        private void loadData()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.GET_COMPANY))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.Read())
                            {
                                txtName.Text = dr["name"].ToString();
                                txtAddress.Text = dr["address"].ToString();
                                txtPhone.Text = dr["phone"].ToString();
                                txtMobile.Text = dr["mobile"].ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            SqlParameter[] param ={
                                          new SqlParameter("@name",SqlDbType.NVarChar){Value=txtName.Text},
                                          new SqlParameter("@address",SqlDbType.NVarChar){Value=txtAddress.Text},
                                          new SqlParameter("@phone",SqlDbType.NVarChar){Value=txtPhone.Text},
                                          new SqlParameter("@mobile",SqlDbType.NVarChar){Value=txtMobile.Text}
                                     };
            DBCon con = new DBCon();
            con.insertData(AppConstraints.UPDATE_COMPANY, param);
        }

       
    }
}
