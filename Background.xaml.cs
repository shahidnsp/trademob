﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using MobileShop_Application.Class;
using Microsoft.Win32;
using System.IO;

namespace MobileShop_Application
{
    /// <summary>
    /// Interaction logic for Background.xaml
    /// </summary>
    public partial class Background : Window
    {
        private byte[] photo;
        public Background()
        {
            InitializeComponent();
        }

        private void btnUpload_Click(object sender, RoutedEventArgs e)
        {
            //Upload Customer Data into Database
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            openFileDialog.Filter = "Image files (*.png;*.jpeg;*.gif;*.jpg)|*.png;*.jpeg;*.gif;*.jpg";
            if (openFileDialog.ShowDialog() == true)
            {
                BitmapImage src = new BitmapImage();
                src.BeginInit();
                src.UriSource = new Uri(openFileDialog.FileName, UriKind.RelativeOrAbsolute);
                src.EndInit();
                img.Source = src;
                img.Stretch = Stretch.Fill;
                img.Source = src;
                FileStream fs = new FileStream(openFileDialog.FileName, FileMode.Open, FileAccess.Read);
                photo = new byte[fs.Length];
                fs.Read(photo, 0, System.Convert.ToInt32(fs.Length));
                fs.Close();
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Are you want to change Background", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(DBCon.conStr))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand(AppConstraints.UPDATE_BACKGROUND))
                        {
                            cmd.Connection = con;
                            cmd.CommandType = CommandType.StoredProcedure;
     
                            if (photo == null)
                            {
                                FileStream fs = new FileStream("background.jpg", FileMode.Open, FileAccess.Read);
                                photo = new byte[fs.Length];
                                fs.Read(photo, 0, System.Convert.ToInt32(fs.Length));
                                fs.Close();
                            }
                            cmd.Parameters.AddWithValue("@back", photo);                           
                            if (cmd.ExecuteNonQuery() > 0)
                            {
                                MessageBox.Show("Background changes after Re-Login","Thank You");
                            }

                        }
                    }
                }
                catch (Exception es)
                {
                    MessageBox.Show(es.Message);
                }
            }
        }
    }
}
