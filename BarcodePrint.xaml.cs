﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace MobileShop_Application
{
    /// <summary>
    /// Interaction logic for BarcodePrint.xaml
    /// </summary>
    public partial class BarcodePrint : Window
    {
        public BarcodeDataSet dInvoice;
        public BarcodePrint()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ReportDocument report = new ReportDocument();
            report.Load("BarcodePrint.rpt");
            report.SetDataSource(dInvoice);
            billPrint.ViewerCore.ReportSource = report;
            billPrint.ToggleSidePanel = SAPBusinessObjects.WPF.Viewer.Constants.SidePanelKind.None;
        }
    }
}
