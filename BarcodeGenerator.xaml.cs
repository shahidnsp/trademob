﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using System.Collections.ObjectModel;
using MobileShop_Application.Class;
using System.ComponentModel;
using System.IO;
using System.Windows.Interop;
using System.Runtime.InteropServices;
using System.Windows.Media.Animation;
using System.Drawing.Text;

namespace MobileShop_Application
{
    /// <summary>
    /// Interaction logic for BarcodeGenerator.xaml
    /// </summary>
    public partial class BarcodeGenerator : Window
    {
        private BarcodeLib.Barcode b = new BarcodeLib.Barcode();
        private BarcodeDataSet dInvoice = new BarcodeDataSet();
        public ObservableCollection<ProductList> List { get; set; }
        public List<int> list = new List<int>();
        public BarcodeGenerator()
        {
            InitializeComponent();
        }
        public class ProductList
        {
            public string item_name { get; set; }
            public string item_id { get; set; }
            public bool ischecked { get; set; }
            public string barcode { get; set; }
            public string sales_rate { get; set; }
            public string qty { get; set; }
        }

        public void loadItems()
        {
            foreach (var id in list)
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(DBCon.conStr))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand("SELECT Id,name,barcode,sale_rate FROM Item WHERE active=1 AND Id="+id))
                        {
                            cmd.Connection = con;
                            List = new ObservableCollection<ProductList>();
                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                if (dr.Read())
                                {
                                    //List.Add(new ProductList { item_name = dr["item_name"].ToString(), item_id = dr["Id"].ToString(), barcode = dr["barcode"].ToString(), ischecked = false, sales_rate = dr["sales_rate"].ToString() });
                                    lstbProduct.Items.Add(new ProductList()
                                    {
                                        item_name = dr["name"].ToString(),
                                        item_id = dr["Id"].ToString(),
                                        barcode = dr["barcode"].ToString(),
                                        ischecked = true,
                                        sales_rate = dr["sale_rate"].ToString(),
                                        qty = "1"
                                    });
                                }
                                
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            this.DataContext = this;
        }
        private void chkProduct_Click(object sender, RoutedEventArgs e)
        {
            CheckBox chk = (sender) as CheckBox;
            if (chk.IsChecked == true)
                chk.Foreground = new SolidColorBrush(Colors.Blue);
            else
                chk.Foreground = new SolidColorBrush(Colors.Black);
        }

        private void fillRotateFlip()
        {
            this.cmbRotateFlip.Items.Add(System.Drawing.RotateFlipType.RotateNoneFlipNone);
            this.cmbRotateFlip.Items.Add(System.Drawing.RotateFlipType.Rotate180FlipNone);
            this.cmbRotateFlip.Items.Add(System.Drawing.RotateFlipType.Rotate180FlipX);
            this.cmbRotateFlip.Items.Add(System.Drawing.RotateFlipType.Rotate180FlipXY);
            this.cmbRotateFlip.Items.Add(System.Drawing.RotateFlipType.Rotate180FlipY);
            this.cmbRotateFlip.Items.Add(System.Drawing.RotateFlipType.Rotate270FlipNone);
            this.cmbRotateFlip.Items.Add(System.Drawing.RotateFlipType.Rotate270FlipX);
            this.cmbRotateFlip.Items.Add(System.Drawing.RotateFlipType.Rotate270FlipXY);
            this.cmbRotateFlip.Items.Add(System.Drawing.RotateFlipType.Rotate270FlipY);
            this.cmbRotateFlip.Items.Add(System.Drawing.RotateFlipType.Rotate90FlipNone);
            this.cmbRotateFlip.Items.Add(System.Drawing.RotateFlipType.Rotate90FlipX);
            this.cmbRotateFlip.Items.Add(System.Drawing.RotateFlipType.Rotate90FlipXY);
            this.cmbRotateFlip.Items.Add(System.Drawing.RotateFlipType.Rotate90FlipY);
            this.cmbRotateFlip.Items.Add(System.Drawing.RotateFlipType.RotateNoneFlipX);
            this.cmbRotateFlip.Items.Add(System.Drawing.RotateFlipType.RotateNoneFlipXY);
            this.cmbRotateFlip.Items.Add(System.Drawing.RotateFlipType.RotateNoneFlipY);
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DBCon con = new DBCon();
            
            //this.cmbRotateFlip.ItemsSource = System.Enum.GetName(typeof(System.Drawing.RotateFlipType),3);
            fillRotateFlip();
            this.cmbRotateFlip.SelectedIndex = 0;
            int i = 0;
            foreach (object o in cmbRotateFlip.Items)
            {
                if (o.ToString().Trim().ToLower() == "rotatenoneflipnone")
                    break;
                i++;
            }//foreach
            fontFamily();
            fillComboBox(this.cmbItem, "SELECT Id,name FROM Item WHERE active=1","Item", "name", "Id", "All");
        }

        private void fontFamily()
        {
            InstalledFontCollection fonts = new InstalledFontCollection();
            FontFamily famity = new FontFamily("Arial");
            System.Drawing.FontFamily[] fontFamilies;
            fontFamilies = fonts.Families;
            cmbFont.Items.Clear();

            int count = fontFamilies.Length;
            for (int i = 0; i < count; i++)
            {
                cmbFont.Items.Add(fontFamilies[i].Name);
            }

            cmbFont.Text = "Arial";
        }



     

        private bool fillComboBox(System.Windows.Controls.ComboBox combobox, string procedureName, string dTable, string dDisplay, string dValue, string defaultValue)
        {
            SqlCommand sqlcmd = new SqlCommand();
            SqlDataAdapter sqladp = new SqlDataAdapter();
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection _sqlconTeam = new SqlConnection(DBCon.conStr))
                {
                    sqlcmd.Connection = _sqlconTeam;
                    sqlcmd.CommandText = procedureName;
                    _sqlconTeam.Open();
                    sqladp.SelectCommand = sqlcmd;
                    sqladp.Fill(ds, dTable);
                    DataRow nRow = ds.Tables[dTable].NewRow();
                    nRow[dDisplay] = defaultValue;
                    nRow[dValue] = "-1";
                    ds.Tables[dTable].Rows.InsertAt(nRow, 0);
                    combobox.ItemsSource = ds.Tables[dTable].DefaultView;
                    combobox.DisplayMemberPath = ds.Tables[dTable].Columns[1].ToString();
                    combobox.SelectedValuePath = ds.Tables[dTable].Columns[0].ToString();
                    combobox.SelectedIndex = 0;

                }
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                return false;
            }
            finally
            {
                sqladp.Dispose();
                sqlcmd.Dispose();
            }
        }


        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool DeleteObject(IntPtr value);
        public BitmapSource GetImageStream(System.Drawing.Image myImage)
        {
            var bitmap = new System.Drawing.Bitmap(myImage);
            IntPtr bmpPt = bitmap.GetHbitmap();
            BitmapSource bitmapSource =
             System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                   bmpPt,
                   IntPtr.Zero,
                   Int32Rect.Empty,
                   BitmapSizeOptions.FromEmptyOptions());

            //freeze bitmapSource and clear memory to avoid memory leaks
            bitmapSource.Freeze();
            DeleteObject(bmpPt);

            return bitmapSource;
        }
        private void btnEncode_Click(object sender, RoutedEventArgs e)
        {
            int W = Convert.ToInt32(this.txtWidth.Text.Trim());
            int H = Convert.ToInt32(this.txtHeight.Text.Trim());
            b.Alignment = BarcodeLib.AlignmentPositions.CENTER;

            //barcode alignment
            switch (cmbBarcodeAlign.Text.ToLower())
            {
                case "left": b.Alignment = BarcodeLib.AlignmentPositions.LEFT; break;
                case "right": b.Alignment = BarcodeLib.AlignmentPositions.RIGHT; break;
                default: b.Alignment = BarcodeLib.AlignmentPositions.CENTER; break;
            }//switch

            BarcodeLib.TYPE type = BarcodeLib.TYPE.UNSPECIFIED;
            switch (cmbEncodeType.Text)
            {
                case "UPC-A": type = BarcodeLib.TYPE.UPCA; break;
                case "UPC-E": type = BarcodeLib.TYPE.UPCE; break;
                case "UPC 2 Digit Ext.": type = BarcodeLib.TYPE.UPC_SUPPLEMENTAL_2DIGIT; break;
                case "UPC 5 Digit Ext.": type = BarcodeLib.TYPE.UPC_SUPPLEMENTAL_5DIGIT; break;
                case "EAN-13": type = BarcodeLib.TYPE.EAN13; break;
                case "JAN-13": type = BarcodeLib.TYPE.JAN13; break;
                case "EAN-8": type = BarcodeLib.TYPE.EAN8; break;
                case "ITF-14": type = BarcodeLib.TYPE.ITF14; break;
                case "Codabar": type = BarcodeLib.TYPE.Codabar; break;
                case "PostNet": type = BarcodeLib.TYPE.PostNet; break;
                case "Bookland/ISBN": type = BarcodeLib.TYPE.BOOKLAND; break;
                case "Code 11": type = BarcodeLib.TYPE.CODE11; break;
                case "Code 39": type = BarcodeLib.TYPE.CODE39; break;
                case "Code 39 Extended": type = BarcodeLib.TYPE.CODE39Extended; break;
                case "Code 39 Mod 43": type = BarcodeLib.TYPE.CODE39_Mod43; break;
                case "Code 93": type = BarcodeLib.TYPE.CODE93; break;
                case "LOGMARS": type = BarcodeLib.TYPE.LOGMARS; break;
                case "MSI": type = BarcodeLib.TYPE.MSI_Mod10; break;
                case "Interleaved 2 of 5": type = BarcodeLib.TYPE.Interleaved2of5; break;
                case "Standard 2 of 5": type = BarcodeLib.TYPE.Standard2of5; break;
                case "Code 128": type = BarcodeLib.TYPE.CODE128; break;
                case "Code 128-A": type = BarcodeLib.TYPE.CODE128A; break;
                case "Code 128-B": type = BarcodeLib.TYPE.CODE128B; break;
                case "Code 128-C": type = BarcodeLib.TYPE.CODE128C; break;
                case "Telepen": type = BarcodeLib.TYPE.TELEPEN; break;
                case "FIM": type = BarcodeLib.TYPE.FIM; break;
                case "Pharmacode": type = BarcodeLib.TYPE.PHARMACODE; break;
                default: MessageBox.Show("Please specify the encoding type."); break;
            }//switch

            try
            {
                if (type != BarcodeLib.TYPE.UNSPECIFIED)
                {
                    if (this.chkGenerateLabel.IsChecked == true)
                        b.IncludeLabel = true;

                    b.RotateFlipType = (System.Drawing.RotateFlipType)Enum.Parse(typeof(System.Drawing.RotateFlipType), this.cmbRotateFlip.SelectedItem.ToString(), true);

                    if (txtAlternative.IsEnabled == true)
                    {
                        if (txtAlternative.Text != String.Empty)
                        {
                            if (chkPrice.IsChecked == true)
                                b.AlternateLabel = txtAlternative.Text + "             " + "Price:₹15000";
                            else
                                b.AlternateLabel = txtAlternative.Text;
                        }
                    }

                    if (chkItem.IsChecked == true)
                    {
                        if (chkPrice.IsChecked == true)
                            b.AlternateLabel = "Item Name" + "             " + "Price:₹15000";
                        else
                            b.AlternateLabel = "                            " + "Price:₹15000";
                    }
                    else if (chkPrice.IsChecked == true)
                    {

                        if (chkGenerateLabel.IsChecked == true)
                            b.AlternateLabel = txtData.Text + "             " + "Price:₹15000";
                        else
                            b.AlternateLabel = "                            " + "Price:₹15000";
                    }
                    else
                    {
                        b.IncludeLabel = true;
                    }



                    //label alignment and position
                    switch (this.cmbLabelLocation.Text.Trim().ToUpper())
                    {
                        case "BOTTOMLEFT": b.LabelPosition = BarcodeLib.LabelPositions.BOTTOMLEFT; break;
                        case "BOTTOMRIGHT": b.LabelPosition = BarcodeLib.LabelPositions.BOTTOMRIGHT; break;
                        case "TOPCENTER": b.LabelPosition = BarcodeLib.LabelPositions.TOPCENTER; break;
                        case "TOPLEFT": b.LabelPosition = BarcodeLib.LabelPositions.TOPLEFT; break;
                        case "TOPRIGHT": b.LabelPosition = BarcodeLib.LabelPositions.TOPRIGHT; break;
                        default: b.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER; break;
                    }//switch

                    System.Windows.Media.Color cf = (System.Windows.Media.Color)pkrForeground.SelectedColor;
                    var fore = System.Drawing.Color.FromArgb(cf.A, cf.R, cf.G, cf.B);
                    System.Windows.Media.Color cb = (System.Windows.Media.Color)pkrBackground.SelectedColor;
                    var back = System.Drawing.Color.FromArgb(cb.A, cb.R, cb.G, cb.B);
                    //===== Encoding performed here =====


                    //barcode.BackgroundImage = b.Encode(type, this.txtData.Text.Trim(), fore, back, W, H);
                    barcode.Source = GetImageStream(b.Encode(type, this.txtData.Text.Trim(), fore, back, W, H));
                    //===================================

                    //show the encoding time
                    // this.lblEncodingTime.Text = "(" + Math.Round(b.EncodingTime, 0, MidpointRounding.AwayFromZero).ToString() + "ms)";

                    txtEncoded.Text = b.EncodedValue;

                    // tsslEncodedType.Text = "Encoding Type: " + b.EncodedType.ToString();
                }//if

                //reposition the barcode image to the middle
                //barcode.Location = new Point((this.barcode.Location.X + this.barcode.Width / 2) - barcode.Width / 2, (this.barcode.Location.Y + this.barcode.Height / 2) - barcode.Height / 2);
            }//try
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }//catch
        }

        private void btnGenerate_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (generateBarcode() == true)
            {
                BarcodePrint barcode = new BarcodePrint();
                barcode.dInvoice = this.dInvoice;
                barcode.ShowDialog();
            }
        }

        private bool generateBarcode()
        {
            dInvoice.Tables["DataTable1"].Rows.Clear();
            foreach (var item in lstbProduct.Items)
            {
                var itm = item as ProductList;
                int qty = 1;
                if (itm != null)
                {
                    if (itm.ischecked == true)
                    {

                        int W = Convert.ToInt32(this.txtWidth.Text.Trim());
                        int H = Convert.ToInt32(this.txtHeight.Text.Trim());
                        b.Alignment = BarcodeLib.AlignmentPositions.CENTER;

                        //barcode alignment
                        switch (cmbBarcodeAlign.Text.ToLower())
                        {
                            case "left": b.Alignment = BarcodeLib.AlignmentPositions.LEFT; break;
                            case "right": b.Alignment = BarcodeLib.AlignmentPositions.RIGHT; break;
                            default: b.Alignment = BarcodeLib.AlignmentPositions.CENTER; break;
                        }//switch

                        BarcodeLib.TYPE type = BarcodeLib.TYPE.UNSPECIFIED;

                        switch (cmbEncodeType.Text)
                        {
                            case "UPC-A": type = BarcodeLib.TYPE.UPCA; break;
                            case "UPC-E": type = BarcodeLib.TYPE.UPCE; break;
                            case "UPC 2 Digit Ext.": type = BarcodeLib.TYPE.UPC_SUPPLEMENTAL_2DIGIT; break;
                            case "UPC 5 Digit Ext.": type = BarcodeLib.TYPE.UPC_SUPPLEMENTAL_5DIGIT; break;
                            case "EAN-13": type = BarcodeLib.TYPE.EAN13; break;
                            case "JAN-13": type = BarcodeLib.TYPE.JAN13; break;
                            case "EAN-8": type = BarcodeLib.TYPE.EAN8; break;
                            case "ITF-14": type = BarcodeLib.TYPE.ITF14; break;
                            case "Codabar": type = BarcodeLib.TYPE.Codabar; break;
                            case "PostNet": type = BarcodeLib.TYPE.PostNet; break;
                            case "Bookland/ISBN": type = BarcodeLib.TYPE.BOOKLAND; break;
                            case "Code 11": type = BarcodeLib.TYPE.CODE11; break;
                            case "Code 39": type = BarcodeLib.TYPE.CODE39; break;
                            case "Code 39 Extended": type = BarcodeLib.TYPE.CODE39Extended; break;
                            case "Code 39 Mod 43": type = BarcodeLib.TYPE.CODE39_Mod43; break;
                            case "Code 93": type = BarcodeLib.TYPE.CODE93; break;
                            case "LOGMARS": type = BarcodeLib.TYPE.LOGMARS; break;
                            case "MSI": type = BarcodeLib.TYPE.MSI_Mod10; break;
                            case "Interleaved 2 of 5": type = BarcodeLib.TYPE.Interleaved2of5; break;
                            case "Standard 2 of 5": type = BarcodeLib.TYPE.Standard2of5; break;
                            case "Code 128": type = BarcodeLib.TYPE.CODE128; break;
                            case "Code 128-A": type = BarcodeLib.TYPE.CODE128A; break;
                            case "Code 128-B": type = BarcodeLib.TYPE.CODE128B; break;
                            case "Code 128-C": type = BarcodeLib.TYPE.CODE128C; break;
                            case "Telepen": type = BarcodeLib.TYPE.TELEPEN; break;
                            case "FIM": type = BarcodeLib.TYPE.FIM; break;
                            case "Pharmacode": type = BarcodeLib.TYPE.PHARMACODE; break;
                            default: MessageBox.Show("Please specify the encoding type."); break;
                        }//switch

                        try
                        {
                            if (type != BarcodeLib.TYPE.UNSPECIFIED)
                            {
                                if (this.chkGenerateLabel.IsChecked == true)
                                    b.IncludeLabel = true;

                                b.RotateFlipType = (System.Drawing.RotateFlipType)Enum.Parse(typeof(System.Drawing.RotateFlipType), this.cmbRotateFlip.SelectedItem.ToString(), true);

                                if (txtAlternative.IsEnabled == true)
                                {
                                    if (txtAlternative.Text != String.Empty)
                                    {
                                        if (chkPrice.IsChecked == true)
                                            b.AlternateLabel = txtAlternative.Text + " " + "₹" +ConvertToEncryprt.ConvertNumberToEncrypt(itm.sales_rate);
                                        else
                                            b.AlternateLabel = txtAlternative.Text;
                                    }
                                }

                                if (chkItem.IsChecked == true)
                                {
                                    if (chkPrice.IsChecked == true)
                                        b.AlternateLabel = itm.barcode.Trim() + " " + itm.item_name + " " + "₹" + ConvertToEncryprt.ConvertNumberToEncrypt(itm.sales_rate);
                                    else
                                        b.AlternateLabel = itm.barcode.Trim() + " " + itm.item_name;
                                }
                                else if (chkPrice.IsChecked == true)
                                {

                                    if (chkGenerateLabel.IsChecked == true)
                                        b.AlternateLabel = itm.barcode.Trim() + " " + "₹" + ConvertToEncryprt.ConvertNumberToEncrypt(itm.sales_rate);
                                    else
                                        b.AlternateLabel = " " + "₹" + ConvertToEncryprt.ConvertNumberToEncrypt(itm.sales_rate);
                                }
                                else
                                {
                                    b.IncludeLabel = true;
                                }



                                //label alignment and position
                                switch (this.cmbLabelLocation.Text.Trim().ToUpper())
                                {
                                    case "BOTTOMLEFT": b.LabelPosition = BarcodeLib.LabelPositions.BOTTOMLEFT; break;
                                    case "BOTTOMRIGHT": b.LabelPosition = BarcodeLib.LabelPositions.BOTTOMRIGHT; break;
                                    case "TOPCENTER": b.LabelPosition = BarcodeLib.LabelPositions.TOPCENTER; break;
                                    case "TOPLEFT": b.LabelPosition = BarcodeLib.LabelPositions.TOPLEFT; break;
                                    case "TOPRIGHT": b.LabelPosition = BarcodeLib.LabelPositions.TOPRIGHT; break;
                                    default: b.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER; break;
                                }//switch


                                try
                                {
                                    qty = Convert.ToInt32(itm.qty);
                                }
                                catch { }

                                System.Windows.Media.Color cf = (System.Windows.Media.Color)pkrForeground.SelectedColor;
                                var fore = System.Drawing.Color.FromArgb(cf.A, cf.R, cf.G, cf.B);

                                System.Windows.Media.Color cb = (System.Windows.Media.Color)pkrBackground.SelectedColor;
                                var back = System.Drawing.Color.FromArgb(cb.A, cb.R, cb.G, cb.B);
                                b.LabelFont = new System.Drawing.Font(cmbFont.Text, Convert.ToInt32(txtFontSize.Text));

                                for (int i = 1; i <= qty; i++)
                                {
                                    DataRow dr = dInvoice.Tables["DataTable1"].NewRow();

                                    dr["item_name"] = itm.item_name.ToString();
                                    dr["barcode"] = imageToByteArray(b.Encode(type, itm.barcode.Trim(), fore, back, W, H));
                                    dr["name"] = txtHeader.Text;
                                    txtEncoded.Text = b.EncodedValue;

                                    dInvoice.Tables["DataTable1"].Rows.Add(dr);
                                }

                            }//if

                        }//try
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }//catch


                    }
                }
            }
            return true;
        }

        private Byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                return ms.ToArray();
            }
        }

        private void chkItem_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (chkItem.IsChecked == true)
                txtAlternative.IsEnabled = false;
            else
                txtAlternative.IsEnabled = true;
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            //Closing -= Window_Closing;
            //e.Cancel = true;
            //var anim = new DoubleAnimation(0, (Duration)TimeSpan.FromSeconds(0.5));
            //anim.Completed += (s, _) => this.Close();
            //this.BeginAnimation(UIElement.OpacityProperty, anim);
        }

        private void cmbItem_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (cmbItem.SelectedIndex == 0)
            {              
                    //lstbProduct.Items.Clear();
                    try
                    {
                        using (SqlConnection con = new SqlConnection(DBCon.conStr))
                        {
                            con.Open();
                            using (SqlCommand cmd = new SqlCommand("SELECT Id,name,barcode,sale_rate FROM Item WHERE active=1"))
                            {
                                cmd.Connection = con;
                                List = new ObservableCollection<ProductList>();
                                using (SqlDataReader dr = cmd.ExecuteReader())
                                {
                                    while (dr.Read())
                                    {
                                        //List.Add(new ProductList { item_name = dr["item_name"].ToString(), item_id = dr["Id"].ToString(), barcode = dr["barcode"].ToString(), ischecked = false, sales_rate = dr["sales_rate"].ToString() });
                                        lstbProduct.Items.Add(new ProductList()
                                        {
                                            item_name = dr["name"].ToString(),
                                            item_id = dr["Id"].ToString(),
                                            barcode = dr["barcode"].ToString(),
                                            ischecked = true,
                                            sales_rate = dr["sale_rate"].ToString(),
                                            qty = "1"
                                        });


                                    }
                                    this.DataContext = this;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            else
            {
                //lstbProduct.Items.Clear();
                try
                {
                    using (SqlConnection con = new SqlConnection(DBCon.conStr))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand("SELECT Id,name,barcode,sale_rate FROM Item WHERE active=1 AND Id=" + cmbItem.SelectedValue))
                        {
                            cmd.Connection = con;

                            List = new ObservableCollection<ProductList>();
                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    //List.Add(new ProductList { item_name = dr["item_name"].ToString(), item_id = dr["Id"].ToString(), barcode = dr["barcode"].ToString(), ischecked = false, sales_rate = dr["sales_rate"].ToString() });
                                    lstbProduct.Items.Add(new ProductList()
                                    {
                                        item_name = dr["name"].ToString(),
                                        item_id = dr["Id"].ToString(),
                                        barcode = dr["barcode"].ToString(),
                                        ischecked = true,
                                        sales_rate = dr["sale_rate"].ToString(),
                                        qty = "1"
                                    });


                                }
                                this.DataContext = this;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            lstbProduct.Items.Clear();
        }
    }
}
