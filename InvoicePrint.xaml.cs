﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using MobileShop_Application.Class;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace MobileShop_Application
{
    /// <summary>
    /// Interaction logic for InvoicePrint.xaml
    /// </summary>
    public partial class InvoicePrint : Window
    {
        public string invoiceID = null;
        private InvoiceDataSet dInvoice = new InvoiceDataSet();
        public InvoicePrint()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            dInvoice.Tables["DataTable1"].Rows.Clear();
            dInvoice.Tables["DataTable2"].Rows.Clear();
            dInvoice.Tables["DataTable3"].Rows.Clear();
            dInvoice.Tables["DataTable4"].Rows.Clear();

                fillCompanyProfile();
                fillInvoice();
                fillInvoiceItem();
                PrintDialog print = new PrintDialog();
                if (print.ShowDialog() == true)
                {
                    ReportDocument report = new ReportDocument();              
                    report.Load("Invoice.rpt");
                    report.SetDataSource(dInvoice);
                    //report.PrintOptions.PrinterName = printer;
                    report.PrintOptions.PaperSize = (CrystalDecisions.Shared.PaperSize)System.Drawing.Printing.PaperKind.A4;
                    report.PrintToPrinter(1, false, 0, 0);
                    billPrint.ViewerCore.ReportSource = report;
                    billPrint.ToggleSidePanel = SAPBusinessObjects.WPF.Viewer.Constants.SidePanelKind.None;
                 }

                 this.Close();               
        }
        /*
        * To Fill Company Details of Invoice
        * 
        * 
        * 
        * */
        private void fillCompanyProfile()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.GET_COMPANY))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter dAdapter = new SqlDataAdapter(cmd))
                        {
                            dAdapter.Fill(dInvoice.Tables["DataTable3"]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*
         * To fill Invoice details
         * */
        private void fillInvoice()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.GET_SALES_INVOICE))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Id", this.invoiceID);
                   
                        using (SqlDataReader rd = cmd.ExecuteReader())
                        {
                            while (rd.Read())
                            {
                                DataRow dr = dInvoice.Tables["DataTable1"].NewRow();
                                dr["Id"] = rd["Id"].ToString();
                                dr["invoiceno"] = rd["invoice_no"].ToString();
                                dr["invoice_date"] = rd["invoice_date"].ToString();                               
                                dr["customer"] = rd["customer"].ToString();                              
                                dr["gross_amount"] = rd["gross_amount"].ToString();
                                fillword(rd["gross_amount"].ToString());
                                dr["discount"] = rd["discount"].ToString();                                                            
                                dr["additional"] = rd["additional"].ToString();
                                dr["paid"] = "";
                                dr["balance"] = rd["balance"].ToString();                              
                                dInvoice.Tables["DataTable1"].Rows.Add(dr);
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*
         * To fill Invoice Items
         * */
        private void fillInvoiceItem()
        {
            int i = 1;
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.GET_SALES_INVOICE_ITEMS))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Id", this.invoiceID);
                        using (SqlDataReader rd = cmd.ExecuteReader())
                        {
                            while (rd.Read())
                            {
                                DataRow dr = dInvoice.Tables["DataTable2"].NewRow();
                                dr["Id"] = rd["Id"].ToString();
                                dr["invoice_id"] = rd["sales_invoice_id"].ToString();
                                dr["slno"] = i;
                                dr["item_name"] = rd["item"].ToString();
                                dr["barcode"] = rd["barcode"].ToString();                           
                                dr["rate"] = rd["sale_rate"].ToString();
                                dr["discount"] = rd["discount"].ToString();                             
                                dr["net"] = rd["gross"].ToString();
                                i++;
                                dInvoice.Tables["DataTable2"].Rows.Add(dr);
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void fillword(string amt)
        {
              DataRow dr = dInvoice.Tables["DataTable4"].NewRow();
              NumberToWord obj = new NumberToWord();
              dr["word"] = obj.changeCurrencyToWords(amt);
              dInvoice.Tables["DataTable4"].Rows.Add(dr);
        }   
    }
}
