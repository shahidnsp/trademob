﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using MobileShop_Application.Class;

namespace MobileShop_Application
{
    /// <summary>
    /// Interaction logic for UserRegistration.xaml
    /// </summary>
    public partial class UserRegistration : Window
    {
        private string editUsername = "";
        public UserRegistration()
        {
            InitializeComponent();
            txtUsername.Focus();
            loadData();
            addHotKeys();
        }
        private void addHotKeys()
        {
            RoutedCommand saveCmd = new RoutedCommand();
            saveCmd.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(saveCmd, btnSave_Click));

            RoutedCommand delCmd = new RoutedCommand();
            delCmd.InputGestures.Add(new KeyGesture(Key.D, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(delCmd, btnDelete_Click));
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        class Users
        {
            public string username { get; set; }
            public string password { get; set; }
        }
        private void loadData()
        {
            lstvUser.Items.Clear();
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.loginString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("SELECT * FROM UserLogin WHERE type!='P'"))
                    {
                        cmd.Connection = con;
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {

                                lstvUser.Items.Add(new Users
                                {
                                    username = dr["username"].ToString(),
                                    password = dr["password"].ToString()
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if(txtUsername.Text==String.Empty)
            {
                MessageBox.Show("Username Required...!!");
                return;
            }

            if (txtPassword.Text == String.Empty)
            {
                MessageBox.Show("Password Required...!!");
                return;
            }

            if (txtPassword.Text != txtRePassword.Text)
            {
                MessageBox.Show("Password doesn't match..!!");
                return;
            }

            if (btnSave.Content.ToString() == "_Save")
            {
                if (MessageBox.Show("Are you want to save..??", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    try
                    {
                        using (SqlConnection con = new SqlConnection(DBCon.loginString))
                        {
                            con.Open();
                            using (SqlCommand cmd = new SqlCommand("INSERT INTO UserLogin('" + txtUsername.Text + "','" + txtPassword.Text + ",'A')"))
                            {
                                cmd.Connection = con;
                                if (cmd.ExecuteNonQuery() > 0)
                                {
                                    MessageBox.Show("Registration Completed");
                                    loadData();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
            else
            {
                if (MessageBox.Show("Are you want to Update..??", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    try
                    {
                        using (SqlConnection con = new SqlConnection(DBCon.loginString))
                        {
                            con.Open();
                            using (SqlCommand cmd = new SqlCommand("UPDATE UserLogin SET username='" + txtUsername.Text + "',password='" + txtPassword.Text + "' WHERE username='" + editUsername + "'"))
                            {
                                cmd.Connection = con;
                                if (cmd.ExecuteNonQuery() > 0)
                                {
                                    MessageBox.Show("Registration Update Completed");
                                    loadData();
                                    btnSave.Content = "_Save";
                                    btnDelete.IsEnabled = false;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Are you want to Delete..??", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(DBCon.loginString))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand("DELETE FROM UserLogin WHERE username='" + editUsername + "')"))
                        {
                            cmd.Connection = con;
                            if (cmd.ExecuteNonQuery() > 0)
                            {
                                MessageBox.Show("User Deletion Completed");
                                loadData();
                                btnSave.Content = "_Save";
                                btnDelete.IsEnabled = false;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void lstvUser_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var item = lstvUser.Items.GetItemAt(lstvUser.SelectedIndex) as Users;
                editUsername = item.username;
                txtUsername.Text = item.username;
                btnSave.Content = "_Update";
                btnDelete.IsEnabled = true;
            }
            catch { }
        }
    }
}
