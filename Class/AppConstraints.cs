﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileShop_Application.Class
{
    class AppConstraints
    {
        //Stored Procedure for Item Insertion
        public const string INSERT_ITEM = "Insert_Item";
        //stored procedure for Brand Insertion
        public const string INSERT_BRAND = "Insert_Brand";
        //stored procedure for Customer Insertion
        public const string INSERT_CUSTOMER = "Insert_Customer";
        //stored procedure for Sales_Invoice Insertion
        public const string INSERT_SALES_INVOICE = "Insert_Sales_Invoice";
        //stored procedure for Purchase_Invoice Insertion
        public const string INSERT_PURCHASE_INVOICE = "Insert_Purchase_Invoice";
        //stored procedure for Purchase_Invoice_Item Insertion
        public const string INSERT_PURCHASE_INVOICE_ITEM = "Insert_Purchase_Invoice_Item";
        //stored procedure for Sale_Invoice_Item Insertion
        public const string INSERT_SALE_INVOICE_ITEM = "Insert_Sale_Invoice_Item";
        //stored procedure for Income Insertion
        public const string INSERT_INCOME = "Insert_Income";
        //stored procedure for Expense Insertion
        public const string INSERT_EXPENSE = "Insert_Expense";
        //stored procedure for Tax_rate Insertion
        public const string INSERT_TAX_RATE = "Insert_Tax";
        //stored procedure for Unit Insertion
        public const string INSERT_UNIT_DATA = "Insert_Tax";
        //stored procedure for StorageLocation Insertion
        public const string INSERT_STORAGE_LOCATION = "Insert_Storage_Location";
     

        //Stored Procedure for fill Brand ComboBox
        public const string COMBO_BRAND = "Combo_Brand";
        //Stored Procedure for fill Customer Combobox
        public const string COMBO_CUSTOMER = "Combo_Customer";
        //Stored Procedure for fill Customer Combobox
        public const string COMBO_BUYER = "Combo_Buyer";
        //Stored Procedure for fill Item Combobox
        public const string COMBO_ITEM = "Combo_Item";
        //Stored Procedure for fill Tax Combobox
        public const string COMBO_TAX = "Combo_Tax_Rate";
        //Stored Procedure for fill Location Combobox
        public const string COMBO_LOCATION = "Combo_Location";
        //Stored Procedure for fill Unit Combobox
        public const string COMBO_UNIT = "Combo_Unit"; 

        //Stored procedure for Delete Customer
        public const string DELETE_CUSTOMER = "Delete_Customer";
        //Stored procedure for Delete Item
        public const string DELETE_ITEM = "Delete_Item";
        //Stored procedure for Delete Brand
        public const string DELETE_BRAND = "Delete_Brand";
        //Stored procedure for Delete Sales Invoice Item
        public const string DELETE_SALES_INVOICE_ITEM = "Delete_Sales_Invoice_Item";
        //Stored procedure for Delete purchase Invoice Item
        public const string DELETE_PURCHASE_INVOICE_ITEM = "Delete_Purchase_Invoice_Item";
        //Stored procedure for Delete Sales Invoice 
        public const string DELETE_SALES_INVOICE = "Delete_Sales_Invoice";
        //Stored procedure for Delete Purchase Invoice 
        public const string DELETE_PURCHASE_INVOICE = "Delete_Purchase_Invoice";
        //Stored procedure for Delete Income
        public const string DELETE_INCOME = "Delete_Income";
        //Stored procedure for Delete Expense
        public const string DELETE_EXPENSE = "Delete_Expense";
        //Stored procedure for Delete Income
        public const string DELETE_INCOME_SALE = "Delete_Income_Sale";
        //Stored procedure for Delete Income
        public const string DELETE_EXPENSE_PURCHASE = "Delete_Expense_Purchase";
       
        //Stored procedure for Delete Income
        public const string DELETE_TAX = "Delete_Tax";
        //Stored procedure for Delete Unit
        public const string DELETE_UNIT = "Delete_Unit";
        //Stored procedure for Delete StorageLocation
        public const string DELETE_STORAGE_LOCATION = "Delete_Storage_Location";

        //Stored procedure for Update Brand
        public const string UPDATE_BRAND = "Update_Brand";
        //Stored procedure for Update Customer
        public const string UPDATE_CUSTOMER = "Update_Customer";
        //Stored procedure for Update Customer
        public const string UPDATE_ITEM = "Update_Item";
        //stored procedure for Sales_Invoice Updation
        public const string UPDATE_SALES_INVOICE = "Update_Sales_Invoice";
        //stored procedure for Purchase_Invoice Updation
        public const string UPDATE_PURCHASE_INVOICE = "Update_Purchase_Invoice";
        //stored procedure for Key Updation
        public const string UPDATE_KEY = "Update_Key";
        //stored procedure for Background Updation
        public const string UPDATE_BACKGROUND = "Update_Background";
        //stored procedure for prefix Updation
        public const string UPDATE_INVOICE_PREPIX = "Update_InvoiceID";
        //stored procedure for company Updation
        public const string UPDATE_COMPANY = "Update_Company";
        //stored procedure for Buyer Updation
        public const string UPDATE_BUYER = "Update_Buyer";
        //stored procedure for Expense Updation
        public const string UPDATE_EXPENSE = "Update_Expense";
        //stored procedure for Expense Updation
        public const string UPDATE_INCOME = "Update_Income";
        //stored procedure for Stock Updation
        public const string UPDATE_STOCK = "Update_Stock";
        //stored procedure for Tax_rate Updation
        public const string UPDATE_TAX = "Update_Tax";
        //stored procedure for Unit Updation
        public const string UPDATE__UNIT_DETAILS = "Update_Unit";
        //stored procedure for Storage_Location Updation
        public const string UPDATE__STORAGE_LOCATION = "Update_Storage_Location";


        //Stored procedure for Brand List
        public const string LIST_VIEW_BRAND = "List_View_Brand";
        //Stored procedure for Brand List
        public const string LIST_VIEW_BRAND_WITH_NAME = "List_View_Brand_With_Name";
        public const string LIST_VIEW_CUSTOMER = "List_View_Customer";
        //Stored procedure for customer List search by name
        public const string LIST_VIEW_CUSTOMER_WITH_NAME = "List_View_Customer_With_Name";
        //Stored procedure for Item List
        public const string LIST_VIEW_ITEM = "List_View_Item";
        //Stored procedure for Item List search by model
        public const string LIST_VIEW_ITEM_WITH_MODEL = "List_View_Item_By_Model";
        //Stored procedure for Item List search by brand
        public const string LIST_VIEW_ITEM_WITH_BRAND = "List_View_Item_By_Brand";
        //Stored procedure for Item List search by imei
        public const string LIST_VIEW_ITEM_WITH_IMEI = "List_View_Item_By_Imei";
        //Stored procedure for Item List search by imei
        public const string LIST_VIEW_ITEM_WITH_BARCODE = "List_View_Item_By_Barcode";
        //Stored procedure for Item List search by imei
        public const string LIST_VIEW_ITEM_WITH_ITEM = "List_View_Item_By_Item";
        //Stored procedure for Item List search by date
        public const string LIST_VIEW_ITEM_WITH_DATE = "List_View_Item_By_Date";
        //Stored procedure for Sales List
        public const string LIST_VIEW_SALES_INVOICE = "List_View_Sales_Invoice";
        //Stored procedure for Sales List
        public const string LIST_VIEW_SALES_INVOICE_BY_INVOICENO = "List_View_Sales_Invoice_By_InvoiceNo";
        //Stored procedure for Sales List
        public const string LIST_VIEW_SALES_INVOICE_BY_CUSTOMER = "List_View_Sales_Invoice_By_Customer";
        //Stored procedure for Sales List
        public const string LIST_VIEW_SALES_INVOICE_BY_DATE = "List_View_Sales_Invoice_By_Date";
        //Stored procedure for Sales List
        public const string LIST_VIEW_SALES_INVOICE_BY_CREDIT = "List_View_Sales_Invoice_By_Credit";
        //Stored procedure for Income List
        public const string LIST_VIEW_INCOME = "List_View_Income";
        //Stored procedure for Income List
        public const string LIST_VIEW_INCOME_BY_INCOME = "List_View_Income_By_Income";
        //Stored procedure for Income List
        public const string LIST_VIEW_INCOME_WITH_DATE = "List_View_Income_With_Date";
        //Stored procedure for Expense List
        public const string LIST_VIEW_EXPENSE = "List_View_Expense";
        //Stored procedure for Expense List
        public const string LIST_VIEW_EXPENSE_BY_EXPENSE = "List_View_Expense_By_Expense";
        //Stored procedure for Income List
        public const string LIST_VIEW_EXPENSE_WITH_DATE = "List_View_Expense_With_Date";
        //Stored procedure for Tax_List
        public const string LIST_VIEW_TAX = "ListViewTax";
        //Stored procedure for ListView_Unit
        public const string LIST_VIEW_UNIT = "ListViewUnit";
        //Stored procedure for ListView_Storage_Location
        public const string LIST_VIEW_STORAGE_LOCATION = "ListViewStorageLocation";


        //Stored procedure for Select_Max_UnitId
        public const string SELECT_MAX_UNIT_ID = "Select_Max_UnitId";
        //store procedure for getting maximum value of StorageLocation
        public const string SELECT_MAX_STORAGE_LOCATION = "Select_Max_storage_LocationId";

        //Stored procedure foe Tax SoftDelete
        public const string SOFTDELETE_TAX = "SoftDelete_Tax";
        //Stored procedure for Unit SoftDelete
        public const string SOFTDELETE_UNIT = "SoftDelete_Unit";
        //Stored procedure for StorageLocation SoftDelete
        public const string SOFTDELETE_STORAGE_LOCATION = "SoftDelete_StorageLocation";

        public const string GET_INVOICE_PREPIX = "Get_Invoice_Prefix";
        public const string GET_TAX_PERCENTAGE = "Get_Tax";
        public const string GET_UNIT = "Get_Unit";
        public const string GET_PRICE = "Get_Price";
        public const string GET_QTY = "Get_Product_Qty";
        public const string GET_COMPANY = "Get_Company";
        public const string ACTIVE_ITEM = "Active_Item";
        public const string GET_SALES_INVOICE = "Get_SalesInvoice";
        public const string GET_SALES_INVOICE_ITEMS = "Get_SalesInvoice_Items";
        public const string SEARCH_ITEMS = "Search_Item";
        public const string SEARCH_ITEMS_WITH_ITEM = "Search_Item_With_Item";
    }
}
