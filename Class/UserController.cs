﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileShop_Application.Class
{   
    class UserController
    {
        //Currently logined Username................
        public static string username = null;
        public static string name = null;
        //Currently logined user type
        public static string type = null;
        //Write Privillage
        public static int write=0;
        //Update Privillage
        public static int update=0;
        //Delete Privillage
        public static int delete=0;
        //To load product type
        public static int sales = 0, purchases = 0, products = 0, employees = 0, crm = 0, accounts = 0, sms = 0, email = 0, pos = 0, barcode = 0;
        public static int uType = 0;
        public static int dualMode = 0;
        public static string theme = "TitleBarResource.xaml";
    }
}
