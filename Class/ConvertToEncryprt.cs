﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileShop_Application.Class
{
    class ConvertToEncryprt
    {
        public static string[] keys = new string[10];
        static string encrypt = "";
        public static string ConvertNumberToEncrypt(string number)
        {
            try
            {
                decimal num = Convert.ToDecimal(number);
                int n = Convert.ToInt32(num);
                string sign = "";
                encrypt = "";
                if(n<0)
                {
                    sign = "-";
                    n = Math.Abs(n);
                }
                string word = "";               
                while (n > 0)
                { // loop till there's nothing left
                    word = (n % 10).ToString(); // assign the last digit
                    encrypt += getEncrypt(word).Trim();
                    n /= 10; // "right shift" the number
                }
                return sign+Resverse(encrypt.Trim());
            }
            catch {}
            return "";
        }

        private static string Resverse(string word)
        {
            int length = word.Length;
            char[]a=word.ToCharArray();
            string temp="";
            for(int i=length-1;i>=0;i--)
                temp+=a[i];
            return temp.Trim();
        }

        public static string ConvertWordToDecrypt(string word)
        {          
            encrypt = "";
            char[] chars = word.ToCharArray();
            int length = chars.Length;
            for (int i = 0; i < length;i++)
            {
                encrypt += getDecrypt(chars[i].ToString());
            }
            return encrypt;
        }

        private static string getEncrypt(string word)
        {
            string w = "";
            switch(word)
            {
                case "0":
                    w= ConvertToEncryprt.keys[0];
                    break;
                case "1":
                    w = ConvertToEncryprt.keys[1];
                    break;
                case "2":
                    w = ConvertToEncryprt.keys[2];
                    break;
                case "3":
                    w = ConvertToEncryprt.keys[3];
                    break;
                case "4":
                    w = ConvertToEncryprt.keys[4];
                    break;
                case "5":
                    w = ConvertToEncryprt.keys[5];
                    break;
                case "6":
                    w = ConvertToEncryprt.keys[6];
                    break;
                case "7":
                    w = ConvertToEncryprt.keys[7];
                    break;
                case "8":
                    w = ConvertToEncryprt.keys[8];
                    break;
                case "9":
                    w = ConvertToEncryprt.keys[9];
                    break;

            }
            return w;
        }

        private static string getDecrypt(string word)
        {
            string w = "";
            switch (word)
            {
                case "A":
                    w = "0";
                    break;
                case "B":
                    w = "1";
                    break;
                case "C":
                    w = "2";
                    break;
                case "D":
                    w = "3";
                    break;
                case "E":
                    w = "4";
                    break;
                case "F":
                    w = "5";
                    break;
                case "G":
                    w = "6";
                    break;
                case "H":
                    w = "7";
                    break;
                case "I":
                    w = "8";
                    break;
                case "J":
                    w = "9";
                    break;

            }
            return w;
        }
    }
}
