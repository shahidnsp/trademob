﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Controls;
using System.Drawing;
using System.Windows.Controls.Primitives;
namespace MobileShop_Application.Class
{
    class DBCon
    {
        public static string loginString = "Data Source=(LocalDB)\\v11.0;AttachDbFilename=|DataDirectory|\\Login.mdf;Integrated Security=True";
        public static string conStr = "Data Source=(LocalDB)\\v11.0;AttachDbFilename=D:\\TradeMobGit\\MobileShop_DB.mdf;Integrated Security=True";
     
        public SqlConnection con;
        
        Class.Message msg = new Message();
        public DBCon()
        {
           
        }

        public string getTotal(string procedure)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(procedure))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.Read())
                            {
                                return dr[0].ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
            return "0.00";
        }

        public string getTotal(string procedure,string from,string to)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(procedure))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@from",from);
                        cmd.Parameters.AddWithValue("@to", to);
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.Read())
                            {
                                return dr[0].ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
            return "0.00";
        }




        /*
         * This function is used to INSERT datas into the database table.
         * 
         * All the database tables are handle with this function.
         * 
         * This function accept 3 parameters.......
         * 1.procedureName:- the name of procedure that stored in database.
         * 2.param:- All the parameters are passed as a SqlParameter object array
         * 3.data:- Array of data to be inserted/updated into database on the order of parameter
         * 
         * 
         * */
        public void insertData(string procedureName,SqlParameter []param)
        {
            //SqlTransaction trans=null;
            try
            {
                //To get how many data are present in Database
                int countParam = param.Count();

                using (con=new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    //trans = con.BeginTransaction("InsertTransaction");
                    
                    using (SqlCommand cmd = new SqlCommand(procedureName))
                    {
                        cmd.Connection = con;
                       // cmd.CommandTimeout = 0;
                        //cmd.Transaction = trans;
                        cmd.CommandType = CommandType.StoredProcedure;

                        for (int i = 0; i < countParam; i++)
                        {
                            //Each data passed into that respective parameter...
                            cmd.Parameters.Add(param[i]);
                        }
                        cmd.ExecuteNonQuery();
                        msg.setMessageBox("Data saved Successfully", 1);
                       // trans.Commit();
                    }

                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                //trans.Rollback();
            }
            
        }


        public void insertInvoice(string procedureName, SqlParameter[] param)
        {
            //SqlTransaction trans=null;
            try
            {
                //To get how many data are present in Database
                int countParam = param.Count();

                using (con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    //trans = con.BeginTransaction("InsertTransaction");

                    using (SqlCommand cmd = new SqlCommand(procedureName))
                    {
                        cmd.Connection = con;
                        // cmd.CommandTimeout = 0;
                        //cmd.Transaction = trans;
                        cmd.CommandType = CommandType.StoredProcedure;

                        for (int i = 0; i < countParam; i++)
                        {
                            //Each data passed into that respective parameter...
                            cmd.Parameters.Add(param[i]);
                        }
                        cmd.ExecuteNonQuery();
                    }

                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                //trans.Rollback();
            }

        }




        /*
         * This function is used to MODIFY datas into the database table.
         * 
         * All the database tables are handle with this function.
         * 
         * This function accept 3 parameters.......
         * 1.procedureName:- the name of procedure that stored in database.
         * 2.param:- All the parameters are passed as a SqlParameter object array
         * 3.data:- Array of data to be inserted/updated into database on the order of parameter
         * 
         * 
         * */
        public void modifyData(string procedureName, SqlParameter[] param)
        {
           // SqlTransaction trans = null;
            try
            {
                //To get how many data are present in Database
                int countParam = param.Count();

                using (con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    //trans = con.BeginTransaction("UpdateTransaction");

                    using (SqlCommand cmd = new SqlCommand(procedureName))
                    {
                        cmd.Connection = con;
                       // cmd.Transaction = trans;
                        cmd.CommandType = CommandType.StoredProcedure;

                        for (int i = 0; i < countParam; i++)
                        {
                            //Each data passed into that respective parameter...
                            cmd.Parameters.Add(param[i]);
                        }
                        cmd.ExecuteNonQuery();
                        msg.setMessageBox("Data updated Successfully", 1);
                       // trans.Commit();
                    }

                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
               // trans.Rollback();
            }
        }


        /*
         * This function is used to Soft delete datas into the database table.
         * 
         * All the database tables are handle with this function.
         * 
         * This function accept 2 parameters.......
         * 1.procedureName:- the name of procedure that stored in database.
         * 2.id: Which data is to soft delete
         * 
         * 
         * */
        public void softDelete(string procedureName, string id)
        {
            // SqlTransaction trans = null;
            try
            {
    
                using (con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    //trans = con.BeginTransaction("UpdateTransaction");

                    using (SqlCommand cmd = new SqlCommand(procedureName))
                    {
                        cmd.Connection = con;
                        // cmd.Transaction = trans;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Id", id);
                        cmd.ExecuteNonQuery();
                        msg.setMessageBox("Data changed Successfully", 1);
                        // trans.Commit();
                    }

                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                System.Windows.Forms.MessageBox.Show(ex.Message);
                // trans.Rollback();
            }
        }




        /*
         * This function is used to DELETE datas from the database table.
         * 
         * All the database tables are handle with this function.
         * 
         * This function accept 3 parameters.......
         * 1.procedureName:- the name of procedure that stored in database.
         * 2.param:- All the parameters are passed as a SqlParameter object array
         * 3.data:- Array of data to be inserted/updated into database on the order of parameter
         * 
         * 
         * */
        public void deleteData(string procedureName, SqlParameter[] param)
        {
           // SqlTransaction trans = null;
            try
            {
                //To get how many data are present in Database
                int countParam = param.Count();
                using (con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                   // trans = con.BeginTransaction("DeleteTransaction");
                    using (SqlCommand cmd = new SqlCommand(procedureName))
                    {
                        cmd.Connection = con;
                       // cmd.Transaction = trans;
                        cmd.CommandType = CommandType.StoredProcedure;
                        for (int i = 0; i < countParam; i++)
                        {
                            //Each data passed into that respective parameter...
                            cmd.Parameters.Add(param[i]);
                        }
                        if(cmd.ExecuteNonQuery()>0)
                            msg.setMessageBox("Data deleted Successfully", 1);
                      //  trans.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
               // trans.Rollback();
            }
        }


       

      /*  public void list_DataView(string procedureName, ListView myList)
        {
           
            myList.Items.Clear();
            try
            {
                using (con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(procedureName))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        using(SqlDataReader dr=cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                ListViewItem lItem = new ListViewItem(dr[0].ToString());
                                
                                for (int i = 1; i <= dr.FieldCount - 1; i++)
                                {
                                    lItem.SubItems.Add(dr[i].ToString());
                       
                                }
                                myList.Items.Add(lItem);
                                int rCount = myList.Items.Count;
                                if (rCount % 2 == 1)
                                {
                                    //lItem.BackColor = Color.WhiteSmoke;
                                    lItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(234)))), ((int)(((byte)(189)))));
                                }
                                else
                                {
                                    lItem.BackColor = Color.White;
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }*/

        /*
        * list_DataView is a common function for storing Database table data into ListView controller 
        * 
        * it accept two parameter 
        *     1)Store Procedure name
        *     2)ListView object used by the form
        * 
        * 
        * */
       public void list_DataView(string procedureName, ListView myList)
        {
           
           // myList.Items.Clear();
            try
            {
                using (con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(procedureName))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlDataAdapter ad = new SqlDataAdapter();
                        ad.SelectCommand = cmd;
                        DataTable dt = new DataTable();
                        ad.Fill(dt);
                        myList.ItemsSource = dt.DefaultView;
                        
                    }
                }
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }
       public void list_DataViewUser(string procedureName, ListView myList)
       {

           // myList.Items.Clear();
           try
           {
               using (con = new SqlConnection(DBCon.loginString))
               {
                   con.Open();
                   using (SqlCommand cmd = new SqlCommand(procedureName))
                   {
                       cmd.Connection = con;
                       cmd.CommandType = CommandType.StoredProcedure;
                       SqlDataAdapter ad = new SqlDataAdapter();
                       ad.SelectCommand = cmd;
                       DataTable dt = new DataTable();
                       ad.Fill(dt);
                       myList.ItemsSource = dt.DefaultView;

                   }
               }
           }
           catch (Exception ex)
           {
               System.Windows.Forms.MessageBox.Show(ex.Message);
           }
       }


       /*
       * list_DataViewQuery is a common function for storing Database table data into ListView controller WHRE Condition applied
       * 
       * it accept two parameter 
       *     1)Query
       *     2)ListView object used by the form
       * 
       * 
       * */
       public void list_DataViewQuery(string qry, ListView myList)
       {

           // myList.Items.Clear();
           try
           {
               using (con = new SqlConnection(DBCon.conStr))
               {
                   con.Open();
                   using (SqlCommand cmd = new SqlCommand(qry))
                   {
                       cmd.Connection = con;
                       SqlDataAdapter ad = new SqlDataAdapter();
                       ad.SelectCommand = cmd;
                       DataTable dt = new DataTable();
                       ad.Fill(dt);
                       myList.ItemsSource = dt.DefaultView;
                   }
               }
           }
           catch (Exception ex)
           {
               System.Windows.Forms.MessageBox.Show(ex.Message);
           }
       }


       /*
       * list_DataView is a common function for storing Database table data into ListView controller 
       * 
       * it accept two parameter 
       *     1)Store Procedure name
       *     2)ListView object used by the form
       *     3)Where condition data
       * 
       * */

       public void list_DataView(string procedureName, ListView myList,string field,string value)
       {

           // myList.Items.Clear();
           try
           {
               using (con = new SqlConnection(DBCon.conStr))
               {
                   con.Open();
                   using (SqlCommand cmd = new SqlCommand(procedureName))
                   {
                       cmd.Connection = con;
                       cmd.CommandType = CommandType.StoredProcedure;
                       cmd.Parameters.Add(new SqlParameter(field, SqlDbType.Decimal) {Value=value});
                       SqlDataAdapter ad = new SqlDataAdapter();
                       ad.SelectCommand = cmd;
                       DataTable dt = new DataTable();
                       ad.Fill(dt);
                       myList.ItemsSource = dt.DefaultView;

                   }

               }
           }
           catch (Exception ex)
           {
               System.Windows.Forms.MessageBox.Show(ex.Message);
           }
       }


        public bool fillComboBox(System.Windows.Controls.ComboBox combobox, string procedureName, string dTable, string dDisplay, string dValue, string defaultValue)
        {
            SqlCommand sqlcmd = new SqlCommand();
            SqlDataAdapter sqladp = new SqlDataAdapter();
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection _sqlconTeam = new SqlConnection(DBCon.conStr))
                {
                    sqlcmd.Connection = _sqlconTeam;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = procedureName;
                    _sqlconTeam.Open();
                    sqladp.SelectCommand = sqlcmd;
                    sqladp.Fill(ds, dTable);
                    DataRow nRow = ds.Tables[dTable].NewRow();
                    nRow[dDisplay] = defaultValue;
                    nRow[dValue] = "-1";
                    ds.Tables[dTable].Rows.InsertAt(nRow, 0);
                    combobox.ItemsSource = ds.Tables[dTable].DefaultView;
                    combobox.DisplayMemberPath = ds.Tables[dTable].Columns[1].ToString();
                    combobox.SelectedValuePath = ds.Tables[dTable].Columns[0].ToString();
                    combobox.SelectedIndex = 0;
                   
                }
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                return false;
            }
            finally
            {
                sqladp.Dispose();
                sqlcmd.Dispose();
            }
        }
        
    }
}
