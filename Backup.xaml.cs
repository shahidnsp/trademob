﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;
using MobileShop_Application.Class;
using System.Data;
using System.Data.SqlClient;


namespace MobileShop_Application
{
    /// <summary>
    /// Interaction logic for Backup.xaml
    /// </summary>
    public partial class Backup : Window
    {
        int uType = 0;
        public Backup()
        {
            InitializeComponent();
        }

        private void btnSource_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Title = "Restore Backup";
            open.Filter = "data|*.mdf";

            if (open.ShowDialog() == true)
            {
                txtSource.Text = open.FileName;
                updateButton();
            }

        }

        private void updateButton()
        {
            if(txtSource.Text!=String.Empty)
                btnRestore.IsEnabled=true;
            else
                btnRestore.IsEnabled=false;

           /* if (txtTarget.Text != String.Empty)
                btnBackup.IsEnabled = true;
            else
                btnBackup.IsEnabled = false;*/

            if (txtSource.Text != String.Empty && txtTarget.Text != String.Empty)
            {
                btnRestore.IsEnabled = false;
                btnBackup.IsEnabled = true;
            }
        }

        private void btnTarget_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Title = "Perform Backup";
            save.Filter = "data|*.mdf";

            if (save.ShowDialog() == true)
            {
                txtTarget.Text = save.FileName;
                updateButton();
            }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnBackup_Click(object sender, RoutedEventArgs e)
        {
            if (checkLogin())
            {
                lblError.Visibility = Visibility.Hidden;

                btnRestore.IsEnabled = false;
                btnBackup.IsEnabled = false;
                btnSource.IsEnabled = false;
                btnTarget.IsEnabled = false;

                if (MessageBox.Show("Are you want to Perform Backup?", "PsyboBiz 1.0", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    try
                    {
                        File.Copy(txtSource.Text, txtTarget.Text, true);
                        MessageBox.Show("Backup has finished successfully");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                btnRestore.IsEnabled = true;
                btnBackup.IsEnabled = true;
                btnSource.IsEnabled = true;
                btnTarget.IsEnabled = true;
            }
            else
            {
                lblError.Visibility = Visibility.Visible;
            }

        }

        private void btnRestore_Click(object sender, RoutedEventArgs e)
        {
            if (checkLogin())
            {
                lblError.Visibility = Visibility.Hidden;

                btnRestore.IsEnabled = false;
                btnBackup.IsEnabled = false;
                btnSource.IsEnabled = false;
                btnTarget.IsEnabled = false;

                if (MessageBox.Show("Database File Location will be change.!..Are you want to Perform Restore?", "PsyboBiz 1.0", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (uType == 0)
                    {
                        try
                        {
                            using (SqlConnection con1 = new SqlConnection(DBCon.loginString))
                            {
                                con1.Open();
                                using (SqlCommand cmd = new SqlCommand("Update_Path"))
                                {
                                    cmd.Connection = con1;
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.AddWithValue("@path", txtSource.Text);
                                    if (cmd.ExecuteNonQuery() > 0)
                                    {
                                        Message msg = new Message();
                                        msg.setMessageBox("Database Path changed Successfully", 1);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            System.Windows.Forms.MessageBox.Show(ex.Message);
                        }
                    }
                   
                   
                }
                btnRestore.IsEnabled = true;
                btnBackup.IsEnabled = true;
                btnSource.IsEnabled = true;
                btnTarget.IsEnabled = true;
            }
            else
            {
                lblError.Visibility = Visibility.Visible;
            }

        }

        private bool checkLogin()
        {
            try
            {
                using (SqlConnection con2 = new SqlConnection(DBCon.loginString))
                {
                    con2.Open();
                    using (SqlCommand cmd1 = new SqlCommand("User_Login"))
                    {
                        cmd1.Connection = con2;
                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd1.Parameters.AddWithValue("@username", txtUsername.Text);
                        cmd1.Parameters.AddWithValue("@password", txtPassword.Password);
                        using (SqlDataReader dr = cmd1.ExecuteReader())
                        {
                            if (dr.Read())
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return false;
        }

        private void btnBackup_MouseEnter(object sender, MouseEventArgs e)
        {
            btnBackup.Foreground = new SolidColorBrush(Colors.Red);
        }

        private void btnBackup_MouseLeave(object sender, MouseEventArgs e)
        {
            btnBackup.Foreground = new SolidColorBrush(Colors.Black);
        }

        private void btnRestore_MouseEnter(object sender, MouseEventArgs e)
        {
            btnRestore.Foreground = new SolidColorBrush(Colors.Red);
        }

        private void btnRestore_MouseLeave(object sender, MouseEventArgs e)
        {
            btnRestore.Foreground = new SolidColorBrush(Colors.Black);
        }

       
    }
}
