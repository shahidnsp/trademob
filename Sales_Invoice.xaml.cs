﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using MobileShop_Application.Class;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using System.IO;

namespace MobileShop_Application
{
    /// <summary>
    /// Interaction logic for Sales_Invoice.xaml
    /// </summary>
    public partial class Sales_Invoice : Window
    {
        public byte[] photo;
        public byte[] proof;
        private string prefix = "";
        private int isPrefix = 0, year = 0;
        private decimal start = 0;
        int isstock = 0;
        [DllImport("user32.dll")]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);
        public string editId = "0",load="";
        private const int GWL_STYLE = -16;
        private decimal isunit = 0, totalNet = 0, totalTax = 0, totalGrand = 0, disc = 0;
        private const int WS_MAXIMIZEBOX = 0x10000; //maximize button
        private const int WS_MINIMIZEBOX = 0x20000; //minimize button
        public string custId = null;
        private int slno = 1,type;
        private string selectedItem = "0";
        private decimal gross = 0, lastFinal = 0, sumqty, kg=0;
        public Sales_Invoice()
        {
            InitializeComponent();
            this.SourceInitialized += MainWindow_SourceInitialized;
            txtBarcode.Focus();
            getPrefix();
            addHotKeys();
            gridBarcode.Visibility = Visibility.Hidden;
            lstvSearchBarcode.Visibility = Visibility.Hidden;
            gridItem.Visibility = Visibility.Hidden;
            lstvSearchItem.Visibility = Visibility.Hidden;
            getBarcode();
        }
        private void addHotKeys()
        {
            RoutedCommand newCmd = new RoutedCommand();
            newCmd.InputGestures.Add(new KeyGesture(Key.N, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(newCmd, btnNew_Click));

            RoutedCommand saveCmd = new RoutedCommand();
            saveCmd.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(saveCmd, btnSave_Click));

            RoutedCommand delCmd = new RoutedCommand();
            delCmd.InputGestures.Add(new KeyGesture(Key.D, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(delCmd, btnDelete_Click));

            RoutedCommand delPnt = new RoutedCommand();
            delCmd.InputGestures.Add(new KeyGesture(Key.P, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(delCmd, btnPrint_Click));
        }
        private IntPtr _windowHandle;
        private void MainWindow_SourceInitialized(object sender, EventArgs e)
        {
            _windowHandle = new WindowInteropHelper(this).Handle;

            //disable maximize button
            DisableMaximizeButton();
        }

        protected void DisableMaximizeButton()
        {
            if (_windowHandle == IntPtr.Zero)
                throw new InvalidOperationException("The window has not yet been completely initialized");

            SetWindowLong(_windowHandle, GWL_STYLE, GetWindowLong(_windowHandle, GWL_STYLE) & ~WS_MAXIMIZEBOX);
        }

        private bool getPrefix()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.GET_INVOICE_PREPIX))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Id", "PROD");
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.Read())
                            {
                                if (dr["yes"] != DBNull.Value)
                                    isPrefix = Convert.ToInt32(dr["yes"]);
                                start = Convert.ToDecimal(dr["start"]);
                                prefix = dr["prefix"].ToString();
                                if (dr["year"] != DBNull.Value)
                                    year = Convert.ToInt32(dr["year"]);
                                return true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return false;
        }

        private void setInvoiceID()
        {
            decimal inv = getMaxID() + start;
            if (inv != 0)
            {
                if (isPrefix == 0)
                {

                    txtInvoiceNo.Text = inv.ToString();
                }
                else
                {
                    if (year == 0)
                    {
                        txtInvoiceNo.Text = prefix + inv.ToString();
                    }
                    else
                    {
                        DateTime nextYear = DateTime.Now.AddYears(1);
                        txtInvoiceNo.Text = prefix + inv.ToString() + "/" + DateTime.Now.Year.ToString() + "-" + nextYear.Year.ToString();
                    }
                }

            }
        }

        private decimal getMaxID()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("SELECT MAX(Id) FROM Sales_Invoice"))
                    {
                        cmd.Connection = con;
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.Read())
                            {
                                if (dr[0] == DBNull.Value)
                                    return 1;
                                else
                                    return (Convert.ToDecimal(dr[0])) + 1;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return 0;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DBCon con = new DBCon();
            con.fillComboBox(this.cmbTax, AppConstraints.COMBO_TAX, "Tax", "name", "Id", "Select");
            con.fillComboBox(this.cmbUnit, AppConstraints.COMBO_UNIT, "Unit", "name", "Id", "Select");
            dtpInvoiceDate.Text = DateTime.Now.ToString();
            setInvoiceID();
            //cmbCustomer.SelectedValue = 5;
            loadEdit();
        }
        private void loadEdit()
        {
            if (this.load == "EDIT")
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(DBCon.conStr))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand("SELECT * FROM Sales_Invoice WHERE Id=" + this.editId))
                        {
                            cmd.Connection = con;
                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                if (dr.Read())
                                {
                                    txtInvoiceNo.Text = dr["invoice_no"].ToString();
                                    dtpInvoiceDate.Text = dr["invoice_date"].ToString();
                                    custId = dr["customer_id"].ToString();
                                    loadCustomer(custId);
                                    txtDiscount.Text = dr["discount"].ToString();
                                    txtGross.Text = dr["gross_amount"].ToString();
                                    txtAdditional.Text = dr["additional"].ToString();
                                    txtBalance.Text = dr["balance"].ToString();
                                    txtTax.Text = dr["tax"].ToString();
                                    btnSave.Content = "_Update";
                                    btnDelete.IsEnabled = true;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                try
                {
                    using (SqlConnection con = new SqlConnection(DBCon.conStr))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand("SELECT * FROM ViewSalesInvoice_Items WHERE sales_invoice_id=" + this.editId))
                        {
                            cmd.Connection = con;
                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    LstvCart.Items.Add(new Invoice_Cart_List()
                                    {
                                        Id = dr["item_id"].ToString(),
                                        slno = slno++.ToString(),
                                        barcode = dr["barcode"].ToString(),
                                        item = dr["item"].ToString(),
                                        price = dr["sale_rate"].ToString(),
                                        discount = dr["discount"].ToString(),
                                        gross = dr["gross"].ToString(),
                                        tax_id = dr["tax_id"].ToString(),
                                        tax = dr["tax"].ToString(),
                                        unit = dr["unit"].ToString(),
                                        unit_id = dr["unit_id"].ToString()
                                    });
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        public void loadCustomer(string id)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("SELECT name,phone,mobile,address FROM Customer WHERE Id=" + id))
                    {
                        cmd.Connection = con;
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.Read())
                            {
                                txtName.Text = dr["name"].ToString();
                                txtPhone.Text = dr["phone"].ToString();
                                txtMobile.Text = dr["mobile"].ToString();
                                txtAddress.Text = dr["address"].ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Save_Or_Update();
            btnPrint.IsEnabled = true;
        }

        private void saveCustomer()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.INSERT_CUSTOMER))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@name", txtName.Text);
                        cmd.Parameters.AddWithValue("@address", txtAddress.Text);
                        cmd.Parameters.AddWithValue("@place", "");
                        cmd.Parameters.AddWithValue("@phone1", txtPhone.Text);
                        cmd.Parameters.AddWithValue("@phone2", txtMobile.Text);
                        cmd.Parameters.AddWithValue("@customer_type","Buyer");
                        cmd.Parameters.AddWithValue("@date", dtpInvoiceDate.Text);
                            FileStream fs = new FileStream("profile.png", FileMode.Open, FileAccess.Read);
                            photo = new byte[fs.Length];
                            fs.Read(photo, 0, System.Convert.ToInt32(fs.Length));
                            fs.Close();
                        cmd.Parameters.AddWithValue("@photo", photo);

                            FileStream fs1 = new FileStream("profile.png", FileMode.Open, FileAccess.Read);
                            proof = new byte[fs1.Length];
                            fs1.Read(proof, 0, System.Convert.ToInt32(fs1.Length));
                            fs1.Close();
                        cmd.Parameters.AddWithValue("@proof", proof);
                        cmd.ExecuteNonQuery();                  
                    }
                }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }

            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("SELECT MAX(Id) FROM Customer"))
                    {
                        cmd.Connection = con;
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.Read())
                            {
                               custId = dr[0].ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void Save_Or_Update()
        {
           
            if(LstvCart.Items.Count==0)
            {
                MessageBox.Show("No Item in Cart...!!");
                return;
            }
            decimal grossAmt = 0, discAmt = 0, addAmt = 0, balAmt = 0,taxAmt=0,paid=0;
            try
            {
                grossAmt = Convert.ToDecimal(txtGross.Text);
            }
            catch { }
            try
            {
                discAmt = Convert.ToDecimal(txtDiscount.Text);
            }
            catch { }

            try
            {
                addAmt = Convert.ToDecimal(txtAdditional.Text);
            }
            catch { }

            try
            {
                balAmt = Convert.ToDecimal(txtBalance.Text);
            }
            catch { }

            try
            {
                taxAmt = Convert.ToDecimal(txtTax.Text);
            }
            catch { }

            if (txtName.Text != String.Empty)
                saveCustomer();
            else
                custId = "5";

            if (btnSave.Content.ToString()== "_Save")
            {
                if (MessageBox.Show("Are you want to Save..??", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    SqlParameter[] param = {
                                               new SqlParameter("@invoice_no",SqlDbType.NVarChar){Value=txtInvoiceNo.Text},
                                               new SqlParameter("@invoice_date",SqlDbType.DateTime){Value=dtpInvoiceDate.SelectedDate.Value.ToShortDateString()+" "+DateTime.Now.ToLongTimeString()},
                                               new SqlParameter("@customer_id",SqlDbType.Decimal){Value=custId},
                                               new SqlParameter("@gross_amount",SqlDbType.VarChar){Value=grossAmt},
                                               new SqlParameter("@discount",SqlDbType.VarChar){Value=discAmt},
                                               new SqlParameter("@additional",SqlDbType.VarChar){Value=addAmt},
                                               new SqlParameter("@balance",SqlDbType.VarChar){Value=balAmt},
                                               new SqlParameter("@tax",SqlDbType.VarChar){Value=taxAmt},
                                               new SqlParameter("@paid",SqlDbType.Decimal){Value=paid}
                                           };
                    DBCon con = new DBCon();
                    con.insertData(AppConstraints.INSERT_SALES_INVOICE, param);

                    string lastinsertedId = getLastInsertedId();

                    if (lastinsertedId != null)
                    {
                        foreach (var item in LstvCart.Items)
                        {
                            var itm = item as Invoice_Cart_List;
                            if (itm != null)
                            {
                                SqlParameter[] param1 = {
                                               new SqlParameter("@sale_invoice_id",SqlDbType.Decimal){Value=lastinsertedId},
                                               new SqlParameter("@item_id",SqlDbType.Decimal){Value=itm.Id},
                                               new SqlParameter("@discount",SqlDbType.Decimal){Value=itm.discount},
                                               new SqlParameter("@gross",SqlDbType.Decimal){Value=itm.gross},
                                               new SqlParameter("@tax_id",SqlDbType.Decimal){Value=itm.tax_id},
                                               new SqlParameter("@qty",SqlDbType.Decimal){Value=itm.qty},
                                               new SqlParameter("@unit_id",SqlDbType.Decimal){Value=itm.unit_id}
                                           };
                                if (checkIsStock(itm.Id) == 1)
                                    deactiveItem(itm.Id);
                                con.insertInvoice(AppConstraints.INSERT_SALE_INVOICE_ITEM, param1);
                            }
                        }
                    }
                    btnSave.Content = "_Update";
                    btnDelete.IsEnabled = true;
                    this.editId = lastinsertedId;


                    SqlParameter[] param3 = {
                                           new SqlParameter("@name",SqlDbType.NVarChar){Value="Cash Sale"},
                                           new SqlParameter("@amount",SqlDbType.VarChar){Value=paid},
                                           new SqlParameter("@description",SqlDbType.NVarChar){Value="Cash sales from "+txtName.Text+"("+txtInvoiceNo.Text+")"},
                                           new SqlParameter("@date",SqlDbType.Date){Value=dtpInvoiceDate.Text},
                                           new SqlParameter("@type",SqlDbType.VarChar){Value="Sales"},
                                           new SqlParameter("@sale_id",SqlDbType.Decimal){Value=lastinsertedId},
                                       };
                    con.insertInvoice(AppConstraints.INSERT_INCOME, param3);
                }
            }
            else
            {
                if (MessageBox.Show("Are you want to Update..??", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    SqlParameter[] param = {
                                               new SqlParameter("@Id",SqlDbType.Decimal){Value=this.editId},
                                               new SqlParameter("@invoice_no",SqlDbType.NVarChar){Value=txtInvoiceNo.Text},
                                               new SqlParameter("@invoice_date",SqlDbType.DateTime){Value=dtpInvoiceDate.Text},
                                               new SqlParameter("@customer_id",SqlDbType.Decimal){Value=custId},
                                               new SqlParameter("@gross_amount",SqlDbType.VarChar){Value=grossAmt},
                                               new SqlParameter("@discount",SqlDbType.VarChar){Value=discAmt},
                                               new SqlParameter("@additional",SqlDbType.VarChar){Value=addAmt},
                                               new SqlParameter("@balance",SqlDbType.VarChar){Value=balAmt},
                                               new SqlParameter("@tax",SqlDbType.VarChar){Value=taxAmt},
                                               new SqlParameter("@paid",SqlDbType.Decimal){Value=paid}
                                           };
                    DBCon con = new DBCon();
                    con.insertData(AppConstraints.UPDATE_SALES_INVOICE, param);

                    
                    SqlParameter[] param2 = {
                                               new SqlParameter("@Id",SqlDbType.Decimal){Value=this.editId},
                                           };
                    con.insertInvoice(AppConstraints.DELETE_SALES_INVOICE_ITEM, param2);

                   

                    if (editId != null)
                    {
                        foreach (var item in LstvCart.Items)
                        {
                            var itm = item as Invoice_Cart_List;
                            if (itm != null)
                            {
                                SqlParameter[] param1 = {
                                               new SqlParameter("@sale_invoice_id",SqlDbType.Decimal){Value=editId},
                                               new SqlParameter("@item_id",SqlDbType.Decimal){Value=itm.Id},
                                               new SqlParameter("@discount",SqlDbType.Decimal){Value=itm.discount},
                                               new SqlParameter("@gross",SqlDbType.Decimal){Value=itm.gross},
                                               new SqlParameter("@tax_id",SqlDbType.Decimal){Value=itm.tax_id},
                                               new SqlParameter("@qty",SqlDbType.Decimal){Value=itm.qty},
                                               new SqlParameter("@unit_id",SqlDbType.Decimal){Value=itm.unit_id}
                                           };

                                con.insertInvoice(AppConstraints.INSERT_SALE_INVOICE_ITEM, param1);
                                if(checkIsStock(itm.Id)==1)
                                    deactiveItem(itm.Id);
                            }
                        }
                    }

                    SqlParameter[] param5 = {
                                               new SqlParameter("@Id",SqlDbType.VarChar){Value=this.editId},
                                           };
                    con.insertInvoice(AppConstraints.DELETE_INCOME_SALE, param5);
                    SqlParameter[] param3 = {
                                           new SqlParameter("@name",SqlDbType.NVarChar){Value="Cash Sale"},
                                           new SqlParameter("@amount",SqlDbType.VarChar){Value=grossAmt},
                                           new SqlParameter("@description",SqlDbType.NVarChar){Value="Cash sales from "+txtName.Text+"("+txtInvoiceNo.Text+")"},
                                           new SqlParameter("@date",SqlDbType.Date){Value=dtpInvoiceDate.Text},
                                           new SqlParameter("@type",SqlDbType.VarChar){Value="Sales"},
                                           new SqlParameter("@sale_id",SqlDbType.Decimal){Value=this.editId},
                                       };
                    con.insertInvoice(AppConstraints.INSERT_INCOME, param3);

                }
            }
        }

        private int checkIsStock(string id)
        {
            using (SqlConnection con = new SqlConnection(DBCon.conStr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT isstock FROM Item WHERE Id="+id))
                {
                    cmd.Connection = con;
                    SqlDataReader rd = cmd.ExecuteReader();
                    if (rd.Read())
                    {
                        return Convert.ToInt32(rd[0].ToString());
                    }
                }
            }
            return 0;
        }

        private void deactiveItem(string id)
        {
            DBCon con = new DBCon();
            SqlParameter[] param1 = {
                                               new SqlParameter("@Id",SqlDbType.Decimal){Value=id},
                                               new SqlParameter("@buyer_id",SqlDbType.Decimal){Value=custId}
                                           };

            con.insertInvoice(AppConstraints.UPDATE_BUYER, param1);
        }

        private string getLastInsertedId()
        {
            using (SqlConnection con = new SqlConnection(DBCon.conStr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT MAX(Id) FROM Sales_Invoice"))
                {
                    cmd.Connection = con;
                    SqlDataReader rd = cmd.ExecuteReader();
                    if (rd.Read())
                    {
                        return rd[0].ToString();
                    }
                }
            }
            return null;
        }

        class Invoice_Cart_List
        {
            public string slno { get; set; }
            public string Id { get; set; }
            public string barcode { get; set; }
            public string item { get; set; }
            public string price { get; set; }
            public string qty { get; set; }
            public string discount { get; set; }
            public string gross { get; set; }
            public string tax { get; set; }
            public string tax_id { get; set; }
            public string unit { get; set; }
            public string unit_id { get; set; }
            
        }

        private void txtSale_Copy1_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        //private void cmbItem_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    if(cmbItem.SelectedIndex>0)
        //    {
        //        using(SqlConnection con=new SqlConnection(DBCon.conStr))
        //        {
        //            con.Open();
        //            using(SqlCommand cmd=new SqlCommand("SELECT sale_rate,barcode,type FROM Item WHERE Id="+cmbItem.SelectedValue))
        //            {
        //                cmd.Connection = con;
        //                SqlDataReader rd = cmd.ExecuteReader();
        //                if(rd.Read())
        //                {
        //                    txtSale.Text = rd[0].ToString();
        //                    txtBarcode.Text = rd[1].ToString();
        //                    txtDisc.Text = "0";
        //                    txtNet.Text = txtSale.Text;
        //                    type = Convert.ToInt32(rd[2].ToString());
        //                }
        //            }
        //        }
        //    }
        //}

        private void txtSale_TextChanged(object sender, TextChangedEventArgs e)
        {
            //To get item price with respect to unit
                //try
                //{
                //    using (SqlConnection con = new SqlConnection(DBCon.conStr))
                //    {
                //        con.Open();
                //        using (SqlCommand cmd = new SqlCommand(AppConstraints.GET_UNIT))
                //        {
                //            cmd.Connection = con;
                //            cmd.CommandType = CommandType.StoredProcedure;
                //            cmd.Parameters.AddWithValue("@Id", cmbUnit.SelectedValue);
                //            using (SqlDataReader dr = cmd.ExecuteReader())
                //            {
                //                if (dr.Read())
                //                {
                //                    isunit = Convert.ToDecimal(dr["isunit"].ToString());
                //                    kg = Convert.ToDecimal(dr["kg"].ToString());
                //                }
                //            }
                //        }
                //    }
                //}
                //catch (Exception ex)
                //{
                //    MessageBox.Show(ex.Message);
                //}

                decimal disc = 0;
                try
                {
                    disc = Convert.ToDecimal(txtDisc.Text);
                }
                catch { }

                try
                {
                    if (isunit == 0)
                    {
                        txtNet.Text = ((Convert.ToDecimal(txtQty.Text) * Convert.ToDecimal(txtSale.Text)) - disc).ToString();
                    }
                    else
                    {
                        decimal net = (Convert.ToDecimal(txtQty.Text) * (Convert.ToDecimal(txtSale.Text) * Convert.ToDecimal(kg))) - disc;
                        txtNet.Text = (Math.Round(net, 2)).ToString();
                    }
                }
                catch { }               
        }

        private void txtDisc_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                decimal disc = 0;
                try
                {
                    disc = Convert.ToDecimal(txtDisc.Text);
                }
                catch { }

                if (isunit == 0)
                {
                    txtNet.Text = ((Convert.ToDecimal(txtQty.Text) * Convert.ToDecimal(txtSale.Text)) - disc).ToString();
                }
                else
                {
                    decimal net = (Convert.ToDecimal(txtQty.Text) * (Convert.ToDecimal(txtSale.Text) * Convert.ToDecimal(kg))) - disc;
                    txtNet.Text = (Math.Round(net, 2)).ToString();
                }
            }
            catch { }
         
            //try
            //{
            //    decimal disc = Convert.ToDecimal(txtDisc.Text);
            //    decimal rate = Convert.ToDecimal(txtSale.Text);
            //    txtNet.Text = (rate - disc).ToString();
            //}
            //catch { }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            //addtoCart();
            cart();
        }
        //private void addtoCart()
        //{
        //    if(cmbItem.SelectedIndex==0)
        //    {
        //        Message msg = new Message();
        //        msg.setCreateError("No Item Selected...");
        //        return;
        //    }
        //    //Avoid Repeatly adding.........same item
        //    foreach (var item in LstvCart.Items)
        //    {
        //        var itm = item as Invoice_Cart_List;
        //        if (itm.Id == cmbItem.SelectedValue.ToString())
        //        {
        //            Message msg = new Message();
        //            msg.setCreateError("Item already added to Cart...");
        //            return;
        //        }
        //    }


        //    LstvCart.Items.Add(new Invoice_Cart_List()
        //    {
        //        Id = cmbItem.SelectedValue.ToString(),
        //        slno = slno++.ToString(),
        //        barcode = txtBarcode.Text,
        //        item = cmbItem.Text,
        //        price = txtSale.Text,
        //        discount = txtDisc.Text,
        //        gross = txtNet.Text
        //    });

        //    decimal net = 0;
        //    decimal disc = 0;
        //    decimal add = 0;

        //    try
        //    {
        //        net = Convert.ToDecimal(txtNet.Text);
        //    }
        //    catch { }
        //    try
        //    {
        //        disc = Convert.ToDecimal(txtDiscount.Text);
        //    }
        //    catch { }
        //    try
        //    {
        //        add = Convert.ToDecimal(txtAdditional.Text);
        //    }
        //    catch { }
        //    decimal grand = (net + add) - disc;
        //    gross += grand;
        //    lastFinal = gross;
        //    txtGross.Text = gross.ToString();
        //    txtPaid.Text = txtGross.Text;
        //    txtBarcode.Text = String.Empty;
        //    txtSale.Text = String.Empty;
        //    txtDisc.Text = "0";
        //    txtNet.Text = String.Empty;
        //    cmbItem.SelectedIndex = 0;
        //    txtBarcode.Focus();
        //}

        private void LstvCart_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Are you want to Remove?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var item = LstvCart.Items.GetItemAt(LstvCart.SelectedIndex) as Invoice_Cart_List;
                    activeItem(item.Id);
                    decimal selectQty = Convert.ToDecimal(item.qty);
                    string curUnit_id = item.unit_id;
                    string itemId = item.Id;
                    string curKg = "0";
                    int curisUnit = 0;

                    sumqty = Convert.ToDecimal(getQty(itemId));
                    if (isstock == 1)
                        activeItem(itemId);
                    if (type == 0)
                    {
                        try
                        {
                            decimal tax = 0;

                            try
                            {
                                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                                {
                                    con.Open();
                                    using (SqlCommand cmd = new SqlCommand(AppConstraints.GET_TAX_PERCENTAGE))
                                    {
                                        cmd.Connection = con;
                                        cmd.CommandType = CommandType.StoredProcedure;
                                        cmd.Parameters.AddWithValue("@Id", item.tax_id);
                                        using (SqlDataReader dr = cmd.ExecuteReader())
                                        {
                                            if (dr.Read())
                                            {
                                                if (dr[0] == DBNull.Value)
                                                {
                                                    tax = 0;
                                                }
                                                else
                                                {
                                                    tax = Convert.ToDecimal(dr[0]);
                                                }
                                            }
                                            else
                                            {
                                                tax = 0;
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }

                            totalNet -= Convert.ToDecimal(item.gross);
                            decimal net = Convert.ToDecimal(item.gross);
                            totalTax -= (net * tax) / 100;
                            txtTax.Text = Math.Round(totalTax, 2).ToString();
                           // txtGross.Text = totalNet.ToString();
                            totalGrand = Math.Round(totalNet + totalTax, 2);
                            lastFinal = totalGrand;
                            txtGross.Text = totalGrand.ToString();
                            txtPaid.Text = totalGrand.ToString();
                        }
                        catch { }

                        try
                        {
                            using (SqlConnection con = new SqlConnection(DBCon.conStr))
                            {
                                con.Open();
                                using (SqlCommand cmd = new SqlCommand(AppConstraints.GET_UNIT))
                                {
                                    cmd.Connection = con;
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.AddWithValue("@Id", curUnit_id);
                                    using (SqlDataReader dr = cmd.ExecuteReader())
                                    {
                                        if (dr.Read())
                                        {
                                            curisUnit = Convert.ToInt32(dr["isunit"].ToString());
                                            curKg = dr[1].ToString();
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }



                        //To Increase Item Quantity with respect to sales...................................................................................
                        decimal currentQty = 0;
                        if (curisUnit == 0)
                        {
                            currentQty = sumqty + Convert.ToDecimal(selectQty);
                        }
                        else
                        {
                            currentQty = sumqty + (Convert.ToDecimal(selectQty) * Convert.ToDecimal(curKg));
                        }

                        try
                        {
                            using (SqlConnection con = new SqlConnection(DBCon.conStr))
                            {
                                con.Open();
                                using (SqlCommand cmd = new SqlCommand(AppConstraints.UPDATE_STOCK))
                                {
                                    cmd.Connection = con;
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.AddWithValue("@Id", itemId);
                                    cmd.Parameters.AddWithValue("@qty", currentQty);
                                    cmd.ExecuteNonQuery();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }

                    LstvCart.Items.RemoveAt(LstvCart.SelectedIndex);                   
                }
            }
            catch { }
        }

        private string getQty(string id)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.GET_QTY))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Id", id);
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.Read())
                            {
                                type = Convert.ToInt32(dr["type"].ToString());
                                isstock = Convert.ToInt32(dr["isstock"].ToString());
                                return dr["qty"].ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
            return "0";
        }

        private void activeItem(string id)
        {
            DBCon con = new DBCon();
            SqlParameter[] param1 = {
                                               new SqlParameter("@Id",SqlDbType.Decimal){Value=id},
                                           };

            con.insertInvoice(AppConstraints.ACTIVE_ITEM, param1);
        }
        private void txtDiscount_TextChanged(object sender, TextChangedEventArgs e)
        {
            decimal disc = 0;
            decimal add = 0;
            try
            {
                disc = Convert.ToDecimal(txtDiscount.Text);
            }
            catch { }
            try
            {
                add = Convert.ToDecimal(txtAdditional.Text);
            }
            catch { }


            decimal amt = 0;
            try
            {
                amt = ((lastFinal + add) - disc);
            }
            catch { }
            txtGross.Text =amt.ToString();
            try
            {
                txtPaid.Text = txtGross.Text;
            }
            catch { }
        }

        private void txtAdditional_TextChanged(object sender, TextChangedEventArgs e)
        {
            decimal disc = 0;
            decimal add = 0;
            try
            {
                disc = Convert.ToDecimal(txtDiscount.Text);
            }
            catch { }
            try
            {
                add = Convert.ToDecimal(txtAdditional.Text);
            }
            catch { }

            txtGross.Text = ((lastFinal + add) - disc).ToString();
            try
            {
                txtPaid.Text = txtGross.Text;
            }
            catch { }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Are you want to Delete..??", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                DBCon con = new DBCon();
                string lastinsertedId = getLastInsertedId();

                SqlParameter[] param2 = {
                                               new SqlParameter("@Id",SqlDbType.Decimal){Value=this.editId},
                                           };
                con.deleteData(AppConstraints.DELETE_SALES_INVOICE, param2);
            }
        }

        private void btnFindCust_Click(object sender, RoutedEventArgs e)
        {
            Customer cust = new Customer();
            cust.cmbCustomerType.SelectedIndex = 1;
            cust.cmbCustomerType.IsEnabled = false;
            cust.Owner = this;
            cust.Show();
           // DBCon con = new DBCon();
           // con.fillComboBox(this.cmbCustomer, AppConstraints.COMBO_BUYER, "Customer", "name", "Id", "Select");
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            setInvoiceID();
            custId = "5";
            txtName.Text = String.Empty;
            txtPhone.Text = String.Empty;
            txtMobile.Text = String.Empty;
            txtGross.Text = String.Empty;
            totalGrand = 0;
            totalNet = 0;
            totalTax = 0;
            txtDiscount.Text = String.Empty;
            txtAdditional.Text = String.Empty;
            txtTax.Text = String.Empty;
            txtPaid.Text = String.Empty;
            txtBalance.Text = String.Empty;
            LstvCart.Items.Clear();
            dtpInvoiceDate.Text = DateTime.Now.ToString();
        }

        private void txtSale_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                cart();
        }

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            //Save_Or_Update();
            InvoicePrint print = new InvoicePrint();
            print.invoiceID = this.editId;
            print.Show();
        }

        private void txtPaid_KeyDown(object sender, KeyEventArgs e)
        {
            

        }

        private void txtPaid_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (Convert.ToDecimal(txtGross.Text) != Convert.ToDecimal(txtPaid.Text))
                {
                    decimal disc = 0;
                    decimal add = 0;
                    try
                    {
                        disc = Convert.ToDecimal(txtDiscount.Text);
                    }
                    catch { }
                    try
                    {
                        add = Convert.ToDecimal(txtAdditional.Text);
                    }
                    catch { }

                    decimal Amt = ((lastFinal + add) - disc);

                    decimal p = 0;
                    try
                    {
                        p = Convert.ToDecimal(txtPaid.Text);
                    }
                    catch { }
                    try
                    {
                        txtBalance.Text = (Amt - p).ToString();
                    }
                    catch { }
                }
                else if (Convert.ToDecimal(txtGross.Text) != Convert.ToDecimal(txtPaid.Text))
                {
                    txtBalance.Text = "0.00";
                }
            }
            catch { }
        }

        class SearchItemList
        {
            public string item_id { get; set; }
            public string barcode { get; set; }
            public string item_name { get; set; }
            public string qty { get; set; }
            public string sale_rate { get; set; }
            public string purchase_rate { get; set; }
        }

        private void fillSearchBarcodeList(string like)
        {
            lstvSearchBarcode.Items.Clear();
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.SEARCH_ITEMS))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@barcode", like);
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                lstvSearchBarcode.Items.Add(new SearchItemList()
                                {
                                    item_id = dr["Id"].ToString(),
                                    barcode = dr["barcode"].ToString(),
                                    item_name = dr["name"].ToString(),
                                    sale_rate = dr["sale_rate"].ToString(),
                                    purchase_rate = dr["purchase_rate"].ToString(),
                                    qty = dr["quantity"].ToString()
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void fillSearchItemList(string like)
        {
            lstvSearchItem.Items.Clear();
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.SEARCH_ITEMS_WITH_ITEM))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@item", like);
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                lstvSearchItem.Items.Add(new SearchItemList()
                                {
                                    item_id = dr["Id"].ToString(),
                                    barcode = dr["barcode"].ToString(),
                                    item_name = dr["name"].ToString(),
                                    sale_rate = dr["sale_rate"].ToString(),
                                    purchase_rate = dr["purchase_rate"].ToString(),
                                    qty = dr["quantity"].ToString()
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lstvSearchBarcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (lstvSearchBarcode.Items.Count > 0)
                {
                    var item = lstvSearchBarcode.Items.GetItemAt(lstvSearchBarcode.SelectedIndex) as SearchItemList;
                    if (item != null)
                    {
                        selectedItem = item.item_id;
                        txtItem.Text = item.item_name;
                        using (SqlConnection con = new SqlConnection(DBCon.conStr))
                        {
                            con.Open();
                            using (SqlCommand cmd = new SqlCommand("SELECT type FROM Item WHERE Id=" + item.item_id))
                            {
                                cmd.Connection = con;
                                SqlDataReader rd = cmd.ExecuteReader();
                                if (rd.Read())
                                {
                                    type = Convert.ToInt32(rd[0].ToString());
                                }
                            }
                        }
                        sumqty = Convert.ToDecimal(item.qty);
                        txtSale.Text = item.sale_rate;
                        txtBarcode.Text = item.barcode;
                        txtDisc.Text = "0";
                        //txtNet.Text = txtSale.Text;
                        txtQty.Text = "1";
                        getItem(item.item_id);
                        selectItemToCart(item.sale_rate);
                        txtQty.Focus();

                        gridBarcode.Visibility = Visibility.Hidden;
                        lstvSearchBarcode.Visibility = Visibility.Hidden;
                    }
                }
            }
        }


        private void selectItemToCart(string rate)
        {
            //To set default item unit
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("SELECT * FROM Unit WHERE Id=" + cmbUnit.SelectedValue))
                    {
                        cmd.Connection = con;
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.Read())
                            {
                                isunit = Convert.ToDecimal(dr["isunit"].ToString());
                                kg = Convert.ToDecimal(dr["kg"].ToString());

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            txtDisc.Text = "0";
                    
            if (isunit == 0)
            {
                txtNet.Text = rate;
            }
            else
            {
                decimal net = Convert.ToDecimal(rate) * Convert.ToDecimal(kg);
                txtNet.Text = (Math.Round(net, 2)).ToString();
            }                               
        }

        private void getItem(string id)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("SELECT unit_id,tax_id FROM Item WHERE Id=" +id))
                    {
                        cmd.Connection = con;
                        SqlDataReader rd = cmd.ExecuteReader();
                        if (rd.Read())
                        {
                            cmbUnit.SelectedValue = rd["unit_id"].ToString();
                            cmbTax.SelectedValue = rd["tax_id"].ToString();
                        }
                    }
                }
            }
            catch { }
        }

        private void cart()
        {
            if (selectedItem == "0" || txtItem.Text == String.Empty)
            {
                txtItem.Foreground = new SolidColorBrush(Colors.Red);
                Message msg = new Message();
                msg.setCreateError("Sorry!..Select item first before adding");
                return;
            }
            if (txtQty.Text == String.Empty)
            {
                //txtItem.BorderBrush = new SolidColorBrush(Colors.Red);
                Message msg = new Message();
                msg.setCreateError("Sorry!..Qty field empty.");
                return;
            }
            ////Avoid Repeatly adding.........same item
            //foreach (var item in listView.Items)
            //{
            //    var itm = item as InvoiceItems;
            //    if (itm.id == selectedItem)
            //    {
            //        msg.setCreateError("Item already added to Cart...");
            //        selectedItem = "0";
            //        return;
            //    }
            //}

            decimal dis = 0;

            try
            {
                dis = Convert.ToDecimal(txtDisc.Text);
            }
            catch { }

            LstvCart.Items.Add(new Invoice_Cart_List()
            {
                Id = selectedItem,
                unit_id = cmbUnit.SelectedValue.ToString(),
                tax_id = cmbTax.SelectedValue.ToString(),
                slno = slno++.ToString(),
                barcode = txtBarcode.Text,
                item=txtItem.Text,
                qty = txtQty.Text,
                unit = cmbUnit.Text,
                price = txtSale.Text,
                discount = dis.ToString(),
                tax = cmbTax.Text,
                gross = txtNet.Text,
            });


            decimal tax = 1;

            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.GET_TAX_PERCENTAGE))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Id", cmbTax.SelectedValue);
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.Read())
                            {
                                if (dr[0] == DBNull.Value)
                                {
                                    tax = 0;
                                }
                                else
                                {
                                    tax = Convert.ToDecimal(dr[0]);
                                }
                            }
                            else
                            {
                                tax = 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            decimal net = Convert.ToDecimal(txtNet.Text);
            totalNet += net;
            totalTax += (net * tax) / 100;
            txtTax.Text = Math.Round(totalTax, 2).ToString();
            totalGrand = Math.Round(totalNet + totalTax, 2);
            txtGross.Text = totalGrand.ToString();
            txtPaid.Text = totalGrand.ToString();
            lastFinal = totalGrand;


                if (type == 0)
                {
                    decimal currentQty = 0;

                   
                    //To Reduce Item Quantity with respect to sales when it is a product...................................................................................
                    
                    if (isunit == 0)
                    {
                        currentQty = sumqty - Convert.ToDecimal(txtQty.Text);
                    }
                    else
                    {
                        currentQty = sumqty - (Convert.ToDecimal(txtQty.Text) *kg);
                    }

                    try
                    {
                        using (SqlConnection con = new SqlConnection(DBCon.conStr))
                        {
                            con.Open();
                            using (SqlCommand cmd = new SqlCommand(AppConstraints.UPDATE_STOCK))
                            {
                                cmd.Connection = con;
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@Id", selectedItem);
                                cmd.Parameters.AddWithValue("@qty", currentQty);
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            selectedItem = "0";

            txtBarcode.Focus();
            txtBarcode.Text = "";
            //txtItem.Text = "";
            txtDisc.Text = "";
            txtSale.Text = "";
            txtQty.Text = "1";
            txtNet.Text = "";

        }
       
    
        private void lstvSearchBarcode_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (lstvSearchBarcode.Items.Count > 0)
            {
                var item = lstvSearchBarcode.Items.GetItemAt(lstvSearchBarcode.SelectedIndex) as SearchItemList;
                if (item != null)
                {
                    selectedItem = item.item_id;
                    txtItem.Text = item.item_name;
                    using (SqlConnection con = new SqlConnection(DBCon.conStr))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand("SELECT type FROM Item WHERE Id=" + item.item_id))
                        {
                            cmd.Connection = con;
                            SqlDataReader rd = cmd.ExecuteReader();
                            if (rd.Read())
                            {
                                type = Convert.ToInt32(rd[0].ToString());
                            }
                        }
                    }
                    sumqty = Convert.ToDecimal(item.qty);
                    txtSale.Text = item.sale_rate;
                    txtBarcode.Text = item.barcode;
                    txtDisc.Text = "0";
                    //txtNet.Text = txtSale.Text;
                    txtQty.Text = "1";
                    getItem(item.item_id);
                    selectItemToCart(item.sale_rate);
                    txtQty.Focus();

                    gridBarcode.Visibility = Visibility.Hidden;
                    lstvSearchBarcode.Visibility = Visibility.Hidden;
                }
            }
        }

        private void txtBarcode_KeyUp(object sender, KeyEventArgs e)
        {
            if (chkBarcode.IsChecked == false)
            {
                if (txtBarcode.Text == String.Empty)
                {
                    lstvSearchBarcode.Visibility = Visibility.Hidden;
                    gridBarcode.Visibility = Visibility.Hidden;
                }
                else
                {
                    lstvSearchBarcode.Visibility = Visibility.Visible;
                    gridBarcode.Visibility = Visibility.Visible;
                    fillSearchBarcodeList(txtBarcode.Text.Trim());
                    if (lstvSearchBarcode.Items.Count > 0)
                        lstvSearchBarcode.SelectedIndex = 0;
                    if (e.Key == Key.Down)
                        lstvSearchBarcode.Focus();
                }
            }
            else
            {
                if (e.Key == Key.Enter)
                {
                    using (SqlConnection con = new SqlConnection(DBCon.conStr))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand("SELECT Id,name,sale_rate,barcode FROM Item WHERE barcode='" +txtBarcode.Text+"'"))
                        {
                            cmd.Connection = con;
                            SqlDataReader rd = cmd.ExecuteReader();
                            if (rd.Read())
                            {
                                selectedItem = rd[0].ToString();
                                txtItem.Text = rd[1].ToString();
                                txtSale.Text = rd[2].ToString();
                                txtBarcode.Text = rd[3].ToString();
                                txtDisc.Text = "0";
                                txtNet.Text = txtSale.Text;
                                txtSale.Focus();
                            }
                        }
                    }                  
                }
            }
        }

        private void Label_MouseLeftButtonDown_2(object sender, MouseButtonEventArgs e)
        {
            gridBarcode.Visibility = Visibility.Hidden;
            lstvSearchBarcode.Visibility = Visibility.Hidden;
        }

        private void chkBarcode_Click(object sender, RoutedEventArgs e)
        {
            if(chkBarcode.IsChecked==true)
                updateBarcode(1);
            else
                updateBarcode(0);
        }

        private void updateBarcode(int i)
        {
            using (SqlConnection con = new SqlConnection(DBCon.conStr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("UPDATE Barcode SET barcode="+i))
                {
                    cmd.Connection = con;
                    cmd.ExecuteNonQuery();
                }
            }       
        }

        private void getBarcode()
        {
            using (SqlConnection con = new SqlConnection(DBCon.conStr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT barcode FROM Barcode"))
                {
                    cmd.Connection = con;
                    SqlDataReader rd = cmd.ExecuteReader();
                    if (rd.Read())
                    {
                        if (Convert.ToInt32(rd[0].ToString()) == 1)
                            chkBarcode.IsChecked = true;
                        else
                            chkBarcode.IsChecked = false;
                    }
                }
            }             
        }

        private void txtSale_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            char a = Convert.ToChar(e.Text);
            if (!char.IsControl(a) && !char.IsDigit(a) && (a != '.'))
            {
                e.Handled = true;
            }


            //Allow only one . 
            if (a == '.' && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtDisc_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            char a = Convert.ToChar(e.Text);
            if (!char.IsControl(a) && !char.IsDigit(a) && (a != '.'))
            {
                e.Handled = true;
            }


            //Allow only one . 
            if (a == '.' && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtNet_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            char a = Convert.ToChar(e.Text);
            if (!char.IsControl(a) && !char.IsDigit(a) && (a != '.'))
            {
                e.Handled = true;
            }


            //Allow only one . 
            if (a == '.' && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtGross_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            char a = Convert.ToChar(e.Text);
            if (!char.IsControl(a) && !char.IsDigit(a) && (a != '.'))
            {
                e.Handled = true;
            }


            //Allow only one . 
            if (a == '.' && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtDiscount_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            char a = Convert.ToChar(e.Text);
            if (!char.IsControl(a) && !char.IsDigit(a) && (a != '.'))
            {
                e.Handled = true;
            }


            //Allow only one . 
            if (a == '.' && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtAdditional_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            char a = Convert.ToChar(e.Text);
            if (!char.IsControl(a) && !char.IsDigit(a) && (a != '.'))
            {
                e.Handled = true;
            }


            //Allow only one . 
            if (a == '.' && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtPaid_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            char a = Convert.ToChar(e.Text);
            if (!char.IsControl(a) && !char.IsDigit(a) && (a != '.'))
            {
                e.Handled = true;
            }


            //Allow only one . 
            if (a == '.' && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtBalance_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            char a = Convert.ToChar(e.Text);
            if (!char.IsControl(a) && !char.IsDigit(a) && (a != '.'))
            {
                e.Handled = true;
            }


            //Allow only one . 
            if (a == '.' && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void cmbUnit_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbUnit.SelectedIndex > 0)
            {
                //To get item price with respect to unit
                try
                {
                    using (SqlConnection con = new SqlConnection(DBCon.conStr))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand(AppConstraints.GET_UNIT))
                        {
                            cmd.Connection = con;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@Id", cmbUnit.SelectedValue);
                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                if (dr.Read())
                                {
                                    isunit = Convert.ToDecimal(dr["isunit"].ToString());
                                    kg = Convert.ToDecimal(dr["kg"].ToString());
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }


                //Get rate of Item.
                try
                {
                    using (SqlConnection con = new SqlConnection(DBCon.conStr))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand(AppConstraints.GET_PRICE))
                        {
                            cmd.Connection = con;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@Id", selectedItem);
                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                if (dr.Read())
                                {
                                    txtSale.Text = dr["sale_rate"].ToString();

                                    decimal disc = 0;
                                    try
                                    {
                                        disc = Convert.ToDecimal(txtDisc.Text);
                                    }
                                    catch { }
                                   
                                    if (isunit == 0)
                                    {
                                        txtNet.Text = ((Convert.ToDecimal(txtQty.Text) * Convert.ToDecimal(txtSale.Text)) - disc).ToString();
                                    }
                                    else
                                    {
                                        decimal net = (Convert.ToDecimal(txtQty.Text) * (Convert.ToDecimal(txtSale.Text) * Convert.ToDecimal(kg))) - disc;
                                        txtNet.Text = (Math.Round(net, 2)).ToString();
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            txtSale.Focus();
        }

        private void txtQty_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                decimal disc = 0;
                try
                {
                    disc = Convert.ToDecimal(txtDisc.Text);
                }
                catch { }

                if (isunit == 0)
                {
                    txtNet.Text = ((Convert.ToDecimal(txtQty.Text) * Convert.ToDecimal(txtSale.Text)) - disc).ToString();
                }
                else
                {
                    decimal net = (Convert.ToDecimal(txtQty.Text) * (Convert.ToDecimal(txtSale.Text) * Convert.ToDecimal(kg))) - disc;
                    txtNet.Text = (Math.Round(net, 2)).ToString();
                }
            }
            catch { }
            
        }

        private void lstvSearchItem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (lstvSearchItem.Items.Count > 0)
                {
                    var item = lstvSearchItem.Items.GetItemAt(lstvSearchItem.SelectedIndex) as SearchItemList;
                    if (item != null)
                    {
                        selectedItem = item.item_id;
                        txtItem.Text = item.item_name;
                        using (SqlConnection con = new SqlConnection(DBCon.conStr))
                        {
                            con.Open();
                            using (SqlCommand cmd = new SqlCommand("SELECT type FROM Item WHERE Id=" + item.item_id))
                            {
                                cmd.Connection = con;
                                SqlDataReader rd = cmd.ExecuteReader();
                                if (rd.Read())
                                {
                                    type = Convert.ToInt32(rd[0].ToString());
                                }
                            }
                        }
                        sumqty = Convert.ToDecimal(item.qty);
                        txtSale.Text = item.sale_rate;
                        txtBarcode.Text = item.barcode;
                        txtDisc.Text = "0";
                        //txtNet.Text = txtSale.Text;
                        txtQty.Text = "1";
                        getItem(item.item_id);
                        selectItemToCart(item.sale_rate);
                        txtQty.Focus();

                        gridItem.Visibility = Visibility.Hidden;
                        lstvSearchItem.Visibility = Visibility.Hidden;
                    }
                }
            }
        }

        private void lstvSearchItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (lstvSearchItem.Items.Count > 0)
            {
                var item = lstvSearchItem.Items.GetItemAt(lstvSearchItem.SelectedIndex) as SearchItemList;
                if (item != null)
                {
                    selectedItem = item.item_id;
                    txtItem.Text = item.item_name;
                    using (SqlConnection con = new SqlConnection(DBCon.conStr))
                    {
                        con.Open();
                        using (SqlCommand cmd = new SqlCommand("SELECT type FROM Item WHERE Id=" + item.item_id))
                        {
                            cmd.Connection = con;
                            SqlDataReader rd = cmd.ExecuteReader();
                            if (rd.Read())
                            {
                                type = Convert.ToInt32(rd[0].ToString());
                            }
                        }
                    }
                    sumqty = Convert.ToDecimal(item.qty);
                    txtSale.Text = item.sale_rate;
                    txtBarcode.Text = item.barcode;
                    txtDisc.Text = "0";
                    //txtNet.Text = txtSale.Text;
                    txtQty.Text = "1";
                    getItem(item.item_id);
                    selectItemToCart(item.sale_rate);
                    txtQty.Focus();

                    gridItem.Visibility = Visibility.Hidden;
                    lstvSearchItem.Visibility = Visibility.Hidden;
                }
            }
        }

        private void txtItem_KeyUp(object sender, KeyEventArgs e)
        {
            if (txtItem.Text == String.Empty)
            {
                lstvSearchItem.Visibility = Visibility.Hidden;
                gridItem.Visibility = Visibility.Hidden;
            }
            else
            {
                lstvSearchItem.Visibility = Visibility.Visible;
                gridItem.Visibility = Visibility.Visible;
                fillSearchItemList(txtItem.Text.Trim());
                if (lstvSearchItem.Items.Count > 0)
                    lstvSearchItem.SelectedIndex = 0;
                if (e.Key == Key.Down)
                    lstvSearchItem.Focus();
            }
        }
    }
}
