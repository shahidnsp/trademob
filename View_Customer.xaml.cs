﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;
using MobileShop_Application.Class;

namespace MobileShop_Application
{
    /// <summary>
    /// Interaction logic for View_Customer.xaml
    /// </summary>
    public partial class View_Customer : Window
    {
        public View_Customer()
        {
            InitializeComponent();
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                Excel.Application app = new Excel.Application();
                app.Visible = true;
                Excel.Workbook wb = app.Workbooks.Add(1);
                Excel.Worksheet ws = (Excel.Worksheet)wb.Worksheets[1];
                Excel.Range range;
                range = ws.get_Range("A1", "J1");
                range.Font.Bold = true;
                range.Font.Underline = true;
                range.Font.Size = 14;
                range.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);

                ws.Cells[1, 1] = "SlNo";
                ws.Cells[1, 2] = "Id";
                ws.Cells[1, 3] = "Name";
                ws.Cells[1, 4] = "Address";
                ws.Cells[1, 5] = "Place";
                ws.Cells[1, 6] = "Phone1";
                ws.Cells[1, 7] = "Phone2";
                ws.Cells[1, 8] = "Photo";
                ws.Cells[1, 9] = "Proof";
                ws.Cells[1, 10] = "Customer Type";
                int i = 2;
                foreach (var lvi in lstvCustomer.Items)
                {
                    var item = lvi as Customer_List;
                    if (item != null)
                    {
                        ws.Cells[i, 1] = item.slno;
                        ws.Cells[i, 2] = item.Id;
                        ws.Cells[i, 3] = item.name;
                        ws.Cells[i, 4] = item.address;
                        ws.Cells[i, 5] = item.place;
                        ws.Cells[i, 6] = item.phone1;
                        ws.Cells[i, 7] = item.phone2;
                        //ws.Cells[i, 8] = item.photo;
                        //ws.Cells[i, 9] = item.proof;
                        ws.Cells[i, 10] = item.customer_type;
                    }
                    i++;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        class Customer_List
        {
             public string slno { get; set; }
             public string Id { get; set; }
             public string name { get; set; }
             public string address { get; set; }
             public string place { get; set; }
             public string phone1 { get; set; }
             public string phone2 { get; set; }
             public Byte[] photo { get; set; }
             public Byte[] proof { get; set; }
             public string customer_type { get; set; }            
        }

        private void loadData()
        {
            string type = "Seller";

            if (cmbCustomerType.SelectedIndex == 1)
                type = "Buyer";
            

            this.Title = type + " List";
            lstvCustomer.Items.Clear();
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(AppConstraints.LIST_VIEW_CUSTOMER))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@type", type);
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            int slno = 1;
                            while (dr.Read())
                            {
                                lstvCustomer.Items.Add(new Customer_List
                                {
                                    Id = dr["Id"].ToString(),
                                    slno = slno++.ToString(),
                                    name = dr["name"].ToString(),
                                    address = dr["address"].ToString(),
                                    place = dr["place"].ToString(),
                                    phone1 = dr["phone1"].ToString(),
                                    phone2 = dr["phone2"].ToString(),
                                    photo = (Byte[])dr["photo"],
                                    proof = (Byte[])dr["proof"],
                                    customer_type = dr["customer_type"].ToString()
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            if (cmbCustomerType.SelectedIndex > 0)
                loadData();   
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Customer newCustomer = new Customer();
            newCustomer.ShowDialog();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //loadData();
        }

        private void cmbCustomerType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(cmbCustomerType.SelectedIndex>0)
             loadData();
        }

        private void SearchById_Copy_TextChanged(object sender, TextChangedEventArgs e)
        {
            lstvCustomer.Items.Clear();
            try
            {
                using (SqlConnection con = new SqlConnection(DBCon.conStr))
                {
                    con.Open();

                    using (SqlCommand cmd = new SqlCommand(AppConstraints.LIST_VIEW_CUSTOMER_WITH_NAME))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@name", txtName.Text);
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            int slno = 1;
                            while (dr.Read())
                            {
                                lstvCustomer.Items.Add(new Customer_List
                                {
                                    Id = dr["Id"].ToString(),
                                    slno = slno++.ToString(),
                                    name = dr["name"].ToString(),
                                    address = dr["address"].ToString(),
                                    place = dr["place"].ToString(),
                                    phone1 = dr["phone1"].ToString(),
                                    phone2 = dr["phone2"].ToString(),
                                    photo = (Byte[])dr["photo"],
                                    proof = (Byte[])dr["proof"],
                                    customer_type = dr["customer_type"].ToString()
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lstvCustomer_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var item = lstvCustomer.Items.GetItemAt(lstvCustomer.SelectedIndex) as Customer_List;               
                Customer customer = new Customer();
                customer.editId = item.Id;
                customer.load = "EDIT";
                customer.Show();
            }
            catch
            {

            }
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            if (cmbCustomerType.SelectedIndex > 0)
                loadData();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
